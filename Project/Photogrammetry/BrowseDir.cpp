#include "StdAfx.h"
#include "stdlib.h"
#include <direct.h>
#include <string.h>
#include "io.h"
#include "stdio.h" 
#include "iostream.h"
#include "BrowseDir.h"

#using namespace std;

CBrowseDir::CBrowseDir()
{
	//initialize fold directory
	_getcwd(m_szInitDir, _MAX_PATH);

	//if last character is not '\', add it
	int len=strlen(m_szInitDir);
	if (m_szInitDir[len-1] != '\\')
		strcat(m_szInitDir,"\\");
}

bool CBrowseDir::SetInitDir(const char *dir)
{
	//transform dir into absolute directory
	if (_fullpath(m_szInitDir,dir,_MAX_PATH) == NULL)
		return false;

	//check whether directory exists
	if (_chdir(m_szInitDir) != 0)
		return false;

	//if last character is not '\', add it
	int len=strlen(m_szInitDir);
	if (m_szInitDir[len-1] != '\\')
		strcat(m_szInitDir,"\\");

	return true;
}

bool CBrowseDir::BeginBrowse(const char *filespec)
{
	ProcessDir(m_szInitDir,NULL);
	return BrowseDir(m_szInitDir,filespec);
}

bool CBrowseDir::BrowseDir(const char *dir,const char *filespec)
{
	if (_chdir(dir) != 0) 
		return false;
	

	//search for files in the directory which meet the filespec
	long hFile;
	_finddata_t fileinfo;
	if ((hFile=_findfirst(filespec, &fileinfo)) != -1)
	{
		do
		{
			//check whether sub-directory,
			//if not, process this file
			if (!(fileinfo.attrib & _A_SUBDIR))
			{
				char filename[_MAX_PATH];
				strcpy(filename,dir);
				strcat(filename,fileinfo.name);
				cout << filename << endl;
				if (!ProcessFile(filename))
					return false;
			}
		} while (_findnext(hFile, &fileinfo) == 0);
		_findclose(hFile);
	}

	_chdir(dir);
	if ((hFile=_findfirst("*.*",&fileinfo)) != -1)
	{
		do
		{
			if ((fileinfo.attrib & _A_SUBDIR))
			{
				if (strcmp(fileinfo.name,".") != 0 && strcmp
					(fileinfo.name,"..") != 0)
				{
					char subdir[_MAX_PATH];
					strcpy(subdir,dir);
					strcat(subdir,fileinfo.name);
					strcat(subdir,"\\");
					ProcessDir(subdir,dir);
					if (!BrowseDir(subdir,filespec))
						return false;
				}
			}
		} while (_findnext(hFile,&fileinfo) == 0);
		_findclose(hFile);
	}
	return true;
}

bool CBrowseDir::ProcessFile(const char *filename)
{
	return true;
}

void CBrowseDir::ProcessDir(const char *currentdir, const char *parentdir)
{
}



#ifndef _BROWS_DIR_
#define _BROWS_DIR_

#define  _MAX_PATH 256
#include <vector>
	
class CBrowseDir
{
protected:

	char m_szInitDir[_MAX_PATH];

public:

	CBrowseDir();
	CBrowseDir(const char *dir);
	~CBrowseDir();

	bool SetInitDir(const char *dir);
	std::vector<char*>* Browse(const char *filespec);

protected:

	bool BrowseDir(const char *dir,const char *filespec);

	virtual bool ProcessFile(const char *filename);

	virtual void ProcessDir(const char *currentdir,const char *parentdir);
};

#endif

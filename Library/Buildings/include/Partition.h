
/*
    Copyright 2010 University of Twente and Delft University of Technology
 
       This file is part of the Mapping libraries and tools, developed
  for research, education and projects in photogrammetry and laser scanning.

  The Mapping libraries and tools are free software: you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the License,
                   or (at your option) any later version.

 The Mapping libraries and tools are distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
          along with the Mapping libraries and tools.  If not, see
                      <http://www.gnu.org/licenses/>.

----------------------------------------------------------------------------*/

#ifndef _Partition_h
#define _Partition_h

#include "House.h"
#include "Buildings_DLL.h"


class BldDll_API Partition : public ObjectPoints2D, public FeatureNumber
{
public:
   Partition() {} ;
   Partition(const ObjectPoints2D &pts, FeatureNumber n)
   	: ObjectPoints2D(pts), FeatureNumber(n) {}; 
   Partition(const Partition &p) { *this = p; }
   ~Partition() {};

   Partition& operator=(const Partition &p);	
   
   /// Returns a constant reference to the object
   const Partition& PartitionRef() const { return *this; }
   Partition& PartitionRef() { return *this; }
   
   /// Test if a point belongs to the partition
   int Member(const PointNumber &node) const;
         
   /// Rotate the partition such that the given corner becomes the first one   
   /// Returns true if the corner is found, false otherwise 
   bool Rotate(const PointNumber &pt);
   
   /// Revert the points  
   void Reverse();     
   	   	   	 
   /// Print
   void Print() const;	  
   
public:
   /// Equality operator
   friend bool operator==(const Partition &p1, const Partition &p2);   		
};


class PartitionScheme : public std::vector<Partition>
{
public:
   PartitionScheme() {};
   ~PartitionScheme() {};
   PartitionScheme& operator=(const PartitionScheme& ps);
   const PartitionScheme& PartitionSchemeRef() const { return *this; }
   PartitionScheme& PartitionSchemeRef(){ return *this; }  
   bool Member(const Partition &p) const;   
   
   /// subtract 2 partition schemes
   PartitionScheme& operator-=(const PartitionScheme& ps);   
   
   void Print() const;
   friend bool operator==(const PartitionScheme &p1, const PartitionScheme &p2);   		   
};

class VectorRectPairs;

class PartitionSchemes : public std::vector<PartitionScheme>
{
  int no_max;
  
public:
   PartitionSchemes(int no = 0) { no_max = no; }
   PartitionSchemes(const House &house);
   ~PartitionSchemes() {};
   PartitionSchemes& operator=(const PartitionSchemes& ps);
   const PartitionSchemes& PartitionSchemesRef() const { return *this; }
   PartitionSchemes& PartitionSchemesRef() { return *this; }                  
   
   /// Add partition scheme to the list only if it isn't contained in the list
   void Add(const PartitionScheme &ps);
   
   /// Sort based on the length of the ps
   void SortMDL();
   void Print() const;
   void MergePartitions(const vector<ObjectPoints2D> &rect_list); 
   void MergePartitionsRec(const PartitionScheme &rect_list, 
   		PartitionSchemes &ps_list, const VectorRectPairs &rect_pairs);    		   
};

#endif   	
    

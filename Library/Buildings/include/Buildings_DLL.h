#ifndef __BUILDINGS_DLL_H__
#define __BUILDINGS_DLL_H__


#ifdef __MSVC__
#ifdef BUILDINGS_EXPORTS
#define BldDll_API __declspec(dllexport)
#else
#define BldDll_API __declspec(dllimport)
#endif
//----------------------------------------------------------------------
// BldDll_API is ignored for all other systems
//----------------------------------------------------------------------
#else
#define BldDll_API
#endif

#endif


/*
Copyright 2010 University of Twente and Delft University of Technology

This file is part of the Mapping libraries and tools, developed
for research, education and projects in photogrammetry and laser scanning.

The Mapping libraries and tools are free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

The Mapping libraries and tools are distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the Mapping libraries and tools.  If not, see
<http://www.gnu.org/licenses/>.

----------------------------------------------------------------------------*/
#include <fstream>

#include "HouseGraph.h"
#include "Constants.h"
#include "Segment2D.h"
#include "Segment.h"

bool MemberSubList(const ObjectPoints2D &rect, const std::vector<ObjectPoints2D> &rect_list);
bool MemberList(const ObjectPoints2D &rect, const std::vector<ObjectPoints2D> &rect_list);
int MemberList3(const ObjectPoints2D &rect, const std::vector<ObjectPoints2D> &rect_list);
bool Member(const ObjectPoint2D &node, const ObjectPoints2D &rect);

// #define __DEBUG

HouseGraph::~HouseGraph()
{
	std::vector<HouseGraphNode*>::iterator i;
	for(i = begin(); i < end(); i++)
	{
		(*i)->neibs.clear();
		delete *i;
	}  
	clear();
}

void HouseGraph::FindRects(std::vector<ObjectPoints2D> &rect_list) const
{
	HouseGraph::const_iterator n;
	ObjectPoints2D visited_nodes;

	for(n=begin(); n<end(); n++)
	{
		ObjectPoints2D rect;
		rect.push_back((*n)->pnt);
		visited_nodes.push_back((*n)->pnt);
		FindRect(*n, rect, rect_list, visited_nodes);
	}
}  

void HouseGraph::FindRect(const HouseGraphNode* node,  
	ObjectPoints2D &rect, std::vector<ObjectPoints2D> &rect_list,
	const ObjectPoints2D &visited_nodes) const
{
	if (rect.size()>2 && IsNeighbours(*rect.begin(), *(rect.end()-1))) {
		if (!MemberList(rect, rect_list))
			rect_list.push_back(rect);
	}

	std::vector<HouseGraphNode*>::const_iterator iNode;
	for(iNode=node->neibs.begin(); iNode<node->neibs.end(); iNode++) 
	{
		if (Member((*iNode)->pnt, rect) || Member((*iNode)->pnt, visited_nodes))
			continue;

		bool bOnLine = false;  
		if (rect.size() > 1) {
			rect.push_back(*rect.begin());          
			ObjectPoints2D::const_iterator pnt;
			for(pnt=rect.begin(); pnt<rect.end()-1; pnt++) {
				Line2D lin(*pnt, *(pnt+1));
				if (lin.PointOnLine((*iNode)->pnt, 0.001)) {
					bOnLine = true;
					break;
				}
			}
			rect.erase(rect.end()-1);
		}

		if (!bOnLine) {
			ObjectPoints2D r1 = rect;
//			r1.insert(r1.begin(), rect.begin(), rect.end());
			r1.push_back((*iNode)->pnt);
			FindRect(*iNode, r1, rect_list, visited_nodes);
		}
	}
}

/*
void HouseGraph::FindRect(const HouseGraphNode* node,  
ObjectPoints2D &rect, std::vector<ObjectPoints2D> &rect_list) const
{
int ind;

if (rect.size() > 2 && IsNeighbours(*rect.begin(), *(rect.end() - 1)))
{
if((ind = MemberList3(rect, rect_list)) != -1)
{
if ((rect_list.begin() + ind)->size() > rect.size())
{
rect_list.erase(rect_list.begin() + ind);
rect_list.push_back(rect);
}       
}
else
{
printf(" add rect %d %d %d %d\n", rect[0].Number(), rect[1].Number(),
rect[2].Number(), rect[3].Number());     
rect_list.push_back(rect);
}
return;
}

std::vector<HouseGraphNode*>::const_iterator i;
for(i = node->neibs.begin(); i < node->neibs.end(); i++)
{
if (!Member((*i)->pt, rect))
{
ObjectPoints2D r1;
r1.insert(r1.begin(), rect.begin(), rect.end());
r1.push_back((*i)->pt);
if (r1.size() > 2 && ((ind = MemberList3(r1, rect_list)) != -1))
{
if ((rect_list.begin() + ind)->size() > r1.size())
FindRect(*i, r1, rect_list);
}
else     
FindRect(*i, r1, rect_list);
}
}
}*/

bool HouseGraph::IsNeighbours(const ObjectPoint2D &n1, 
	const ObjectPoint2D &n2) const
{       
	std::vector<HouseGraphNode*>::const_iterator n;
	for(n = begin(); n < end(); n++)
	{
		if ((*n)->pnt.Number() == n1.Number())
		{
			std::vector<HouseGraphNode*>::const_iterator i;
			for(i = (*n)->neibs.begin(); i < (*n)->neibs.end(); i++)
			{
				if ((*i)->pnt.Number() == n2.Number())
					return true;
			}
			return false;
		}
		if ((*n)->pnt.Number() == n2.Number())
		{
			std::vector<HouseGraphNode*>::const_iterator i;
			for(i = (*n)->neibs.begin(); i < (*n)->neibs.end(); i++)
			{
				if ((*i)->pnt.Number() == n1.Number())
					return true;
			}
			return false;
		}  
	}
	return false;
}

int HouseGraph::FindNearbyGraphNode(const Position2D &pos, double err_dist) const
{
	std::vector<HouseGraphNode*>::const_iterator n;
	for(n = begin(); n < end(); n++)
	{
		if (Distance((*n)->pnt, pos) < err_dist)
			return n - begin();
	}
	return -1;
}        

void HouseGraph::InsertGraphNode(HouseGraphNode* node, 
	const Position2D &pos1, const Position2D &pos2)
{
	Segment s(pos1, pos2);
	if (s.PointOnSegment(node->pnt))
	{
		// look if there is an arc in the graph between pos1 and pos2
		bool found = false;
		std::vector<HouseGraphNode*>::iterator n;
		for(n = begin(); n < end(); n++)
		{
			if (Distance((*n)->pnt, pos1) < MIN_DIST_MAP_POINTS / 2)
			{
				std::vector<HouseGraphNode*>::iterator i;
				for(i = (*n)->neibs.begin(); i < (*n)->neibs.end(); i++)
				{
					if (Distance((*i)->pnt, pos2) < MIN_DIST_MAP_POINTS / 2)
					{
						DeleteNeighbour(*i, pos1);
						(*i)->neibs.push_back(node);
						node->neibs.push_back(*i);
						(*n)->neibs.erase(i);
						(*n)->neibs.push_back(node);
						node->neibs.push_back(*n);
						found = true;
						break;
					}
				}      	             
				if (!found)
				{
					for(i = (*n)->neibs.begin(); i < (*n)->neibs.end(); i++)
					{
						if ((*i)->pnt.Number() == node->pnt.Number())
						{
#ifdef __DEBUG
							printf("no insert\n");
#endif

							return;
						}
						if ((s.PointOnLine((*i)->pnt, MIN_DIST_MAP_POINTS) && 
							(Distance((*i)->pnt, pos2) < Distance(pos1, pos2))))
						{
							InsertGraphNode(node, (*i)->pnt, pos2);
							return;
						}
					}
					InsertGraphNode(node, pos2, (*i)->pnt);  
					return;
				}
				else
					break;
			}
		}        
	}       
	else
	{                		        
		Position2D pos_close;
		if (Distance(pos1, node->pnt) < Distance(pos2, node->pnt))
			pos_close = pos1;
		else
			pos_close = pos2;

		std::vector<HouseGraphNode*>::iterator n;
//		double d1, d2;
		for(n = begin(); n < end(); n++)
		{
			if (Distance((*n)->pnt, pos_close) < MIN_DIST_MAP_POINTS / 2)
			{
				std::vector<HouseGraphNode*>::iterator i;
				for(i = (*n)->neibs.begin(); i < (*n)->neibs.end(); i++)
				{
					if ((*i)->pnt.Number() == node->pnt.Number())
					{
#ifdef __DEBUG
						printf("no insert\n");
#endif
						return;
					}           


					Segment ss((*i)->pnt, pos_close);
					/*
					if (node->pnt.Number() == 26 && (*i)->pnt.Number() == 1204)
					{
					printf("%d,  %d, %d   %d,  %6.6f\n", (*n)->pnt.Number(), (*i)->pnt.Number(),
					ss.PointOnLine((*i)->pnt, MIN_DIST_MAP_POINTS), 
					ss.PointOnSegment(node->pnt), ss.DistanceToPoint(node->pnt));
					ss.PointOnSegment(node->pnt);
					} */

					if (ss.PointOnLine(node->pnt, MIN_DIST_MAP_POINTS) && 
						(ss.PointOnSegment(node->pnt) || 
						Distance(pos_close, node->pnt) > Distance((*i)->pnt, node->pnt)))
					{     
						InsertGraphNode(node, pos_close, (*i)->pnt);
						return;
					}
				}  


				Segment ss(node->pnt, pos_close);
				for(i = (*n)->neibs.begin(); i < (*n)->neibs.end(); i++)
				{

					if (node->pnt.Number() == 26 && (*i)->pnt.Number() == 1204)
					{
						printf("%d,  %d, %d   %d,  %6.6f\n", (*n)->pnt.Number(), (*i)->pnt.Number(),
							ss.PointOnLine((*i)->pnt, MIN_DIST_MAP_POINTS), 
							ss.PointOnSegment(node->pnt), ss.DistanceToPoint(node->pnt));
						ss.PointOnSegment(node->pnt);
					} 

					if (ss.PointOnLine((*i)->pnt, MIN_DIST_MAP_POINTS) && 
						Distance(pos_close, node->pnt) > Distance((*i)->pnt, node->pnt))
					{     
						InsertGraphNode(node, pos_close, (*i)->pnt);
						return;
					}
				}  

				printf("Error!!!!!!: An arc should exist ");
				PrintNode(**n);
			}  
		}   
	}   
} 

/*
void HouseGraph::AddNeighbour(HouseGraphNode* node1, HouseGraphNode* node2)     
{
std::vector<HouseGraphNode*>::iterator i;
for(i = node1->neibs.begin(); i < node1->neibs.end(); i++)
{
if(*i == node2)
return;
Segment s(node1->pnt, (*i)->pnt));
if (s.PointOnLine(node2->pnt, MIN_DIST_MAP_POINTS))
{
if (s.PointOnSegment(node2->pnt)
{
(*i)->neibs.erase(node1);
(*i)->neibs.push_back(node2);    
node2->neibs.push_back(i);    
node1->neibs.erase(i);
node1->neibs.push_back(node2);    
node2->neibs.push_back(node1);    
return;
}
else
AddNeighbour(node2, node1);
}  
}       
}
*/

void HouseGraph::DeleteNeighbour(HouseGraphNode* node, const Position2D &pos)        
{
	std::vector<HouseGraphNode*>::iterator i;
	for(i = node->neibs.begin(); i < node->neibs.end(); i++)
	{
		if(Distance((*i)->pnt, pos) < MIN_DIST_MAP_POINTS)
		{
			node->neibs.erase(i);
			return;
		}
	}       
}

void HouseGraph::PrintNode(const HouseGraphNode &node) const
{
	printf("Node %d: ", node.pnt.Number());
	std::vector<HouseGraphNode*>::const_iterator i;
	for(i = node.neibs.begin(); i < node.neibs.end(); i++)
	{
		printf("%d, ", (*i)->pnt.Number());
	}
	printf("\n");     
}


void HouseGraph::PrintNode(const PointNumber &no) const
{
	std::vector<HouseGraphNode*>::const_iterator n;      
	for(n = begin(); n < end(); n++)
	{
		if ((*n)->pnt == no)
		{  
			PrintNode(**n);
			break;
		}
	}
}

void HouseGraph::Print() const
{
	std::ofstream stream("debug.txt", std::fstream::out);

	std::vector<HouseGraphNode*>::const_iterator n;      
	for(n = begin(); n < end(); n++)
	{
		printf("Node: %d,  neibs:", (*n)->pnt.Number());
		stream<<"Node: "<<(*n)->pnt.Number()<<"  neibs:";

		std::vector<HouseGraphNode*>::const_iterator n0;
		for(n0 = (*n)->neibs.begin(); n0 < (*n)->neibs.end(); n0++) {
			printf("%d, ", (*n0)->pnt.Number());
			stream<<(*n0)->pnt.Number()<<", ";
		}

		printf("\n");
		stream<<std::endl;
	}
}


bool SameRectangles(const ObjectPoints2D &rect1, const ObjectPoints2D &rect2)
{
	if (rect1.size() != rect2.size())
		return false;

	ObjectPoints2D::const_iterator j;  
	for(j = rect2.begin(); j < rect2.end(); j++)
		if (!Member(*j, rect1))
			return false;
	return true;
}


bool MemberList(const ObjectPoints2D &rect, const std::vector<ObjectPoints2D> &rect_list)
{   
	if (rect_list.empty())
		return false;

	std::vector<ObjectPoints2D>::const_iterator i;
	for(i = rect_list.begin(); i < rect_list.end(); i++)
	{
		if (SameRectangles(*i, rect))
			return true;
	}

	return false;                
}        


int MemberList3(const ObjectPoints2D &rect, const std::vector<ObjectPoints2D> &rect_list)
{   
	if (rect_list.empty())
		return -1;

	std::vector<ObjectPoints2D>::const_iterator i;
	ObjectPoints2D::const_iterator j;
	int no = 0;

	for(i = rect_list.begin(); i < rect_list.end(); i++)
	{
		no = 0;
		for(j = rect.begin(); j < rect.end(); j++)
		{
			if (Member(*j, *i))
			{
				no++;
				if (no >= 3)
					return i - rect_list.begin();
			}  
		}   
	}

	return -1;                
}        



bool MemberSubList(const ObjectPoints2D &rect, const std::vector<ObjectPoints2D> &rect_list)
{
	std::vector<ObjectPoints2D>::const_iterator i;
	ObjectPoints2D::const_iterator j;
	bool found = true;

	for(i = rect_list.begin(); i < rect_list.end(); i++)
	{
		found = true;
		for(j = rect.begin(); j < rect.end(); j++)
		{
			if (!Member(*j, *i))
			{
				found = false;
				break;
			}  
		}   
		if (found)
			return true;
	}
	return false;                
}        


bool Member(const ObjectPoint2D &node, const ObjectPoints2D &rect)
{
	ObjectPoints2D::const_iterator j;
	for(j = rect.begin(); j < rect.end(); j++)
		if(j->Number() == node.Number())
			return true;
	return false;
}        



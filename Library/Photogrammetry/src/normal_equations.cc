
/*
Copyright 2010 University of Twente and Delft University of Technology

This file is part of the Mapping libraries and tools, developed
for research, education and projects in photogrammetry and laser scanning.

The Mapping libraries and tools are free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

The Mapping libraries and tools are distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the Mapping libraries and tools.  If not, see
<http://www.gnu.org/licenses/>.

----------------------------------------------------------------------------*/



/* Two routines to do a simple least squares adjustment.
*
* Update_Normal_Eq
* Solve_Normal_Eq
*
* Last update: 15-07-97
*
* George Vosselman
*/
//#include "stdafx.h"
#include <stdlib.h>
//#include "digphot_arch.h"
#include "Database.h"
/*------------------------------------------------------------------------------
Updating of the normal equation system by one additional observation.

a   - row of the design matrix
y   - corresponding observation
w   - weight of the observation
ata - normal matrix
aty - right hand side of normal equation system
np  - number of unknown parameters
------------------------------------------------------------------------------*/

void Update_Normal_Eq(double *a, double y, double w, double *ata, 
	double *aty, int np)
{
	double *aptr1, *aptr2, *ataptr, *atyptr, w2;
	int    ir, ic;

	w2 = w * w;
	for (ir=0, aptr1=a, ataptr=ata, atyptr=aty;
		ir<np;
		ir++, aptr1++, atyptr++) {
			for (ic=0, aptr2=a;
				ic<np;
				ic++, aptr2++, ataptr++) {
					(*ataptr) += (*aptr1) * (*aptr2) * w2;
			}
			(*atyptr) += (*aptr1) * y * w2;
	}
}

/*------------------------------------------------------------------------------
Solving the normal equation system with LINPACK subroutines.

ata - normal matrix
aty - right hand side of normal equation system
this array is overwritten with the determined parameters
np  - number of unknown parameters
------------------------------------------------------------------------------*/
#include "Eigen/Dense"
using namespace Eigen;
void Solve_Normal_Eq(double *ata, double *aty, int np)
{
	MatrixXd m(np, np); 
	for (int i=0; i<np; ++i) {
		for (int j=0; j<np; ++j) {
			m(i,j) = ata[i*np+j];
		}
	}
	VectorXd b(np);
	for (int i=0; i<np; ++i) 
		b(i) = aty[i];

	VectorXd x(np);
	x = m.fullPivLu().solve(b);
	
	for (int i=0; i<np; ++i) 
		aty[i] = x(i);

/*	double* A = new double[np*np];
	double* B = new double[np];
	int* pivot = new int[np];
	double* x = new double[np];

	memcpy(A, ata, np*np*sizeof(double));
	memcpy(B, aty, np*sizeof(double));
	memset(pivot, 0, np*sizeof(int));
	memset(x, 0, np*sizeof(double));

	int err;
	err = LU_Decomposition_with_Pivoting(A, pivot, np);
	if (err<0) goto end_flag;//Matrix A is singular
	err = LU_with_Pivoting_Solve(A, B, pivot, x, np);
	if (err<0) goto end_flag;

	memcpy(aty, x, np*sizeof(double));

end_flag:
	delete [] A;
	delete [] B;
	delete [] pivot;
	delete [] x;*/
	/*
	int    *ipvt, job=0;
	double *z, rcond;

#ifdef hpux
	void dgeco(double *, int *, int *, int *, double *, double *);
	void dgesl(double *, int *, int *, int *, double *, int *);
#else
	void dgeco_(double *, int *, int *, int *, double *, double *);
	void dgesl_(double *, int *, int *, int *, double *, int *);
#endif

	ipvt = (int *) malloc(np * sizeof(int));
	z    = (double *) malloc(np * sizeof(double));

	// LU decomposition 

#ifdef hpux
	dgeco(ata, &np, &np, ipvt, &rcond, z);
#else
	dgeco_(ata, &np, &np, ipvt, &rcond, z);
#endif

	// Solve the equation system 

#ifdef hpux
	dgesl(ata, &np, &np, ipvt, aty, &job);
#else
	dgesl_(ata, &np, &np, ipvt, aty, &job);
#endif

	free(ipvt);  free(z);*/
}


/*
Copyright 2010 University of Twente and Delft University of Technology

This file is part of the Mapping libraries and tools, developed
for research, education and projects in photogrammetry and laser scanning.

The Mapping libraries and tools are free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

The Mapping libraries and tools are distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the Mapping libraries and tools.  If not, see
<http://www.gnu.org/licenses/>.

----------------------------------------------------------------------------*/

//#include "stdafx.h"

#include <math.h>
#include <stdlib.h>
#include "digphot_arch.h"
#include "vMatrix.h"
#include "Rotation2D.h"
#include "Rotation3D.h"


//extern "C" double **matrix(long nrl, long nrh, long ncl, long nch);
//extern "C" void svdcmp(double **a, int m, int n, double w[], double **v);
//extern "C" double *NRvector(long nl, long nh);
//extern "C" void free_vector(double *v, long nl, long nh);
//extern "C" void free_matrix(double **m, long nrl, long nrh, long ncl, long nch);

#if defined linux
extern "C" void dgeco_(double *, int *, int *, int *, double *, double *);
extern "C" void dgedi_(double *, int *, int *, int *, double *, double *, int *);
#else
#  if defined hpux
extern "C" void dgeco(double *, int *, int *, int *, double *, double *);
extern "C" void dgedi(double *, int *, int *, int *, double *, double *, int *);
#  else /* irix */
extern "C" void dgeco_(double *, int *, int *, int *, double *, double *);
extern "C" void dgedi_(double *, int *, int *, int *, double *, double *, int *);
#  endif
#endif







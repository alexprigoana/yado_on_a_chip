#include "LineTopologiesComplex.h"
#include "Plane.h"
#include "TIN.h"
#include "DataBounds2D.h"
#include "ObjectPoints.h"

#include <assert.h>
#include <map>

#define MAP_PAR_LABEL 2

//Dong
LineTopologies LineTopologyComplex::ToLineTopologies() 
{
	LineTopologies tempTopLines;
	LineTopologies::const_iterator itrinnerRing;

	for(itrinnerRing=m_innerRinges.begin(); itrinnerRing!=m_innerRinges.end(); ++itrinnerRing)
		tempTopLines.push_back(*itrinnerRing);
	tempTopLines.push_back(m_outerRing);

	return tempTopLines;
}

//Dong
void LineTopologyComplex::SetAttribute(const LineTopologyTag tag, const int value)
{
	this->OuterRing().SetAttribute(tag, value);
}

//Dong
int LineTopologyComplex::GetAttribute(const LineTopologyTag tag)
{
	if(!this->OuterRing().HasAttribute(tag))
		return -1;
	return this->OuterRing().Attribute(tag); 
}

bool LineTopologyComplex::AddInnerRing(const LineTopology& innerRing) 
{
	if(std::find(m_innerRinges.begin(), m_innerRinges.end(), innerRing)==m_innerRinges.end())
		m_innerRinges.push_back(innerRing);
	return true;
}


bool LineTopologyComplex::ReplaceOuterRing(const LineTopology& outerRing) 
{
	m_outerRing=outerRing; 
	return true;
}

bool LineTopologyComplex::CoverPoint(const ObjectPoints& objPnts, const Position3D& pnt) const 
{
	return CoverPoint(objPnts, (Vector2D)pnt.Position2DOnly());
}

bool LineTopologyComplex::CoverPoint(const ObjectPoints& objPnts, const Position2D& pnt) const 
{
	return CoverPoint(objPnts, (Vector2D)pnt);
}

bool LineTopologyComplex::CoverPoint(const ObjectPoints& objPnts, const Vector3D& pnt) const 
{
	return CoverPoint(objPnts, (Vector2D)pnt);
}
bool LineTopologyComplex::CoverPoint(const ObjectPoints& objPnts, const Vector2D& pnt) const 
{
	if (!InsidePolygonJordan(pnt, objPnts, m_outerRing)) return false;
	LineTopologies::const_iterator inRing;
	for (inRing=m_innerRinges.begin(); inRing!=m_innerRinges.end(); inRing++ ) 
		if (InsidePolygonJordan(pnt, objPnts, *inRing)) return false;

	return true;
}

DataBounds2D LineTopologyComplex::DeriveBound(const ObjectPoints& objPnts)
{
	DataBounds2D temBound;

	for (unsigned int i=0; i<m_outerRing.size(); ++i)
		temBound.Update(objPnts[m_outerRing[i].Number()].Position2DOnly());

	for (unsigned int i=0; i<m_innerRinges.size(); ++i) {
		for (unsigned int j=0; j<m_innerRinges[i].size(); ++j)
			temBound.Update(objPnts[m_innerRinges[i][j].Number()].Position2DOnly());
	}

	return temBound;
}

double LineTopologyComplex::Area(ObjectPoints objPnts) const
{
	double area=m_outerRing.CalculateArea(objPnts);
	for (unsigned int i=0; i<m_innerRinges.size(); i++)
		area -= m_innerRinges[i].CalculateArea(objPnts);

	return area;
}

#include "LineSegments3D.h"
double LineTopologyComplex::Distance2Pnt(const ObjectPoints& objPnts, const Vector3D& inPnt) const
{
	double	minDist = 10000.0, temDist;
	LineTopologies temLineTops = m_innerRinges;
	temLineTops.push_back(m_outerRing);
	LineSegments3D lineSegs = LineSegments3D(objPnts, temLineTops);

	for (unsigned int i=0; i<lineSegs.size(); i++) {
		temDist = lineSegs[i].DistanceToPoint(inPnt);
		if (minDist>temDist) minDist = temDist;
	}

	return minDist;
}

bool LineTopologyComplex::InsidePolygonJordan(const Vector2D& curPnt, const ObjectPoints& pts, const PointNumberList& top) const 
{
	ObjectPoints::const_iterator begPnt, pt0, pt1;
	PointNumberList::const_iterator node;
	int   num_intersections;
	double  Y_test;

	//Check if the polygon is closed and has at least three points
	if (top.begin()->Number()!=(top.end()-1)->Number()) return false;
	if (top.size() < 4) return false;

	begPnt = pts.begin();
	pt0 = begPnt+top[0].Number();
	num_intersections = 0;
	const double candX = curPnt.X();
	const double candY = curPnt.Y();
	double dx1, dx2;

	//#pragma omp parallel for private(pt1, dx1, dx2)
	for (int i=1; i<top.size(); ++i) {
		pt1 = begPnt + top[i].Number();
		dx1 = candX - pt0->X();
		dx2 = pt1->X() - candX;
		if (pt1->X() != candX && dx1*dx2>=0) {
			Y_test = (dx2*pt0->Y() + dx1*pt1->Y())/(pt1->X()-pt0->X());
			if (Y_test > candY) num_intersections++;
		}

		pt0 = pt1;
	}

	//return !(num_intersections%2);
	if ((num_intersections/2)*2 == num_intersections) return(0);
	else return(1);

	/*
	int c = 0;
	ObjectPoint *pt0, *pt1;
	PointNumberList::const_iterator node;

	// Get the first polygon point
	pt0 = pts.GetPoint(*(top.begin()));
	if (!pt0) {
		fprintf(stderr, "Error: polygon node %d is missing in the ObjectPoints3D list\n", top.begin()->Number());
		return(0);
	}

	for (node=top.begin()+1; node!=top.end(); node++) {
		pt1 = pts.GetPoint(*node);
		if( ((pt1->Y()>curPnt.Y()) != (pt0->Y()>curPnt.Y())) &&
			(curPnt.X() < (pt0->X()-pt1->X())*(curPnt.Y()-pt1->Y())/(pt0->Y()-pt1->Y()) + pt1->X()) )
			c = !c;   
		pt0 = pt1;
	}

	return(bool(c));*/
}

LineTopologiesComplex::LineTopologiesComplex(const LineTopologies& inLineTops) 
	:std::vector<LineTopologyComplex>()
{
#ifdef _DEBUG
	inLineTops.Write("debug.top");
#endif

	LineTopologies::const_iterator itrLineTop;
	LineTopologiesComplex::iterator itrComPol;
	std::multimap<int, int> mapComPolID;//polygon ID number --> complex polygon index
	typedef std::multimap<int, int>::iterator MAPITR;
	//MAPITR itrMap;
	std::pair<MAPITR, MAPITR> rangeMap;
	this->reserve(inLineTops.size());

	//outer rings
	for (itrLineTop=inLineTops.begin(); itrLineTop!=inLineTops.end(); itrLineTop++) {
		if (!itrLineTop->HasAttribute(IDNumberTag)) continue;
		if (itrLineTop->HasAttribute(HoleTag) && itrLineTop->Attribute(HoleTag)!=0)
			continue;

		int ID = itrLineTop->Attribute(IDNumberTag);
		this->push_back(LineTopologyComplex());
		itrComPol = this->begin() + this->size()-1;
		itrComPol->ReplaceOuterRing(*itrLineTop);
		mapComPolID.insert(std::make_pair(ID,this->size()-1));
	}

	//inner rings
	for (itrLineTop=inLineTops.begin(); itrLineTop!=inLineTops.end(); itrLineTop++) {
		if (!itrLineTop->HasAttribute(IDNumberTag)) continue;
		if (itrLineTop->HasAttribute(HoleTag) && itrLineTop->Attribute(HoleTag)!=1) 
			continue;

		int ID = itrLineTop->Attribute(IDNumberTag);
		rangeMap = mapComPolID.equal_range(ID);
		while (rangeMap.first != rangeMap.second)
		{
			itrComPol = this->begin() + rangeMap.first->second;
			rangeMap.first++;

			LineTopology& outRing = itrComPol->OuterRing();
			if (*itrLineTop == outRing) continue;//same polygon

			//only same building part
			//bool bIsPart0 = outRing.Label()==MAP_PAR_LABEL;
			//bool bIsPart1 = itrLineTop->Label()==MAP_PAR_LABEL;
			//if (bIsPart0!=bIsPart1) continue;
			bool bHasPart0 = outRing.HasAttribute(BuildingPartNumberTag);
			bool bHasPart1 = itrLineTop->HasAttribute(BuildingPartNumberTag);
			if ((bHasPart0 && !bHasPart1) || (!bHasPart0 && bHasPart1)) continue;
			int bldPart0 = outRing.Attribute(BuildingPartNumberTag);
			int bldPart1 = itrLineTop->Attribute(BuildingPartNumberTag);
			if (bldPart0 != bldPart1) continue;

			itrComPol->AddInnerRing(*itrLineTop);
		}
	}

	this->shrink_to_fit();
};

LineTopologies LineTopologiesComplex::ToLineTopologies() 
{
	LineTopologies outTopLines, temTopLines;
	//tempTopLines = inComplxTopLine.ToLineTopologies();

	for (int i=0; i<this->size();i++) {
		temTopLines = (*this)[i].ToLineTopologies();
		outTopLines.insert(outTopLines.end(), temTopLines.begin(), temTopLines.end());
	}
	
	return outTopLines;
}

//Dong
void LineTopologiesComplex::SetAttribute(const LineTopologyTag tag, const int value)
{
	for (int i=0; i<this->size();i++) {
		(*this)[i].SetAttribute(tag, value);
	}
}

TIN LineTopologyComplex::DeriveTIN(const ObjectPoints& boundPnts, const ObjectPoints& otherPoints)
{
	double *coordinate_list = 0;
	double *coordinate = 0;
	double *hole_list = 0;

	int *edge_list, *edge_point, num_edges;
	ObjectPoints::const_iterator point;
	LineTopologies::const_iterator polygon;
	LineTopology::const_iterator node;
	TIN tin;

	//////////////////////////////////////////////////////////////////////////
	//step 1
	// Create the coordinate list
	int coordinateNum =  m_outerRing.size()-1+otherPoints.size();
	coordinate_list = (double*) malloc(2*coordinateNum*sizeof(double));
	coordinate = coordinate_list;
	//outer ring
	for (unsigned int i=0; i<m_outerRing.size()-1; i++) {
		point = boundPnts.begin()+m_outerRing[i].Number();
		*coordinate++ = point->X();
		*coordinate++ = point->Y();
	}

	//other points
	for (point=otherPoints.begin(); point!=otherPoints.end(); point++) {
		*coordinate++ = point->X();
		*coordinate++ = point->Y();
	}

	//////////////////////////////////////////////////////////////////////////
	//step 2
	//Create the edge list of outer ring
	num_edges = m_outerRing.size();
	if (num_edges) {
		edge_list = (int*) malloc(2*num_edges*sizeof(int));
		edge_point = edge_list;
		for (int i=0; i<num_edges; i++) {
			*edge_list++ = i;
			*edge_list++ = i+1;
		}
		//last edge
		*edge_list++ = num_edges-1;
		*edge_list = 0;
	}

    TIN t;
    return t;
}


/*
Copyright 2010 University of Twente and Delft University of Technology

This file is part of the Mapping libraries and tools, developed
for research, education and projects in photogrammetry and laser scanning.

The Mapping libraries and tools are free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

The Mapping libraries and tools are distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the Mapping libraries and tools.  If not, see
<http://www.gnu.org/licenses/>.

----------------------------------------------------------------------------*/



/*
--------------------------------------------------------------------------------
Collection of functions for class Planes          

Planes &Planes::operator =(const Planes &)    Copy assignment

Initial creation
Author : George Vosselman
Date   : 03-07-2000

Update #1
Author :
Date   :
Changes:

--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
Include files                  
--------------------------------------------------------------------------------
*/
//#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include "Planes.h"

/*
--------------------------------------------------------------------------------
Copy assignment
--------------------------------------------------------------------------------
*/

Planes &Planes::operator = (const Planes &planes)
{
	if (!empty()) erase(begin(), end());
	if (!planes.empty()) insert(begin(), planes.begin(), planes.end());
	return(*this);
}

/*
--------------------------------------------------------------------------------
Erase all planes
--------------------------------------------------------------------------------
*/

void Planes::Erase()
{
	for (Planes::iterator plane=begin(); plane!=end(); plane++) 
		plane->Erase();
	erase(begin(), end());
}

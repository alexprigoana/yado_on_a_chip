
/*
    Copyright 2010 University of Twente and Delft University of Technology
 
       This file is part of the Mapping libraries and tools, developed
  for research, education and projects in photogrammetry and laser scanning.

  The Mapping libraries and tools are free software: you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the License,
                   or (at your option) any later version.

 The Mapping libraries and tools are distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
          along with the Mapping libraries and tools.  If not, see
                      <http://www.gnu.org/licenses/>.

----------------------------------------------------------------------------*/

//#include "stdafx.h"

#include <math.h>
#include <stdlib.h>
#include <algorithm>
#include "LSStructs.h"

//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
/*bool vRecord::Multiply(const std::vector<vRecord> & m, const vRecord &v)
{
	assert (!m.empty() && m.begin()->size() != 0 &&
			!v.empty() && v.size() == m.begin()->size());

	int n_rows = m.size();
	int n_cols = m.begin()->size();
	int m_rows = v.size();

	if (size() != n_rows)
		*this = vRecord(n_rows);

	for(int i = 0; i < n_rows; i++){
		(*this)[i] = 0.;
		for(int k = 0; k < n_cols; k++)
			(*this)[i] += m[i][k] * v[k];
	}
	return true;
}*/

//----------------------------------------------------------------------------

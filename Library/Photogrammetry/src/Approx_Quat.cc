
/*
    Copyright 2010 University of Twente and Delft University of Technology
 
       This file is part of the Mapping libraries and tools, developed
  for research, education and projects in photogrammetry and laser scanning.

  The Mapping libraries and tools are free software: you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the License,
                   or (at your option) any later version.

 The Mapping libraries and tools are distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
          along with the Mapping libraries and tools.  If not, see
                      <http://www.gnu.org/licenses/>.

----------------------------------------------------------------------------*/



/* Quatern.c: 
* Calculates the approximate values of the quaternions q1 q2 and q3 
* Input: 
* The essential matrix E[3][3] 
* The basic model bx, by, and bz 
* Output: 
* The quaternions q1, q2 and q3 
* Method: 
* Consider the equation (Fot I, p. 107, formula 6.41): 
* {Sb (I + Sq) - E (I - Sq)} * {Sb (I + Sq) - E(I - Sq)} 
* After developing and partially differentiate by q1, q2 and q3 arise 
* There are three equations with unknowns the quaternions q1 q2 and Q3. 
* First, the coefficients of these equations calculated And 
* quat[3][3] installed. 
* The results of the equations in vector y [3] placed. 
* Next, the routines dgeco() and dgesl() 
* Y = quat.x system solved 
* Where x is the vector to be determined quaternions. 
*/

//#include "digphot_arch.h"
//#include "stdafx.h"
#include "Eigen/Dense"

void Approx_Quaternion(double e[3][3], double bx, double by, double bz,
                       double *q1, double *q2, double *q3)
{
   double quat[3][3], y[3];
 
   /* declaraties tbv dgeco() en dgesl(): */
   int quatdim=3, ipvt[3], job=1;
   double rcond, z[3];
   
//   void dgeco_(double *, int *, int *, int *, double *, double *);
//   void dgesl_(double *, int *, int *, int *, double *, int *);

  // Calculate the coeff. of q1, q2 and q3 and placement quat [3] [3] 

   quat[0][0] = 
   -4*bx*e[1][2]      +4*bx*e[2][1]      +4*bx*bx           +4*by*e[0][2]
   +2*by*by           -4*bz*e[0][1]      +2*bz*bz           +2*e[0][1]*e[0][1]
   +2*e[0][2]*e[0][2] +2*e[1][1]*e[1][1] +2*e[1][2]*e[1][2] +2*e[2][1]*e[2][1]
   +2*e[2][2]*e[2][2];

   quat[0][1] = 
   -2*bx*e[2][0]      +2*bx*by           +2*by*e[2][1]      +2*bz*e[0][0]
   -2*bz*e[1][1]      -2*e[0][0]*e[0][1] -2*e[1][0]*e[1][1] -2*e[2][0]*e[2][1];

   quat[0][2] =  
    2*bx*e[1][0]      +2*bx*bz           -2*by*e[0][0]      +2*by*e[2][2]
   -2*bz*e[1][2]      -2*e[0][0]*e[0][2] -2*e[1][0]*e[1][2] -2*e[2][0]*e[2][2];

   quat[1][0] = 
   -2*bx*e[2][0]      +2*bx*by           +2*by*e[2][1]      +2*bz*e[0][0]
   -2*bz*e[1][1]      -2*e[0][0]*e[0][1] -2*e[1][0]*e[1][1] -2*e[2][0]*e[2][1];

   quat[1][1] = 
   -4*bx*e[1][2]      +2*bx*bx           +4*by*e[0][2]      -4*by*e[2][0]
   +4*by*by           +4*bz*e[1][0]      +2*bz*bz           +2*e[0][0]*e[0][0]
   +2*e[0][2]*e[0][2] +2*e[1][0]*e[1][0] +2*e[1][2]*e[1][2] +2*e[2][0]*e[2][0]
   +2*e[2][2]*e[2][2];

   quat[1][2] = 
    2*bx*e[1][1]      -2*bx*e[2][2]      -2*by*e[0][1]      +2*by*bz
   +2*bz*e[0][2]      -2*e[0][1]*e[0][2] -2*e[1][1]*e[1][2] -2*e[2][1]*e[2][2];

   quat[2][0] = 
    2*bx*e[1][0]      +2*bx*bz           -2*by*e[0][0]      +2*by*e[2][2]
   -2*bz*e[1][2]      -2*e[0][0]*e[0][2] -2*e[1][0]*e[1][2] -2*e[2][0]*e[2][2];
   
   quat[2][1] = 
    2*bx*e[1][1]      -2*bx*e[2][2]      -2*by*e[0][1]      +2*by*bz
   +2*bz*e[0][2]      -2*e[0][1]*e[0][2] -2*e[1][1]*e[1][2] -2*e[2][1]*e[2][2];
   
   quat[2][2] =
    4*bx*e[2][1]      +2*bx*bx           -4*by*e[2][0]      +2*by*by
   -4*bz*e[0][1]      +4*bz*e[1][0]      +4*bz*bz           +2*e[0][0]*e[0][0]
   +2*e[0][1]*e[0][1] +2*e[1][0]*e[1][0] +2*e[1][1]*e[1][1] +2*e[2][0]*e[2][0]
   +2*e[2][1]*e[2][1];

    //Calculate the terms of the vector y [3]

   y[0] = -4*bx*e[1][1] -4*bx*e[2][2] +4*by*e[0][1] +4*bz*e[0][2];
   y[1] =  4*bx*e[1][0] -4*by*e[0][0] -4*by*e[2][2] +4*bz*e[1][2];
   y[2] =  4*bx*e[2][0] +4*by*e[2][1] -4*bz*e[0][0] -4*bz*e[1][1];

   //Solve the system of equations quat.x = y 
   //Dgeco () is a routine operation for the matrix worked quat
   //For dgesl ()
   //Dgesl () is the vector x = y quat.x from the system to solve
   //The vector Y is overwritten by the outcome x,
   //Ie Q1, Q2 and Q3.
   //There dgeco () and dgesl () values have been fortranroutines
   //of variables such quatdim = 3 passed through their address 

   //#ifdef hpux
   //   dgeco(quat, &quatdim, &quatdim, ipvt, &rcond, z);
   //#else
   //   dgeco_((double *) quat, &quatdim, &quatdim, ipvt, &rcond, z);
   //#endif

   //rcond more as approaches zero, the solution is worse determined

   //#ifdef hpux
   //   dgesl(quat, &quatdim, &quatdim, ipvt, y, &job);
   //#else
   //   dgesl_((double *) quat, &quatdim, &quatdim, ipvt, y, &job);
   //#endif
   // If Job = 1 then the transpose form of quat used
   //This is necessary since dgesl () is a fortran routine.

   //   *q1 = y[0];   *q2 = y[1];   *q3 = y[2];

   Eigen::Matrix3d A;
   Eigen::Vector3d b;
   Eigen::Vector3d x;
   A(0,0)=quat[0][0]; A(0,1)=quat[0][1]; A(0,2)=quat[0][2];
   A(1,0)=quat[1][0]; A(1,1)=quat[1][1]; A(1,2)=quat[1][2];
   A(2,0)=quat[2][0]; A(2,1)=quat[2][1]; A(2,2)=quat[2][2];
   b(0) = y[0]; b(1) = y[1]; b(2) = y[2];
   x = A.fullPivLu().solve(b);
   *q1 = x(0);   *q2 = x(0);   *q3 = x(0);
}

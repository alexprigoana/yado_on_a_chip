
/*
Copyright 2010 University of Twente and Delft University of Technology

This file is part of the Mapping libraries and tools, developed
for research, education and projects in photogrammetry and laser scanning.

The Mapping libraries and tools are free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

The Mapping libraries and tools are distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the Mapping libraries and tools.  If not, see
<http://www.gnu.org/licenses/>.

----------------------------------------------------------------------------*/



/*
--------------------------------------------------------------------------------
Collection of functions for class Plane
--------------------------------------------------------------------------------
*/

/*
--------------------------------------------------------------------------------
Include files                  
--------------------------------------------------------------------------------
*/
//#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
//#include <unistd.h>
#include <string>
//#include <strings.h>
#include <math.h>
#include "Plane.h"
#include "stdmath.h"
#include <iostream>
#include "Database.h"
#include "LineTopologies.h"
#include "ObjectPoints.h"

#include <Eigen/Dense>
using namespace Eigen;

#define pi 3.1415926535797


Plane::Plane() : FeatureNumber() 
{
	coord_sum = NULL;
	offset = NULL;
	moments = NULL;

	InitialisePointers(); 
	Initialise();
}


Plane::Plane(const Vector3D& point, const Vector3D& normalVec)
	: FeatureNumber()
{
	coord_sum = NULL;
	offset = NULL;
	moments = NULL;

	double len=normalVec.SqLength();
	if( len < 0.000005 )
	{
		normal=Vector3D( 0.0, 0.0, 1.0 );
		distance = point.Z();
	}
	else {
		normal=normalVec/sqrt(len);
		distance = normal.DotProduct( point );
	}

	label = num_pts = 0;
	smallest_eigenvalue = 0.0;
	InitialisePointers();
}

Plane::Plane(const Position3D &p1, const Position3D &p2, const Position3D &p3)
{
	coord_sum = NULL;
	offset = NULL;
	moments = NULL;

	normal = (p2 - p1).VectorProduct(p3 - p1);
	if (normal.Length() > 0.0) {
		normal = normal.Normalize();
		distance = 0.0;
		distance = Distance(p1);
	}

	label = num_pts = 0;
	smallest_eigenvalue = 0.0;
}

Plane::Plane(const ObjectPoints& objPnts, const LineTopology& lineTop)
{
	if (lineTop.size()<4) {
		coord_sum = NULL;
		offset = NULL;
		moments = NULL;

		InitialisePointers(); 
		Initialise();
	} else {
		//std::vector
	}
}

void Plane::InitialisePointers()
{
	coord_sum = offset = NULL;
	moments = NULL;
}

void Plane::Initialise()
{
	normal = Vector3D();
	distance = 0.0;
	label = num_pts = 0;
	if (coord_sum != NULL) {
		delete coord_sum;
		delete offset;
		delete moments;
	}
	
	coord_sum = NULL;
	offset = NULL;
	moments = NULL;

	smallest_eigenvalue = 0.0;
}

/*--------------------------------------------------------------------------------
Copy assignment
--------------------------------------------------------------------------------*/

Plane & Plane::operator=(const Plane &pl)
{
	if (this == &pl) return *this;

	// Clear old data
	if (coord_sum != NULL) {
		delete coord_sum;
		delete offset;
		delete moments;
	}

	// Copy new data
	num = pl.num;
	normal = pl.normal;
	distance = pl.distance;
	label = pl.label;
	num_pts = pl.num_pts;
	if (pl.coord_sum != NULL) {
		coord_sum = new Vector3D(*(pl.coord_sum));
		offset    = new Vector3D(*(pl.offset));
		moments   = new double[9];
		memcpy((void *) moments, (const void *) pl.moments, 9 * sizeof(double));
		smallest_eigenvalue = pl.smallest_eigenvalue;
	}
	else {
		InitialisePointers();
		smallest_eigenvalue = 0.0;
	}
	return *this;
}

PHO_DLL bool operator == (const Plane& lhr, const Plane& rhr)
{
	Plane p0(lhr);
	Plane p1(rhr);

	double l0 = p0.Normal().Length();
	double l1 = p1.Normal().Length();
	
	if (l0 != 1.0 && l0!=0.0) {
		p0.Normal() /= l0;
		p0.Distance() /= l0;
	}

	if (l1 != 1.0 && l1!=0.0) {
		p1.Normal() /= l1;
		p1.Distance() /= l1;
	}

	if(p1.Distance()*p0.Distance() < 0.0) {
		p1.SwapNormal();
	}

	return p0.Normal() == p1.Normal() && p0.Distance() == p1.Distance();
}


/*--------------------------------------------------------------------------------
Print values of Plane parameters to screen
--------------------------------------------------------------------------------*/

void Plane::Print() const
{
	printf("\nNormal vector has parameters: X %f, Y %f, Z %f", 
		normal.X(), normal.Y(), normal.Z());
	printf("\nDistance to the origin is %f", distance);	
	printf("\nGradients are: X %f, Y %f", XGrad(), YGrad());
}

/*--------------------------------------------------------------------------------
Set normal vector 
--------------------------------------------------------------------------------*/

void Plane::SetNormal(const Vector3D &v1, const Vector3D &v2)
{
	normal = v1.VectorProduct(v2);
}

/*--------------------------------------------------------------------------------
Set distance value
--------------------------------------------------------------------------------*/

void Plane::SetDistance(double depend, const Vector3D &vec)
{
	distance = depend - (XGrad()*vec.X()) - (YGrad()*vec.Y());
}	

/*--------------------------------------------------------------------------------
Interpolate dependent value
--------------------------------------------------------------------------------*/

double Plane::Interp(double X, double Y) const
{
	double depend = distance + XGrad()*X + YGrad()*Y;
	return depend;
}

/*--------------------------------------------------------------------------------
Distance of a point to the plane
--------------------------------------------------------------------------------*/

double Plane::Distance(const Position3D &point) const
{
	return(point.X() * normal.X() + point.Y() * normal.Y() +
		point.Z() * normal.Z() - distance);
}

/*--------------------------------------------------------------------------------
Calculate Z at a given X and Y location
--------------------------------------------------------------------------------*/
double Plane::Z_At(double X, double Y, int *success) const
{
	if (normal.Z() == 0) {
		*success = 0;
		return(0.0);
	}
	*success = 1;
	return((-X * normal.X() - Y * normal.Y() + distance) / normal.Z());
}

/*--------------------------------------------------------------------------------
Project a point onto the plane
--------------------------------------------------------------------------------*/
Position3D Plane::Project(const Position3D &point) const
{
	Position3D projection;
	double     dist_point;

	dist_point = Distance(point);
	projection.X() = point.X() - dist_point * normal.X();
	projection.Y() = point.Y() - dist_point * normal.Y();
	projection.Z() = point.Z() - dist_point * normal.Z();
	return(projection);
}

/*--------------------------------------------------------------------------------
Intersect two planes
--------------------------------------------------------------------------------*/
PHO_DLL bool Intersect2Planes(const Plane &plane1, const Plane &plane2,
	Line3D &line)
{
	Vector3D normal1, normal2, direction;
	Matrix3d A; 
	Vector3d b, x, c;
//	double   a[9], b[3];

	normal1 = plane1.Normal();
	normal2 = plane2.Normal();

	/* Check angle between planes */
	if (Angle(normal1, normal2) * 45 / atan(1.0) < 0.01) return(0);

	/* Direction of the 3D line */
	direction = normal1.VectorProduct(normal2).Normalize();

	/* Set up the equations system of the two planes and one additional plane
	* going through the origin and perpendicular to the line direction.
	*
	* Note that matrix A is transposed, since Solve_Normal_Eq passes A to a
	* FORTRAN routine. Solve_Normal_Eq was meant for symmetrical matrices and
	* therefore does not transpose A as required for FORTRAN matrices.
	*/

//	a[0] = normal1.X();        a[3] = normal1.Y();        a[6] = normal1.Z();
//	a[1] = normal2.X();        a[4] = normal2.Y();        a[7] = normal2.Z();
//	a[2] = direction.X();      a[5] = direction.Y();      a[8] = direction.Z();
//	b[0] = plane1.Distance();  b[1] = plane2.Distance();  b[2] = 0.0;
//	Solve_Normal_Eq(a, b, 3);
	A(0,0) = normal1.X();		A(0,1) = normal1.Y();		A(0,2) = normal1.Z();
	A(1,0) = normal2.X();		A(1,1) = normal2.Y();		A(1,2) = normal2.Z();
	A(2,0) = direction.X();		A(2,1) = direction.Y();		A(2,2) = direction.Z();
	b(0) = plane1.Distance();	b(1) = plane2.Distance();	b(2) = 0.0;

	x = A.fullPivLu().solve(b);

	/* Set the line parameters */
	line = Line3D(x(0), x(1), x(2), direction.X(), direction.Y(), direction.Z());
	//line = Line3D(b[0], b[1], b[2], direction.X(), direction.Y(), direction.Z());
	return(1);
}

/*--------------------------------------------------------------------------------
Intersect line with plane
--------------------------------------------------------------------------------*/
PHO_DLL bool IntersectLine3DPlane(const Line3D &line, const Plane &plane,
	Position3D &point)
{
	Vector3D normal, direction;
	double   denom, scalar;

	/* Retrieve directions */

	normal = plane.Normal();
	direction = line.Direction();

	/* Check intersection angle */

	denom = normal.DotProduct(direction);
	if (denom == 0.0) return(0);

	/* Calculate intersection point */

	scalar = (plane.Distance() - line.FootPoint().DotProduct(normal)) / denom;
	point = line.Position(scalar);

	return(1);
}

/*--------------------------------------------------------------------------------
Check if plane is horizontal
--------------------------------------------------------------------------------*/
bool Plane::IsHorizontal(double err_angle) const
{
	if (Angle(Normal(), Vector3D(0, 0, 1)) <= err_angle) return(1);
	if (Angle(Normal(), Vector3D(0, 0, -1)) <= err_angle) return(1);
	return(0);
}

/*
--------------------------------------------------------------------------------
Check if plane is Vertical
--------------------------------------------------------------------------------
*/

bool Plane::IsVertical(double err_angle) const
{

	//completely vertical make product=0;
	double product=normal.DotProduct(Vector3D(0,0,1));
	if((product>0)&&product<err_angle)
		return 1;
	else if((product<0)&&product>-err_angle)
		return 1;
	else return 0;  

}
/*
--------------------------------------------------------------------------------
Add or remove a point to the plane
--------------------------------------------------------------------------------
*/

bool Plane::AddPoint(const Position3D &pos, bool recalculate)
{
	return ChangePoint(pos, true, recalculate);
}

bool Plane::RemovePoint(const Position3D &pos, bool recalculate)
{
	return ChangePoint(pos, false, recalculate);
}

bool Plane::ChangePoint(const Position3D &pos, bool add, bool recalculate)
{
	Vector3D diff;

	// Check if moment vectors exist
	if (coord_sum == NULL) {
		coord_sum = new Vector3D();
		offset    = new Vector3D();
		moments   = new double[9];
		memset((void *) moments, 0, 9 * sizeof(double));
	}

	if (num_pts == 0) *offset = pos;

	diff = pos - *offset;
	if (add) {
		num_pts++;              // Update point count
		*coord_sum += diff;     // Update coordinate sum
		for (int i=0; i<3; i++) // Update moment matrix
			for (int j=0; j<=i; j++)
				moments[i*3+j] += diff[i] * diff[j];
	}
	else {
		num_pts--;              // Update point count
		*coord_sum -= diff;     // Update coordinate sum
		for (int i=0; i<3; i++) // Update moment matrix
			for (int j=0; j<=i; j++)
				moments[i*3+j] -= diff[i] * diff[j];
	}

	if (recalculate) return Recalculate();
	return false;
}

/*--------------------------------------------------------------------------------
Recalculate plane parameters from moments
--------------------------------------------------------------------------------*/
#include "Eigen/Eigenvalues"

bool Plane::Recalculate()
{
	Vector3D centre;
	double   tmp1[3], tmp2[3], eigen_real[3], eigen_imag[3], eigen_vec[3][3],
		central_moments[9];
	int      dim=3, matz=1, smallest;

	// Check number of points
	if (num_pts < 3) return false;

	// Centre of gravity of all points
	centre = *coord_sum / (double) num_pts;

	// Derive centralised moments
	for (int i=0; i<3; i++)
		for (int j=0; j<=i; j++)
			central_moments[i*3+j] = moments[i*3+j] -
			num_pts * centre[i] * centre[j];

	// Mirror moments array to make it full
	central_moments[1] = central_moments[3];
	central_moments[2] = central_moments[6];
	central_moments[5] = central_moments[7]; 

	// Determine the eigen values and eigen vectors with Eispack
//	rg_(&dim, &dim, (double *) central_moments, eigen_real, eigen_imag, &matz,
//		(double *) eigen_vec, tmp1, tmp2, &error);

	// Select the smallest eigen value
//	smallest = (eigen_real[0] < eigen_real[1]) ? 0 : 1;
//	smallest = (eigen_real[smallest] < eigen_real[2]) ? smallest : 2;
//	smallest_eigenvalue = eigen_real[smallest];

//	normal.X() = eigen_vec[smallest][0];
//	normal.Y() = eigen_vec[smallest][1];
//	normal.Z() = eigen_vec[smallest][2];

	Eigen::Matrix3d A;
	A(0,0) = central_moments[0]; 
	A(0,1) = central_moments[1]; 
	A(0,2) = central_moments[2];
	A(1,0) = central_moments[3]; 
	A(1,1) = central_moments[4]; 
	A(1,2) = central_moments[5];
	A(2,0) = central_moments[6]; 
	A(2,1) = central_moments[7]; 
	A(2,2) = central_moments[8];

	Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> solver(A);
	Eigen::Vector3d eigenVal;
	Eigen::Matrix3d eigenVec;
	//  solver.compute();
	eigenVal = solver.eigenvalues();
	eigenVec = solver.eigenvectors();

//	smallest = eigenVal(0)<eigenVal(1)? 0:1;
//	smallest = eigenVal(smallest)<eigenVal(2)? smallest:2;

	// Take the corresponding eigen vector as normal vector
	normal.X() = eigenVec(0, 0);
	normal.Y() = eigenVec(1, 0);
	normal.Z() = eigenVec(2, 0);

	normal = normal.Normalize();

	// Derive the distance to the origin (and correct for offset)
	distance = normal.DotProduct(centre + *offset);

	return true;
}

/*--------------------------------------------------------------------------------
Derive the centre of gravity
--------------------------------------------------------------------------------*/
Position3D Plane::CentreOfGravity() const
{
	Position3D centre;

	// Check if there are points
	if (num_pts == 0) return Position3D(1.0e30, 1.0e30, 1.0e30);

	// Centre of gravity of all points
	centre.vect() = (*coord_sum / (double) num_pts) + *offset;
	return centre;
}

/*--------------------------------------------------------------------------------
Erase plane data
--------------------------------------------------------------------------------*/
void Plane::Erase()
{
	if (coord_sum) {free(coord_sum); coord_sum = NULL;}
	if (offset) {free(offset); offset = NULL;}
	if (moments) {free(moments); moments = NULL;}
	num_pts = 0;
}

/*--------------------------------------------------------------------------------
Swap the normal vector direction
--------------------------------------------------------------------------------*/
void Plane::SwapNormal()
{
	normal *= -1.0;
	distance = -distance;
}

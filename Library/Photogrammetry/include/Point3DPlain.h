#ifndef _Point3DPlain_h_
#define _Point3DPlain_h_

#include "Photogrammetry.h"
#include "Position3D.h"
#include "PointNumber.h"

//------------------------------------------------------------------------------
/// A non-stochastic point in a three-dimensional coordinate system
//------------------------------------------------------------------------------

class PHO_DLL Point3DPlain : public PointNumber, public Position3D {

  public:

    /// Default constructor
    Point3DPlain() : PointNumber(), Position3D() {}

    /// Construct from coordinates and a point number
    Point3DPlain(double x, double y, double z, int n)	
	: PointNumber(n), Position3D(x, y, z) {} 

    /// Construct from a coordinate vector and a point number
    Point3DPlain(const Vector3D &v, const PointNumber &n)
    	: PointNumber(n), Position3D(v) {}

    /// Copy constructor
    Point3DPlain(const Point3DPlain *point)
      : PointNumber(), Position3D()
    	{ x[0] = point->x[0]; x[1] = point->x[1];
    	  x[2] = point->x[2]; num = point->num;
    	}  

    /// Default destructor
    ~Point3DPlain()
      {};
};
#endif /* _Point3DPlain_h_ */   /* Do NOT add anything after this line */

#ifndef _Lines2D_h_
#define _Lines2D_h_

#include "Photogrammetry.h"
#include "Line2D.h"

//------------------------------------------------------------------------------
/// A vector of two-dimensional lines
//------------------------------------------------------------------------------

class PHO_DLL Lines2D : public std::vector<Line2D> {

  public:
    /// Default constructor
    Lines2D() : std::vector<Line2D>() {}

    /// Default destructor
    ~Lines2D()
      {if (!empty()) erase(begin(), end());}

    /// Copy assignment
    Lines2D & operator = (const Lines2D &);
};

#endif /* _Lines2D_h_ */   /* Do NOT add anything after this line */

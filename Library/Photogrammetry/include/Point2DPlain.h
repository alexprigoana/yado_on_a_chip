#ifndef _Point2DPlain_h_
#define _Point2DPlain_h_

/*--------------------------------------------------------------------------------
This file contains the definitions of the following classes:

  Point2DPlain  - A non-stochastic point in a two-dimensional coordinate system

--------------------------------------------------------------------------------*/
#include "Photogrammetry.h"
#include "Position2D.h"
#include "PointNumber.h"

//------------------------------------------------------------------------------
/// A non-stochastic point in a two-dimensional coordinate system
//------------------------------------------------------------------------------

class PHO_DLL Point2DPlain : public PointNumber, public Position2D 
{
  public:

    /// Default constructor
    Point2DPlain() : PointNumber(), Position2D() {}

    /// Construct from coordinates and a point number
    Point2DPlain(double x, double y, int n)
      : PointNumber(n), Position2D(x, y) {}; 
	
    /// Construct from a vector and a point number
    Point2DPlain(const Vector2D &v, const PointNumber &n)
      : PointNumber(n), Position2D(v) {};	

    /// Default destructor
    ~Point2DPlain() {};
};
#endif /* _Point2DPlain_h_ */   /* Do NOT add anything after this line */


/*
Copyright 2010 University of Twente and Delft University of Technology

This file is part of the Mapping libraries and tools, developed
for research, education and projects in photogrammetry and laser scanning.

The Mapping libraries and tools are free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

The Mapping libraries and tools are distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the Mapping libraries and tools.  If not, see
<http://www.gnu.org/licenses/>.

----------------------------------------------------------------------------*/



/*!
* \file
* \brief Interface to Class Vector3D - A vector in a three-dimensional coordinate system
*
*/
/*!
* \class Vector3D
* \ingroup Photogrammetry
* \brief Interface to Class Vector3D - A vector in a three-dimensional coordinate system
*
* <table width="100%" border=0 cellpadding=0 cellspacing=2 bgcolor="#eeeeee"><tr><td>
*
* \author        \Pierre_Ermes
* \date		January 1998 (Created)
*
* \remark \li Define base geometries, Vector3D.
*
* \todo None
*
* \bug None
*
* \warning None
*
*
*/

#ifndef _VECTOR3D_H_
#define _VECTOR3D_H_ 
/*--------------------------------------------------------------------
*   Project   : STW, close range photogrammetry, piping installations
*
*   File made : januari 1998
*   Author    : Pierre Ermes
*	Modified  :
*   Purpose   : Define base geometries, Vector3D.
*
*
*--------------------------------------------------------------------*/

//#include <stdlib.h>
//#include <limits.h>
#include <math.h>
#include <cstring>
//#include <strings.h>
#include <iostream>
#include "Photogrammetry.h"
#include <assert.h>
//#include <fstream> // necessary, because it includes <iosfwd>, defining istream
// as basic_istream<char>, required for insertion operator

//#include "Vector2D.h"

//#define W_DIM 3			/* world dimension */

class Vector2D;
class Orientation3D;
class Rotation3D;

/// A vector in a three-dimensional coordinate system

class PHO_DLL Vector3D 
{
protected:
	double x[3];
	//double x[0], x[1], x[2];
public:
	Vector3D() { memset(x, 0, 3*sizeof(double)); }
	Vector3D(double xv, double yv, double zv) {	x[0] = xv; x[1] = yv; x[2] = zv;}
	//	Vector3D(const double *v) { x[0] = v[0]; x[1] = v[1]; x[2] = v[2]; }
	Vector3D(const Vector3D &v) {memcpy(x, v.x, 3*sizeof(double));}
	Vector3D(const Vector2D &v, double z);
	~Vector3D() {}

	double X() const { return x[0]; }
	double Y() const { return x[1]; }
	double Z() const { return x[2]; }

	//double operator[] (int i) const { return x[i]; }

	//double &X(int i) { return x[i]; }
	double &X() { return x[0]; }
	double &Y() { return x[1]; }
	double &Z() { return x[2]; }

	double operator[] (int i) const {assert(i>=0 && i<3); return x[i];}
	double & operator[] (int i) {assert(i>=0 && i<3); return x[i];}

	/// Partial derivative of a translation with respect to vector element i.
	/** @param i Index of vector element, should be 0, 1, or 2. */
	Vector3D PartialDeriv(int i) const;
	double SqLength() const;
	/// Euclidian length
	double Length() const;
	double SqLength2D() const;
	/// Euclidian length of first two elements.
	double Length2D() const;
	Vector3D Normalize() const;
	const Vector3D &vect() const { return *this; }
	Vector3D &vect() { return *this; }
	Vector3D &operator = (const Vector3D &v);

	Vector3D VectorProduct(const Vector3D &v) const;
	double DotProduct(const Vector3D &v) const;
	Vector3D Align(const Vector3D &v) const;
	int AbsLargest() const;

	/// Compute two vectors that are perpendicular to each other and this one.
	/** Before invoking this function, be sure that 'this' is normalised. */
	void PerpendicularVectors(Vector3D &xv, Vector3D &yv) const;


	const Vector3D &operator += (const Vector3D &v);
	const Vector3D &operator -= (const Vector3D &v);
	const Vector3D &operator *= (double d);
	const Vector3D &operator *= (const Rotation3D &r);
	const Vector3D &operator *= (const Orientation3D &o);
	Vector3D &operator *= (Orientation3D &o);
	const Vector3D &operator /= (double d);
	PHO_DLL friend Vector3D operator +(const Vector3D &v1, const Vector3D &v2);
	PHO_DLL friend Vector3D operator -(const Vector3D &v1, const Vector3D &v2);
	PHO_DLL friend Vector3D operator *(const Vector3D &v, double d);
	PHO_DLL friend Vector3D operator *(double d, const Vector3D &v);
	PHO_DLL friend Vector3D operator /(const Vector3D &v, double d);
	PHO_DLL friend Vector3D operator *(const Rotation3D &r, const Vector3D &v);
	PHO_DLL friend Vector3D operator *(const Orientation3D &o, const Vector3D &v);
	PHO_DLL friend Vector3D operator *(Orientation3D &o, Vector3D &v);
	PHO_DLL friend bool operator ==(const Vector3D &v1, const Vector3D &v2);
	PHO_DLL friend bool operator <(const Vector3D &v1, const Vector3D &v2);
	Vector3D Add(const Vector3D &v) const {return(*this + v);}

	double Direction2D(const Vector3D &dest) const;
	double Angle2D(const Vector3D &p1, const Vector3D &p2) const;
	const Vector2D vect2D() const;

	PHO_DLL friend double Angle(const Vector3D &v1, const Vector3D &v2);


	/* Additions for Piper */
	///Construct from angles
	Vector3D(double phi, double theta)  {
		x[0] = cos(phi)*sin(theta);
		x[1] = sin(phi)*sin(theta);
		x[2] = cos(theta);
	}

	/// Print the vector
	void PrintVector();
	bool Unity();
	double Sum() const;
	double AbsMax(int* element) const;

	PHO_DLL friend std::istream &operator>>(std::istream &is, Vector3D &v)
	{ return is >> v.x[0] >> v.x[1] >> v.x[2]; }
	PHO_DLL friend std::ostream &operator<<(std::ostream &os, const Vector3D &v)
	{ return os << v.x[0] << ' ' << v.x[1] << ' ' << v.x[2]; }
	inline	bool IsHorizontal(double angle_tolerance) const;
};


#endif

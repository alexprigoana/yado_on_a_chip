#ifndef _LineNumber_h_
#define _LineNumber_h_

#include "Photogrammetry.h"
#include "FeatureNumber.h"

class PHO_DLL LineNumber : public FeatureNumber 
{

  public:

    /// Constructor with initialisation
    LineNumber(int num_src = 0)
      : FeatureNumber(num_src) {}

    /// Copy constructor
    LineNumber(const LineNumber &n)
      : FeatureNumber(n.num) {}

    /// Default constructor
    ~LineNumber() {};
      
    // Not documented. Usage of NumberRef should be preferred
    LineNumber &numb()
      { return *this; }    
      
    // Not documented. Usage of NumberRef const should be preferred
    const LineNumber &numb() const
      { return *this; }    
      
    /// Return the writable reference
    LineNumber &NumberRef()
      { return *this; }    
      
    /// Return the readable reference
    const LineNumber &NumberRef() const
      { return *this; }    
};
#endif 

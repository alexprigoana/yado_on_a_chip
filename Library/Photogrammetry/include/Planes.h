#ifndef _Planes_h_
#define _Planes_h_

#include <vector>
#include "Plane.h"
#include "Photogrammetry.h"

//------------------------------------------------------------------------------
/// A vector of planes
//------------------------------------------------------------------------------

class PHO_DLL Planes : public std::vector<Plane> 
{

public:
	/// Default constructor
	Planes() : std::vector<Plane>() {}

	/// Default destructor
	~Planes() {Erase();}

	/// Copy assignment
	Planes & operator = (const Planes &);

	/// Erase all planes
	void Erase();
};
#endif /* _Planes_h_ */   /* Do NOT add anything after this line */

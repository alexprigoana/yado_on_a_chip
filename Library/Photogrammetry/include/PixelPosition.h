#ifndef _PixelPosition_h_
#define _PixelPosition_h_

#include "Photogrammetry.h"
#include "Position2D.h"
class InteriorOrientation;

//------------------------------------------------------------------------------
/// A position in a digital image
//------------------------------------------------------------------------------

class PHO_DLL PixelPosition : public Position2D {

  public:

    /// Default constructor
    PixelPosition() : Position2D() {}

    /// Construct from a row and column coordinate
    PixelPosition(double r, double c) : Position2D(r, c) {}
	
    /// Construct from a two-dimensional vector 
    PixelPosition(const Vector2D &vec) : Position2D(vec) {}
    	
    /// Construct by transforming a position in a camera coordinate system into the image coordinate system
    /** @param campos Position in a camera coordinate system
        @param intor  Interior orientation of the camera
    */
    PixelPosition(const Position2D *campos, const InteriorOrientation *intor);

    /// Copy constructor
    PixelPosition(const PixelPosition &pos) : Position2D()
        { vect() = pos.vect(); }   
	
    /// Destructor
    ~PixelPosition() {};

    /// Copy assignament
    PixelPosition& operator=(const PixelPosition &pos)
        { vect() = pos.vect(); return *this; }
      
    /// Return the readable row number
    double Row() const
      { return x[0]; }	

    /// Return the readable column number
    double Col() const
      { return x[1]; }	

    /// Return the readable column number
//    double Column() const
//      { return x[1]; }	

    /// Return the writable row number
    double &Row()
      { return x[0]; }      

    /// Return the writable column number
    double &Col()
      { return x[1]; }

    /// Return the writable column number
//    double &Column()
//      { return x[1]; }
};
#endif /* _PixelPosition_h_ */   /* Do NOT add anything after this line */

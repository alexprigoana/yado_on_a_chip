#ifndef _vRecord_H_
#define _vRecord_H_

#include <vector>
#include <assert.h>
#include <numeric>
#include "Photogrammetry.h"
#include "Vector3D.h"
#include "sortStruct.h"
#include <algorithm>


template <class T> bool cmpGT(T, T);
template <class T> bool cmpLT(T, T);
template <class T> bool cmpGE(T, T);
template <class T> bool cmpLE(T, T);
template <class T> bool cmpEQ(T, T);
template <class T> bool cmpNE(T, T);

template<class T>
class FRSvector : public std::vector<T>
{
public:

    typedef T type;

	// constructor, specify number of elements and their value
	// default constructor allocates no space
	FRSvector(int i = 0, T val = (T)0) : std::vector<T>()
        { assert( i>= 0 ); this->reserve( i ); 
          for (int j = 0; j < i; j++) this->push_back(val); }

	// constructor, make a copy of a Vector3D
    FRSvector(const Vector3D & rhs) : std::vector<T>()
        { Erase();
      this->reserve(3);
          //ALEX: is this what we want ?? for sure?
      push_back((T)rhs.X()); push_back((T)rhs.Y()); push_back((T)rhs.Z()); }

	/// constructor with a C array to the elements
	FRSvector(int i, const T* vals);

	/// destructor
	~FRSvector(){ Erase(); }

	/// copy constructor and assignment operator are not implemented
	/// these are inhereted from the parent class

	/// Conversion to a Vector3D
	/// sloppy set to false checks if vector size is 3; true handles any 
	/// case starting with the first element of *this
	Vector3D AsVector3D(bool sloppy = false);

	/// what is this for ????
	const FRSvector<T> & Vector() const {return (*this);}

	/// set the size of a vector (define it)
	void Define(int nElements, const T& val=0){
		assert(nElements > 0);
		
		if (this->size() == nElements){
			*this = val;
			return;
		}
		else{
			Erase();
			this->reserve(nElements);
			for(int j = 0; j < nElements; j++) 
                this->push_back(val);
		}

	}

	/// delete elements
    void Erase() {if (!this->empty()) this->erase(this->begin(), this->end());}

	/// assign rhs to all elements of the vector
	FRSvector<T> & operator = (T rhs);

	/// add rds to *this
	FRSvector<T> & operator += (const FRSvector<T> &rhs);

	/// add rhs to *this and return the resulting vectors
	FRSvector<T> operator + (const FRSvector<T> &rhs)const;

	/// multiply *this transposed with a matrix (vector of FRSvector)
	/// return the result
	FRSvector<T> operator * (const std::vector< FRSvector<T> > &)const;

	/// subtract rhs from *this and return the resulting vector
	FRSvector<T> operator - (const FRSvector<T> &rhs)const;

	/// subtract rhs from *this
	FRSvector<T> & operator -= (const FRSvector<T> &rhs);

	/// return vector = *this * val
	FRSvector<T> operator * (T val) const;

	/// multiply all elements by val
	FRSvector<T> & operator *= (T val);

	/// divide all elements by val
	FRSvector<T> & operator /= (T val);

	/// multiply rhs with *this
	/// Multiplication itemwise NOT DOT PRODUCT
	FRSvector<T> & operator *= (const FRSvector<T> &rhs);

	/// compute the (euclidean) norm
    /// possibly augmented by weights, given in W
    /// return value is always of type double, irrespective of typename T
	double Norm(const FRSvector<T> *W = NULL) const;

	/// compute the (euclidean) square of the norm
    /// possibly augmented by weights, given in W
     /// return value is always of type double, irrespective of typename T
	double NormSquare(const FRSvector<T> *W = NULL) const;
	
	/// normalize vector (divide by its Norm())
	void Normalize();

	/// compute the dot product (inner product) of
	/// *this and rhs. Return 0 if lengths are incompatible.
	T DotProduct(const FRSvector<T> &rhs) const;

	/// compute the minimum and maximum value (considering signs) of *this
	void MinMaxValues(T &minVal, T &maxVal) const;

	/// computing the Median value of an array
	double Median(bool isAbsolute = false) const;

	/// Returing the sum of all elements
	T Sum() const;

	/// Returing the average, passing back the Std.
	/// for Std. divide by size()-1, if only one element is given std=0.0
	double Stat(double &std) const;

	/// Computes a histogram. The lowest Class starts at lowBound, the 
	/// highest finishes at lowBound+binWidth*numBins, 
    /// the number of classes is numClasses, their width is binWidth.  
	/// Works for floating point and integer types of T. For latter
	/// the integer division (X-lowBound)/binWidth is the problematic part.
	/// The histogram is stored in histo, the return value are the class
	/// boundaries.
	/// below and above give the freuqency of the values out of bounds.
	FRSvector<T> Histogram( FRSvector<int>& histo,
                            const T& lowBound, const T& binWidth, 
                            int numBins,
                            int& numBelow, int& numAbove )const;

	/// Computes a histogram. If numClasses is even, zero is on the
	/// boundary between two classes, if numClasses is odd, it is the
	/// center of a class. The total number of classes is numClasses+2, 
	/// providing classes that range to infinity on either end.
	/// Calls the above Histogram method.
    /// May give unexpected results for int-types.
	FRSvector<T> Histogram( FRSvector<int>& histo,
                            int numBins, const T& binWidth,
                            int& numBelow, int& numAbove )const;

	/// Computes a histogram with classes of width classWidth. Concerning
	/// the default arguments for lower and upper boundary the rules are:
	/// low < upp     ... use supplied values
	/// low = upp = 0 ... make histogram starting from zero so that each
	///                   element will fall in a proper class
	/// low > upp     ... make histogram starting at lowest value in the 
	///                   vector, ending at the highest value
	/// in any case two classes ranging to + and -infinity, resp. will be 
	/// included.
	FRSvector<T> Histogram( FRSvector<int>& histo,
                            const T& binWidth, const T& lowBound = (T)0,
                            const T& uppBound = (T)0 )const;

	/// Changing the vector elements to their absolute values 
	void Absolute();

	/// Generating a signs vector (-1, 0, 1), each element is 
	/// turned into -1, 0, 1 depending on its sign. Values within 
	/// +- EPS are turned to zero
	FRSvector<T> Signs(T EPS=(T)1.e-6) const;

	/// Sorting the vector and return an index list to the original vector
	FRSvector<T> Sort(std::vector<int> &indices) const;

	/// Returning differences between consecutive elements in the vector
	FRSvector<T> AdjacentDifference() const;

	/// Counting occurance of a phenomenon in the vector
	/// where PFi2D can receive the operator that check the phenomenon
	/// returns indices of occurences
	FRSvector<int> Count(T value, bool (*op)(T, T) = cmpEQ, int fromItem = 0);

    /// count how often the phenomenon T is in the vector, where equality
    /// is related to the function cmpEQ. If the second parameter is 
    /// specified, the return value contains the indices of the equal 
    /// vector elements. Optionally, comparison can start not from the 
    /// very first item, but from fromItem.
	int CountOccurrences (T value, FRSvector<int> * = NULL, 
	                           bool (*op)(T, T) = cmpEQ, int fromItem = 0);

    /// return a vector that only has the unique values which can be found
    /// in *this. The operator != is used to determine, if two elements 
    /// are different.
    FRSvector<T> UniqueValues();

	/// add the elements of v to *this, starting at position i0
	/// return false on impossiblity
	bool AddPartial(const FRSvector<T> & v, int i0=0);

	/// replace the elements of v to *this, starting at position i0
    /// return false on impossiblity
    bool ReplacePartial(const FRSvector<T> & v, int i0=0);

	/// write as a row vector, preceeded by description
	void Print(char * description = NULL, char *format = NULL) const;

	/// write to stream s, just in the same style as vMatrix
	std::ostream& WriteToStream(std::ostream& s)const;
};

template<class T>
Vector3D FRSvector<T>::AsVector3D(bool sloppy)
{
	if (!sloppy) 
		assert (this->size() == 3);
	if (this->size() >= 3)
		return Vector3D( (*this)[0], (*this)[1], (*this)[2]);
	else if (this->size() == 2) 
		return Vector3D((*this)[0], (*this)[1], 0);
	else if (this->size() == 1) 
		return Vector3D((*this)[0], 0, 0);
	else
		return Vector3D();
}
//----------------------------------------------------------------------------
template<class T>
FRSvector<T>::FRSvector(int i, const T* vals) : std::vector<T>()
{
	assert( i>0 ); assert( vals );
	this->reserve( i ); 
	for( int j=0; j<i; j++ )
		this->operator[](j) = vals[j];
}
//----------------------------------------------------------------------------
template<class T>
FRSvector<T>& FRSvector<T>::operator = (T rhs)
{
	if (!this->empty())
		for (int i = 0; i < this->size(); i++)
			(*this)[i] = rhs;
	return *this;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> & FRSvector<T>::operator += (const FRSvector<T> &rhs)
{
	assert(!this->empty() && !rhs.empty() && rhs.size() == this->size());

	typename FRSvector<T>::const_iterator item;
	int i;
	for (item = rhs.begin(), i = 0; item != rhs.end(); item++, i++)
		(*this)[i] += *item;
	return *this;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> & FRSvector<T>::operator -= (const FRSvector<T> &rhs)
{
	assert( !this->empty() && !rhs.empty() && rhs.size() == this->size() );

	typename FRSvector<T>::const_iterator item;
	int i;

	for (item = rhs.begin(), i = 0; item != rhs.end(); item++, i++)
		(*this)[i] -= *item;
	return *this;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> & FRSvector<T>::operator *= (const FRSvector<T> &rhs)
{
	assert( !this->empty() && !rhs.empty() && rhs.size() == this->size() );

	typename FRSvector<T>::const_iterator item;
	int i;

	for (item = rhs.begin(), i = 0; item != rhs.end(); item++, i++)
		(*this)[i] *= *item;
	return *this;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> & FRSvector<T>::operator *= (T val)
{
	assert( !this->empty() );

	for (int i = 0; i < this->size(); i++)
		(*this)[i] *= val;
	return *this;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> & FRSvector<T>::operator /= (T val)
{
	assert( !this->empty() && val != 0. );

	for (int i = 0; i < this->size(); i++)
		(*this)[i] /= val;
	return *this;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> FRSvector<T>::operator * (T val) const
{
	assert( !this->empty() );

	FRSvector<T> res(this->size());
	for (int i = 0; i < this->size(); i++)
		res[i] = (*this)[i] * val;
	return res;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> FRSvector<T>::operator * (const std::vector< FRSvector<T> > & m)const
{
	assert( this->size() == m.size() );

	int n_items = m.begin()->size();
	FRSvector<T> res(n_items);
	for (int i = 0; i < n_items; i++)
		for (int j = 0; j < this->size(); j++)
			res[i] += (*this)[j] * m[j][i];
	return res;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> FRSvector<T>::operator + (const FRSvector<T> &rhs)const
{
	assert( !this->empty() && this->size() == rhs.size() );

	FRSvector<T>	res;
	res.reserve(this->size());
	for(int i = 0; i < this->size(); i++)
		res.push_back((*this)[i] + rhs[i]);
	return res;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> FRSvector<T>::operator - (const FRSvector<T> &rhs)const
{
	assert( !this->empty() && this->size() == rhs.size() );

	FRSvector<T>	res;
	res.reserve(this->size());
	for(int i = 0; i < this->size(); i++)
		res.push_back((*this)[i] - rhs[i]);
	return res;
}

//----------------------------------------------------------------------------
template<class T>
double FRSvector<T>::NormSquare(const FRSvector<T> *W) const
{
	double norm = 0.;
	if (W != NULL && !W->empty() && W->size() == this->size())
		for (int i = 0; i < this->size(); i++)
			norm += (*this)[i] * (*this)[i] * (*W)[i];
	else
		for (int i = 0; i < this->size(); i++)
			norm += (*this)[i] * (*this)[i];
	return norm;
}
//----------------------------------------------------------------------------

template<class T>
double FRSvector<T>::Norm(const FRSvector<T> *W) const
{
	double norm = 0.;
	if (W != NULL && !W->empty() && W->size() == this->size())
		for (int i = 0; i < this->size(); i++)
			norm += (*this)[i] * (*this)[i] * (*W)[i];
	else
		for (int i = 0; i < this->size(); i++)
			norm += (*this)[i] * (*this)[i];
	return sqrt( norm );
}

template<class T>
void FRSvector<T>::Normalize()
{
	assert( !this->empty() );

	double length = Norm();
	assert( length ); // this should typically be a throw statement
	// rather than an assertion
	*this /= length;
}
//----------------------------------------------------------------------------
template<class T>
T FRSvector<T>::DotProduct(const FRSvector<T> &rhs)const
{
	assert( !this->empty() && this->size() == rhs.size() );

	T norm = 0.;
	for (int i = 0; i < this->size(); i++)
		norm += (*this)[i] * rhs[i];
	return norm;
}

//----------------------------------------------------------------------------
template<class T>
void FRSvector<T>::MinMaxValues(T &minVal, T &maxVal)const
{
	typename FRSvector<T>::const_iterator index;

	assert(!this->empty());

	index = std::min_element(this->begin(), this->end());
	minVal = *index;
	index = std::max_element(this->begin(), this->end());
	maxVal = *index;
}

//----------------------------------------------------------------------------^M
template<class T>
double FRSvector<T>::Median(bool isAbsolute) const
{
	assert(!this->empty());

	FRSvector<T> tmp(*this);
	if (isAbsolute == true) tmp.Absolute();

    std::sort(tmp.begin(), tmp.end());

	long medVal = (long)ceil( tmp.size()/2.0 );

	if (tmp.size() % 2 == 1)
		return (double)(   *(tmp.begin() + medVal)     );
	else
		return (double)( ( *(tmp.begin() + medVal - 1) + 
		*(tmp.begin() + medVal    ) ) / 2. );
}

//----------------------------------------------------------------------------
template<class T>
void FRSvector<T>::Absolute()
{
	typename FRSvector<T>::iterator itr;
	for (itr =  this->begin(); itr != this->end(); itr++)
		*itr = (T)fabs(*itr);
}

//----------------------------------------------------------------------------
template<class T>
T FRSvector<T>::Sum() const
{
	T total = (T)0;
	for (typename FRSvector<T>::const_iterator itr =  this->begin();
		itr != this->end(); itr++)
		total += *itr;

	return total;
}

//----------------------------------------------------------------------------
template<class T>
double FRSvector<T>::Stat(double &std) const
{
	assert (!this->empty());

	double avg = Sum()/(double) this->size();

	// Unbiased std. estiamte
	std  = (this->size() == 1) 
		? 0
		: sqrt( (this->NormSquare() - (double) this->size() * avg*avg ) /
		((double)(this->size() - 1)));

	return avg;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> FRSvector<T>::Histogram( FRSvector<int>& histo,
	const T& lowBound, const T& binWidth,
	int numBins, 
	int& numBelow, int& numAbove )const
{
	histo.resize( numBins, 0 );
	FRSvector<T> bounds( numBins+1 );
	numBelow=numAbove=0;

	typename FRSvector<T>::const_iterator itr;
	for( itr=this->begin(); itr!=this->end(); itr++ )
	{
		int index = (int)floor( ((*itr)-lowBound) / binWidth );
		if( index<0 ) 
			numBelow++;
		else if( index>=numBins )
			numAbove++;
		else
			histo[index]++;
	}

	for( int i=0; i<=numBins; i++ )
		bounds[i] = i*binWidth + lowBound;

	return bounds;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> FRSvector<T>::Histogram( FRSvector<int>& histo,
	int numBins,
	const T& binWidth,
	int& numBelow, int& numAbove )const
{
	T low( -binWidth*(numBins/(T)2) );
	return Histogram( histo, low, binWidth, numBins, numBelow, numAbove );
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> FRSvector<T>::Histogram( FRSvector<int>& histo,
	const T& binWidth, const T& lowBound, 
	const T& uppBound )const
{
	T min, max;
	int below, above;
	int numCl;
	if( lowBound < uppBound ) 
	{
		min = lowBound;
		max = uppBound;
		numCl = (int)ceil( (max-min)/binWidth );
	}
	else
	{
		MinMaxValues( min, max );
		if( lowBound == (T)0 && uppBound == (T)0 )
			min = (T)0; // use only max of data
		numCl = (int)ceil( (max-min)/binWidth );
		if( max == min+numCl*binWidth )
			numCl += 1;
	}
	return Histogram( histo, min, binWidth, numCl, below, above );
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> FRSvector<T>::Signs(T tEPS) const
{
	FRSvector<T> signs(*this);
	typename FRSvector<T>::iterator itr;
	for (itr =  signs.begin(); itr != signs.end(); itr++){
		if( (T)fabs(*itr) < tEPS ) *itr =  0.;
		else if (*itr > 0)        *itr =  1.;
		else if (*itr < 0)        *itr = -1.;
	}
	return signs;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> FRSvector<T>::Sort(std::vector<int> &indices) const
{
	std::vector< sortStruct<T> > tmp;
    typename std::vector< sortStruct<T> >::iterator tmpItr;
	tmp.reserve(this->size());

	int i = 0;
    typename FRSvector<T>::const_iterator itr;
	for (itr = this->begin(); itr != this->end(); itr++, i++)
		tmp.push_back(sortStruct<T>(*itr, i));
	std::sort(tmp.begin(), tmp.end());

	FRSvector<T> result;
	result.reserve(this->size());
	if (!indices.empty()) 
		indices.erase(indices.begin(), indices.end());
	indices.reserve(this->size());

	for (tmpItr = tmp.begin(); tmpItr != tmp.end(); tmpItr++){
		result.push_back(tmpItr->Item());
		indices.push_back(tmpItr->Index());
	}
	return result;
}


//----------------------------------------------------------------------------
template<class T>
FRSvector<T> FRSvector<T>::AdjacentDifference() const
{
	assert( this->size() > 1 );

	FRSvector<T> result(this->size());
	std::adjacent_difference(this->begin(), this->end(), result.begin());

	return result;
}

//----------------------------------------------------------------------------
template<class T>
bool FRSvector<T>::AddPartial(const FRSvector<T> & v, int i0)
{
	assert( this->size() > 1 );
	if( i0 + v.size() > this->size() )
		return false;
	for( int i=0; i<v.size(); i++ )
		(*this)[i+i0] += v[i];
	return true;
}

//----------------------------------------------------------------------------
template<class T>
bool FRSvector<T>::ReplacePartial(const FRSvector<T> & v, int i0)
{
	assert( this->size() > 1 );
	if( i0 + v.size() > this->size() )
		return false;
	for( int i=0; i<v.size(); i++ )
		(*this)[i+i0] = v[i];
	return true;
}

//----------------------------------------------------------------------------
template<class T>
void FRSvector<T>::Print(char * description, char *format) const
{
	if (this->empty())
		return;
	typename FRSvector<T>::iterator elem;

	char * dformat = "%.3f ";
	char * pformat = (format == NULL)? dformat : format;
	if (description != NULL)
		printf ("%s = ", description);
	for (elem = this->begin(); elem != this->end(); elem++)
		printf(pformat, *elem);
	printf("\n");
}

//----------------------------------------------------------------------------
template<class T>
std::ostream& FRSvector<T>::WriteToStream(std::ostream& s)const
{
	s << this->size() << "\n";
	for( int r=0; r<this->size(); r++ )
	{
		s << (*this)[r] << "  ";
	}
	s << "\n";
	return s;
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<int> FRSvector<T>::Count(T value, bool (*op)(T, T), int fromItem)
{
	FRSvector<int> indices;
	int i = 0;
	typename FRSvector<T>::iterator itr = this->begin();

	if (fromItem > 0 && fromItem < this->size())
	{ i += fromItem; itr += fromItem;}

	for (; itr != this->end(); itr++, i++)
		if (op(*itr, value))
			indices.push_back(i);

	return indices;
}

//----------------------------------------------------------------------------
template<class T>
int FRSvector<T>::CountOccurrences(T value, FRSvector<int> *indices, 
	bool (*op)(T, T), int fromItem)
{
	int count = 0;
	int i = 0;
	typename FRSvector<T>::iterator itr = this->begin();

	if (indices != NULL && !indices->empty())
		indices->erase(indices->begin(), indices->end());

	if (fromItem > 0 && fromItem < this->size())
	{ i += fromItem; itr += fromItem;}

	for (; itr != this->end(); itr++, i++)
		if (op(*itr, value)){
			if (indices != NULL) indices->push_back(i);
			count++;
		}

		return count;
}

//----------------------------------------------------------------------------

typedef FRSvector<double> vRecord;
typedef FRSvector<int> vVector;
typedef FRSvector<int>  intVector;
typedef FRSvector<long> vecLong;

template<class T>
FRSvector<T> FRSvector<T>::UniqueValues()
{
	intVector indices;
	intVector::iterator itr;
	FRSvector<T> result;

	FRSvector<T> tmp = Sort(indices);

	result.push_back(*(this->begin() + indices[0]));
	for (itr = indices.begin() + 1; itr != indices.end(); itr++){
		if (*(this->begin() + *itr) != *(this->begin() + *(itr - 1)))
			result.push_back(*(this->begin() + *itr));
	}
	return result;
}

/*
Vector3D vRecord::AsVector3D(bool sloppy)
{
	if (!sloppy) 
		assert (size() == 3);
	if (size() >= 3)
		return Vector3D( (*this)[0], (*this)[1], (*this)[2]);
	else if (size() == 2) 
		return Vector3D((*this)[0], (*this)[1], 0);
	else if (size() == 1) 
		return Vector3D((*this)[0], 0, 0);
	else
		return Vector3D();
}
//----------------------------------------------------------------------------
vRecord & vRecord::operator = (double rhs)
{
	if (!empty())
		for (int i = 0; i < size(); i++)
			(*this)[i] = rhs;
	return *this;
}

//----------------------------------------------------------------------------
vRecord & vRecord::operator += (const vRecord &rhs)
{
	vRecord::const_iterator item;
	int i;
	assert( !empty() && !rhs.empty() && rhs.size() == size() );
	//	if (!empty() && !rhs.empty() && rhs.size() == size())
	for (item = rhs.begin(), i = 0; item != rhs.end(); item++, i++)
		(*this)[i] += *item;
	return *this;
}

//----------------------------------------------------------------------------
vRecord & vRecord::operator -= (const vRecord &rhs)
{
	vRecord::const_iterator item;
	int i;
	assert( !empty() && !rhs.empty() && rhs.size() == size() );
	//	if (!empty() && !rhs.empty() && rhs.size() == size())
	for (item = rhs.begin(), i = 0; item != rhs.end(); item++, i++)
		(*this)[i] -= *item;
	return *this;
}

//----------------------------------------------------------------------------
vRecord & vRecord::operator *= (const vRecord &rhs)
{
	vRecord::const_iterator item;
	int i;
	if (!empty() && !rhs.empty() && rhs.size() == size())
		for (item = rhs.begin(), i = 0; item != rhs.end(); item++, i++)
			(*this)[i] *= *item;
	return *this;
}

//----------------------------------------------------------------------------
vRecord & vRecord::operator *= (double val)
{
	if (!empty())
		for (int i = 0; i < size(); i++)
			(*this)[i] *= val;
	return *this;
}

//----------------------------------------------------------------------------
vRecord & vRecord::operator /= (double val)
{
	if (!empty() && val != 0.)
		for (int i = 0; i < size(); i++)
			(*this)[i] /= val;
	return *this;
}

//----------------------------------------------------------------------------
vRecord vRecord::operator * (double val) const
{
	vRecord res(size());
	if (!empty())
		for (int i = 0; i < size(); i++)
			res[i] = (*this)[i] * val;
	return res;
}

//----------------------------------------------------------------------------
vRecord vRecord::operator * (const std::vector<vRecord> & m)const
{
	if (size() != m.size())
		return vRecord();

	int n_items = m.begin()->size();
	vRecord res(n_items);
	for (int i = 0; i < n_items; i++)
		for (int j = 0; j < size(); j++)
			res[i] += (*this)[j] * m[j][i];
	return res;
}


//----------------------------------------------------------------------------
vRecord vRecord::operator + (const vRecord &rhs)const
{
	vRecord	res;

	if (empty() || size() != rhs.size())
		return res;

	res.reserve(size());
	for(int i = 0; i < size(); i++)
		res.push_back((*this)[i] + rhs[i]);

	return res;
}

//----------------------------------------------------------------------------
vRecord vRecord::operator - (const vRecord &rhs)const
{
	vRecord	res;

	if (empty() || size() != rhs.size())
		return res;

	res.reserve(size());
	for(int i = 0; i < size(); i++)
		res.push_back((*this)[i] - rhs[i]);

	return res;
}

//----------------------------------------------------------------------------
double vRecord::Norm(const vRecord *W) const
{
	double norm = 0.;
	if (W != NULL && !W->empty() && W->size() == size())
		for (int i = 0; i < size(); i++)
			norm += (*this)[i] * (*this)[i] * (*W)[i];
	else
		for (int i = 0; i < size(); i++)
			norm += (*this)[i] * (*this)[i];
	return sqrt( norm );
}

void vRecord::Normalize()
{
	double length = Norm();
	assert( length );
	*this /= length;
}
//----------------------------------------------------------------------------
double vRecord::DotProduct(const vRecord &rhs) const
{
	if (empty() || size() != rhs.size())
		return 0;

	double norm = 0.;
	for (int i = 0; i < size(); i++)
		norm += (*this)[i] * rhs[i];
	return norm;
}

//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
void vRecord::MinMaxValues(double &minVal, double &maxVal) const
{
	vRecord::const_iterator index;
	index = std::min_element(begin(), end());
	minVal = *index;
	index = std::max_element(begin(), end());
	maxVal = *index;
}

//----------------------------------------------------------------------------
void vRecord::Print(char * description, char *format) const
{
	if (empty())
		return;
	vRecord::const_iterator elem;
	if (description != NULL)
		printf ("%s = ", description);
	for (elem = begin(); elem != end(); elem++)
		printf ("%.3f ", *elem);
	printf("\n");
}

std::ostream& vRecord::WriteToStream(std::ostream& s)const
{
	s << size() << "\n";
	for( int r=0; r<size(); r++ )
	{
		s << (*this)[r] << "  ";
	}
	s << "\n";
	return s;
}*/

//#include "vRecord.tcc"

#endif

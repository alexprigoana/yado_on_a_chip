
/*
Copyright 2010 University of Twente and Delft University of Technology

This file is part of the Mapping libraries and tools, developed
for research, education and projects in photogrammetry and laser scanning.

The Mapping libraries and tools are free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

The Mapping libraries and tools are distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the Mapping libraries and tools.  If not, see
<http://www.gnu.org/licenses/>.

----------------------------------------------------------------------------*/



#ifndef _VMATRIX_
#define _VMATRIX_

#include "LSStructs.h"
#include <Rotation2D.h>
#include <Rotation3D.h>

typedef enum Matrix_type {
	RankDef = false, /// Incremental Least Squares solution
	FullRank = true /// One step Least Squares solution
} MatrixType;

typedef enum Inversion_type {
	STANDARD = 0, SVD = 1
} InversionType;

class Rotation2D;
class Rotation3D;

// The declaration of friend functions is required before friend declaration
// in FRSmatrix class definition. This also requires a first short declaration
// of the FRSmatrix template class itself.
// George Vosselman, 20-Mar-2005
template <class T>
class FRSmatrix;

// The following two declarations were moved from the end of this file to here.
template<class T>
std::ostream& operator<< ( std::ostream& s, const FRSmatrix<T>& mat );

template<class T>
T MultRowColumn( const FRSmatrix<T>& matrix1, int row1, 
	const FRSmatrix<T>& matrix2, int col2 );
// End of modifications (GV)

extern "C" void dgeco_(double *, int *, int *, int *, double *, double *);
extern "C" void dgedi_(double *, int *, int *, int *, double *, double *, int *);

template <class T>
class FRSmatrix : public std::vector< FRSvector<T> >
{
public:
	//////////////////////////////////////////////
	/// definition, construction, destruction, copying
	/// construction of special matrices (e.g. unit matrix)

	/// constructor: m x n elements (rows x cols)
	/// this is also the default constructor
	FRSmatrix(int rows=0, int cols=0, T val=(T)0)
	{ Define( rows, cols, val ); }

	/// Construct from a 2D rotation matrix
	FRSmatrix(const Rotation2D &rot);

	/// Construct from a 3D rotation matrix
	FRSmatrix(const Rotation3D &rot);

	/// Copy constructor
	FRSmatrix(const FRSmatrix<T>& rhs);

	/// constructor with a linear array of values (row sorted)
	FRSmatrix(const T *aa, int num_rows, int num_columns)
	{ ConvertFromLP( aa, num_rows, num_columns ); }

	/// Copy assignment (aka assignment operator)
	FRSmatrix & operator = (const FRSmatrix<T> &rhs);

	/// Copy assignment, with one scalar
	FRSmatrix & operator = (T rhs);

	/// destructor, release memory
	~FRSmatrix() {Erase();}

	/// Define a matrix of m_rows by m_cols
	bool Define(int m_rows, int m_cols, T val=0);

	/// Erase matrix elements
	void Erase();

	/// Define a unit matrix of m_rows by m_cols
	void Identity(int m_rows, int m_cols);

	///Copy assignment with transposition
	FRSmatrix & Transpose( const FRSmatrix<T> &b );

	/// Set this matrix to unity
	void Unit()
	{ Identity( NumRows(), NumColumns() ); }

	/// Construct a quadratic identiy matrix with row rows
	void Identity( int row )
	{ Identity ( row, row ); }

	/// Set all elements to zero
	void Zero()
	{ Define( NumRows(), NumColumns() ); }

	/////////////////////////////////////////////////
	/// conversion to/from different formats
	/// see also constructors from other elements

	/// Conversion to NR format (array of arrays)
	void Convert2NR(T ***) const;

	/// Conversion to Linpack format (one linear array)
	/// converts *this into a linear array of T
	/// the client has the responsibility to delete *a, 
	/// if it is not used anymore
	/// alternatively the signature could be (T &* a)
	void Convert2LP( T **a ) const;

	/// Conversion from NR format
	/// function not implemented, signature of method unclear
	void ConvertFromNR(FRSmatrix<T> *, double **, long, long);

	/// Conversion from LP format
	/// copy values from a (row sorted, c columns, r rows) to *this
	void ConvertFromLP( const T *a, long r, long c );

	/////////////////////////////////////////////////
	///query size

	// Not documented. Usage of NumRows is preferred.
	int NoRows() const
	{ return NumRows(); }

	/// Number of rows
	int NumRows() const
	{ return (int) this->size();}

	// Not documented. Usage of NumColumns is preferred.
	int NoCol() const
	{ return NumColumns();}

	/// Number of columns
	int NumColumns() const
	{ assert( NumRows() ); return (int) this->operator[](0).size(); }


	///////////////////////////////////////////////
	/// element access

	/// return a reference to element i,j
	/// also the square bracket operators can be used
	T & R(int i, int j){ return (*this)[i][j];}

	/// Element access for reading by row column coordinates
	T Val(int r, int c) const
	{ return (*this)[r][c]; }

	/// Element access for writing by row column coordinates
	T & Val(int r, int c)
	{ return R(r, c); }

	/// Element access for reading by index (for vectors)
	T Val(int i) const
	{ assert( NumColumns() == 1 );
	return (*this)[i][0]; }

	/// Element access for writing by index (for vectors)
	T & Val(int i)
	{ assert( NumColumns() == 1 );
	return R(i, 0); }

	////////////////////////////////////////////////
	///matrix operations (+-*/) with other matrices, vectors and scalars
	/// also including multiplication with transposed arguments
	/// and determinant calculation

	/// multiply this matrix with another matrix, return the result
	FRSmatrix<T> operator * (const FRSmatrix<T> &m) const;

	/// multiply this matrix with a vector, return the result
	FRSvector<T> operator * (const FRSvector<T> &m) const;

	/// add another matrix to this, return the result
	FRSmatrix<T> operator + (const FRSmatrix<T> &rhs) const;

	/// subtract another matrix from this matrix, return the result
	FRSmatrix<T> operator - (const FRSmatrix<T> &rhs) const;

	/// add another matrix to *this
	FRSmatrix<T> & operator += (const FRSmatrix<T> &rhs);

	/// subtract another matrix from *this
	FRSmatrix<T> & operator -= (const FRSmatrix<T> &rhs);

	/// multiply another matrix with *this
	FRSmatrix<T> & operator *= (const FRSmatrix<T> &m);

	/// multiply *this with a scalar
	FRSmatrix<T> & operator *= (T val);

	/// Multiply all matrix elements by a scalar (same as *=(double))
	FRSmatrix<T> & operator * (T c);

	/// Add a scalar to all matrix elements
	FRSmatrix<T> & operator + (T c);

	/// Subtract a scalar from all matrix elements
	FRSmatrix<T> & operator - (T c);

	/// Increment Diagonal by a diagonal matrix (represented as a vector)
	bool IncrementDiagonal(const FRSvector<T> &);

	/// Increment Diagonal by a diagonal matrix (represented as a constant)
	bool IncrementDiagonal(T val);

	/*
	function omitted, because it is not object oriented
	/// Multiply a matrix B by a matirx A and store result in X,
	/// restricted to the upper left sub matrices of A and B 
	/// as defined in mvarARows ...
	void Multiply(vMatrix &A, vMatrix &B, vMatrix &X, 
	int mvarARows, int mvarAColumns, int mvarBColumns);
	*/
	/// store A*B in *this
	void Multiply(const FRSmatrix<T> &A, const FRSmatrix<T> &B);

	/// Multiply *this with its transposed from the left. Let A := *this ->
	/// Returning an ATA where W is an optional weight matrix
	/// with a default NULL for identity
	FRSmatrix<T> MultTranspose (const FRSvector<T> * W = NULL) const;

	/// Multiply *this transposed with the argument matrix.
	/// if the argument matrix pointer = NULL, then take *this instead
	/// vRecord is a diagonal weight matrix
	/// if the current matrix is A, and vMatrix*=NULL, 
	/// then the result is ATA
	FRSmatrix<T> Transpose_MultBy (FRSvector<T> *, FRSmatrix<T> *) const;

	/// Multiply *this with the transposed argument matrix.
	/// if the argument matrix pointer = NULL, then take *this instead
	/// if the current matrix is A then the result is AAT
	/// again, the vRecord is a diagonal weight matrix 
	/// *this * vRecord * vMatrix^T
	/// if vRecord == NULL, it is considered as an identity matrix
	FRSmatrix<T> MultByTranspose (FRSvector<T> *, FRSmatrix<T> *) const;

	/// Add the outer product (v*wT). at default w = NULL. Then adding v*vT
	/// n.b. outer product is aka dyadic product
	/// The addition is performed to a submatrix of *this, beginning at 
	/// row0, col0,
	/// the size of the submatrix is v->size()
	bool AddOuterProduct(const FRSvector<T> *v, 
		int row0 = 0, int col0 = 0, 
		const FRSvector<T> *w = NULL);

	/// Create *this from the outer product (v*wT). at default w = NULL. 
	/// Then creating from v*vT
	bool CreateFromOuterProduct(FRSvector<T> *v, FRSvector<T> *w = NULL);

	/// Create a projection matrix from the vector v.
	/// This is the matrix I-v*vT, I standing for identity matrix.
	/// A vector x multiplied with this matrix will then lie in the 
	/// hyperplane perpendicular to v and is the projection of x into
	/// this hyperplane.
	/// returns false, if v has length 0. In this case the matrix is
	/// the zero matrix.
	bool CreateProjectionMatrix(FRSvector<T> *v);

	/// Create a mirror matrix from the vector v.
	/// This matrix will mirror vectors on the hyperplane
	/// which has as normal v. 
	/// In 3D this can be used to rotate vectors which lie in plane
	/// perpendicular to n1 (or points in a plane through the origin) 
	/// into the plane perpendicular to n2. v = n1+n2. 
	/// The rotation axis is then n1 x n2,
	/// and the rotation angle is the angle between n1 and n2.
	/// Of course, these planes include the origin, otherwise it holds only
	/// for vectors. 
	/// returns false, if v has length 0.
	bool CreateMirrorMatrix(FRSvector<T> *v);

	/// Return the determinant of a matrix
	T Det() const;

	/// friend function declaration for computing the row of matrix1 
	/// with the column of matrix2

	friend T MultRowColumn<T>( const FRSmatrix<T>& matrix1, int row1, 
		const FRSmatrix<T>& matrix2, int col2 );

	////////////////////////////////////////////////////////////////////
	///sub matrices adding and replacing
	/// concatenation (appending a row)

	/// add to the elements of *this starting at r0 and c0 those of v
	/// return false on impossiblitiy
	bool AddPartial(const FRSmatrix<T> & v, int r0 = 0, int c0 = 0);

	/// replace the elements of *this starting at r0 and c0 with those of v
	/// return false on impossiblitiy
	bool ReplacePartial(const FRSmatrix<T> &v, int r0=0, int c0=0);

	/// like ReplacePartial, but with a different name. Return the updated
	/// this-matrix. Assert correctness of arguments in debug mode.
	FRSmatrix<T> & Insert(const FRSmatrix<T> &sub, 
		int row_offset, int col_offset)
	{ bool success = ReplacePartial( sub, row_offset, col_offset );
	assert( success );
	return *this; }

	/// Insert a part of a matrix at the specified location
	/// row_offset and column_offser refer to *this,
	/// the other four integers specify the submatrix of matrix to be
	/// copied to *this
	FRSmatrix<T> &Insert(const FRSmatrix<T> &matrix, 
		int first_row, int first_column,
		int number_of_rows, int number_of_columns,
		int row_offset, int column_offset);

	void push_back( const FRSvector<T>& row ) { 
		assert( !NumRows() || (int) row.size() == NumColumns() );
		((std::vector< FRSvector<T> >*)this)->push_back( row ); 
	}

	////////////////////////////////////////////////////////////////////
	/// inversion

	/// Inversion by Gauss Elimination
	/// Ainv = (*this)^(-1), applying pivoting
	/// singularity is checked against EPS
	bool InvertByGauss(FRSmatrix<T> &Ainv, T EPS=(T)1.e-6) const;

	/// Inversion of a 3 by 3 Matrix
	/// singularity is checked against EPS
	bool Invert_3x3(FRSmatrix<T> &, T EPS=(T)1.e-6) const;

	/// Inversion by SVD
	/// singularity is checked against EPS
	/// the rank deficit is returned as rDeficit
	bool InvertBySVD(FRSmatrix<T> &, int& rDeficit, T EPS=(T)1.e-6) const;

	/// Inversion with the Linpack routines dgeco and dgedi.
	/// (*this) := matrix^(-1)
	/// dgeco performs a factorization by Gaussian elimination.
	/// dgedi invert the factorized matrix.
	/// @param matrix Matrix to be inverted
	/// @param cond   Condition number of factorization
	/// @return The inverted matrix (= this)
	FRSmatrix<T> &Inverse( const FRSmatrix<T> &matrix, double &cond );

	/// General interface for inversion
	bool Invert(FRSmatrix<T> &, int inversionType = STANDARD, T EPS = (T)1.e-6);

	/// get the singular value decomposition
	/// any Matrix A can be decomposed into U*W*Vt, where the t stands
	/// for the transposed matrix, in an (almost) unique way,
	/// with Ut*U = Vt*V = unit matrix of dimension: number of columns in A,
	/// and W as diagonal matrix. U has the same dimension as A, V is
	/// as square matrix. 
	/// Here, A (=*this) remains unchanged, eigVal holds the values of 
	/// W and eigVec is V.
	/// For quadratic symmetric matrices eigVal and eigVec are really
	/// the eigen values and (right) eigen vectors of the matrix *this.
	//NR_REMOVED	void GetSingularValues(FRSvector<T>& eigVal, FRSmatrix<T>& eigVec)const;

	////////////////////////////////////////////////////////////////////
	/// sparse matrices

	/// assing *this to the multiplication of the "ordinay"
	/// matrix (A) with a sparse diagonal matrix (given as a
	/// vector of matrices)
	/// Sparse multiplication
	bool SparseMultiplication(FRSmatrix<T> & A, 
		std::vector< FRSmatrix<T> > &obj);

	/// *this = A + obj, see comment on SpaseMultiplication
	bool SparseAddition(FRSmatrix<T> & A, 
		std::vector< FRSmatrix<T> > &obj);

	/// *this = A - obj, see comment on SpaseMultiplication
	bool SparseSubtraction(FRSmatrix<T> & A, 
		std::vector< FRSmatrix<T> > &obj);


	///////////////////////////////////////////////////////////
	/// input/output (input still missing)

	/// Print matrix
	void Print()const;

	/// Print matrix with the specified format
	/// function should be const, but iterators prohibit this
	/// thus a dirty trick has been used in ::Print(char*)
	void Print(char *format)const;

	/// output with the insertion operator
	/// write first the number of rows and columns, then the matrix elements
    friend std::ostream& operator<< <T> (std::ostream& s,
		const FRSmatrix<T>& mat);

protected:
	// Partial pivoting, starting at the specified row (?)
	int Pivot(int row);

};

//////////////////////////////////////////////////////////////////////////
template<class T>
bool FRSmatrix<T>::Define(int m_rows, int m_cols, T val)
{
	if(!this->empty())
		Erase();
	if (m_rows == 0 || m_cols == 0)
		return false;

	FRSvector<T> r(m_cols, val); // row of mvarRows Set to 0 is default 
	// but just in case
	for(int col=0; col < m_rows; col++)
		//		push_back(r);
		((std::vector< FRSvector<T> >*)this)->push_back( r );
	// for efficiency, the rows have all the same length, 
	// there is no point in checking it

	return true;
}

//----------------------------------------------------------------------------
template<class T>
void FRSmatrix<T>::Erase()
{
	typename FRSmatrix<T>::iterator record;

	// Clean up
	//	printf("In Erase \n");
	if(!this->empty()){
		for(record = this->begin(); record != this->end(); record++)
			record->erase(record->begin(),record->end());
        this->erase(this->begin(), this->end());
	}
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T>::FRSmatrix(const FRSmatrix<T>& rhs) : std::vector <FRSvector<T> >()
{
	if (!this->empty()) Erase();
	for (typename FRSmatrix<T>::const_iterator row = rhs.begin(); row != rhs.end(); row++)
		push_back(*row);
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T>::FRSmatrix(const Rotation2D &rot)
{
	Define(2, 2);
	for (int ir=0; ir<2; ir++)
        for (int ic=0; ic<2; ic++)
            Val(ir, ic) = rot.R(ir, ic);
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T>::FRSmatrix(const Rotation3D &rot)
{
	Define(3, 3);
	for (int ir=0; ir<3; ir++)
		for (int ic=0; ic<3; ic++)
			Val(ir, ic) = rot.R(ir, ic);
}

//----------------------------------------------------------------------------
template<class T>
void FRSmatrix<T>::Identity(int m_rows, int m_cols)
{
	if (m_rows != m_cols)
		printf("Unit of a non-square matrix\n");

	Define(m_rows, m_cols);
	int bound=(m_rows < m_cols)?m_rows:m_cols;
	for(int i =0; i < bound; i++)
		(*this)[i][i] = 1.;
}


//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T> & FRSmatrix<T>::operator = (const FRSmatrix<T> &rhs)
{
	if (this == &rhs)
		return *this;
	if (!this->empty()) Erase();
	for (typename FRSmatrix<T>::const_iterator row = rhs.begin(); row != rhs.end(); row++)
		push_back(*row);
	return(*this);
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T> & FRSmatrix<T>::operator = (T rhs)
{
	if (!this->empty())
		for (int i = 0; i < this->size(); i++)
			for (int j = 0; j < this->begin()->size(); j++)
				(*this)[i][j] = rhs;
	return *this;
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T> & FRSmatrix<T>::Transpose (const FRSmatrix<T> &b)
{
	assert( this != &b );
	Define( b.NumColumns(), b.NumRows() );
	for( int i = 0; i<b.NumRows(); i++ )
		for( int j=0; j<b.NumColumns(); j++ )
			(*this)[j][i] = b[i][j];
	return *this;
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T> & FRSmatrix<T>::operator += (const FRSmatrix<T> &rhs)
{
	assert( !this->empty() );
	assert( !rhs.empty() );
	assert( rhs.size() == this->size() );
	assert( rhs.begin()->size() == this->begin()->size() );

	for (int i = 0; i < this->size(); i++)
		for (int j = 0; j < this->begin()->size(); j++)
			(*this)[i][j] += rhs.Val(i, j);
	return *this;
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T> & FRSmatrix<T>::operator -= (const FRSmatrix<T> &rhs)
{
	assert( !this->empty() );
	assert( !rhs.empty() );
	assert( rhs.size() == this->size() );
	assert( rhs.begin()->size() == this->begin()->size() );

	for (int i = 0; i < this->size(); i++)
		for (int j = 0; j < this->begin()->size(); j++)
			(*this)[i][j] -= rhs.Val(i, j);
	return *this;
}

//---------------------------------------------------------------------------- 
template<class T>
FRSmatrix<T> & FRSmatrix<T>::operator *= (const FRSmatrix<T> &m)
{
	FRSmatrix<T> res;
	res.Multiply(*this, m);
	*this = res;
	return *this;
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T> & FRSmatrix<T>::operator *= (T val)
{
	assert( !this->empty() );
	assert( this->begin()->size() );

	for (int i = 0; i < this->size(); i++)
		for (int j = 0; j < this->begin()->size(); j++)
			(*this)[i][j] *= val;
	return *this;
}

//----------------------------------------------------------------------------
// Multiply all matrix elements by a constant
template<class T>
FRSmatrix<T> & FRSmatrix<T>::operator * (T c)
{
	if (!this->empty() && this->begin()->size())
		for (int i = 0; i < this->size(); i++)
			for (int j = 0; j < this->begin()->size(); j++)
				(*this)[i][j] *= c;
	return *this;
}

//----------------------------------------------------------------------------
/// Add a constant to all matrix elements
template<class T>
FRSmatrix<T> & FRSmatrix<T>::operator + (T c)
{
	if (!this->empty() && this->begin()->size())
		for (int i = 0; i < this->size(); i++)
			for (int j = 0; j < this->begin()->size(); j++)
				(*this)[i][j] += c;
	return *this;
}

//----------------------------------------------------------------------------
/// Subtract a constant from all matrix elements
template<class T>
FRSmatrix<T> & FRSmatrix<T>::operator - (T c)
{
	if (!this->empty() && this->begin()->size())
		for (int i = 0; i < this->size(); i++)
			for (int j = 0; j < this->begin()->size(); j++)
				(*this)[i][j] -= c;
	return *this;
}

//----------------------------------------------------------------------------
template<class T>
bool FRSmatrix<T>::IncrementDiagonal(const FRSvector<T> & vec)
{
	if(this->empty() || this->size() != this->begin()->size() ||
		vec.size() != this->size())
		return false;

	for (int i = 0; i < this->size(); i++)
		(*this)[i][i] += vec[i];
	return true;

}

//----------------------------------------------------------------------------
template<class T>
bool FRSmatrix<T>::IncrementDiagonal(T val)
{
	if(this->empty() || this->size() != this->begin()->size())
		return false;

	for (int i = 0; i < this->size(); i++)
		(*this)[i][i] += val;
	return true;
}

/*
//----------------------------------------------------------------------------
template<class T>
void FRSmatrix<T>::Multiply(vMatrix &A, vMatrix &B, vMatrix &X, int mvarARows, int mvarAColumns, int mvarBColumns)
{
int i, j, k;
T sum;

//Multiply A with B and put the result in X
for(i = 0; i < mvarARows; i++){
for(j = 0; j < mvarBColumns; j++){
sum = 0.0;
for(k = 0; k < mvarAColumns; k++)
sum = sum + A[i][k] * B[k][j];
X[i][j] = sum;
}
}
}
*/

//----------------------------------------------------------------------------
template<class T>
void FRSmatrix<T>::Multiply(const FRSmatrix<T> &A, const FRSmatrix<T> &B)
{
	int n_rows = A.size();
	int n_cols = A.begin()->size();

	int m_rows = B.size();
	int m_cols = B.begin()->size();

	assert( n_cols == m_rows );

	Define(n_rows, m_cols);

	//Multiply A with B
	for(int i = 0; i < n_rows; i++)
		for(int j = 0; j < m_cols; j++)
			for(int k = 0; k < n_cols; k++)
				(*this)[i][j] += A[i][k] * B[k][j];
}

//----------------------------------------------------------------------------
template<class T>
FRSvector<T> FRSmatrix<T>::operator * (const FRSvector<T> &m) const
{
	assert( !this->empty() && !m.empty() );

	int n_rows = this->size();
	int n_cols = this->begin()->size();
	int m_rows = m.size();

	assert( n_cols == m_rows );

	FRSvector<T> res( n_rows );

	T sum;
	//Multiply *this with m and put the result in res
	for(int i = 0; i < n_rows; i++){
		sum = 0.;
		for(int k = 0; k < n_cols; k++)
			sum += (*this)[i][k] * m[k];
		res[i] = sum;
	}
	return res;
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T> FRSmatrix<T>::operator * (const FRSmatrix<T> &m) const
{
	assert(!this->empty() && !m.empty());

	int n_rows = this->size();
	int n_cols = this->begin()->size();
	int m_rows = m.size();
	int m_cols = m.begin()->size();

	assert( n_cols == m_rows );

	FRSmatrix<T> res( n_rows, m_cols, 0 );

	//Multiply *this with m and put the result in res
	for(int i = 0; i < n_rows; i++)
		for(int j = 0; j < m_cols; j++)
			for(int k = 0; k < n_cols; k++)
				res[i][j] += (*this)[i][k] * m[k][j];
	return res;
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T> FRSmatrix<T>::operator + (const FRSmatrix<T> &rhs) const
{
	FRSmatrix<T>	res;

	int n_rows = this->size(); 
	int n_cols = this->begin()->size();

	int m_rows = rhs.size();
	int m_cols = rhs.begin()->size();

	assert( n_rows == m_rows && n_cols == m_cols );

	res.Define(n_rows, n_cols);

	//Multiply A with B and put the result in res
	for(int i = 0; i < n_rows; i++){
		for(int j = 0; j < n_cols; j++){
			res[i][j] = (*this)[i][j] + rhs[i][j];
		}
	}
	return res;
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T> FRSmatrix<T>::operator - (const FRSmatrix<T> &rhs) const
{
	FRSmatrix<T>	res;

	int n_rows = this->size(); 
	int n_cols = this->begin()->size();

	int m_rows = rhs.size();
	int m_cols = rhs.begin()->size();

	assert( n_rows == m_rows && n_cols == m_cols );

	res.Define(n_rows, n_cols);

	//Multiply A with B and put the result in res
	for(int i = 0; i < n_rows; i++){
		for(int j = 0; j < n_cols; j++){
			res[i][j] = (*this)[i][j] - rhs[i][j];
		}
	}
	return res;
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T> FRSmatrix<T>::MultTranspose (const FRSvector<T> *W) const
{
	int i, j, k;
	T sum;
	int n_rows = this->size();
	int n_cols = this->begin()->size();

	assert( !W || W->size() == n_rows );

	FRSmatrix<T> res;
	res.Define(n_cols, n_cols);

	if (W == NULL){
		for(i = 0; i < n_cols; i++){
			for(j = i; j < n_cols; j++){
				sum = 0.0;
				for(k = 0; k < n_rows; k++)
					sum += (*this)[k][i] * (*this)[k][j];
				res[i][j] = sum;
			}
		}
	}
	else{
		for(i = 0; i < n_cols; i++){
			for(j = i; j < n_cols; j++){

				sum = 0.0;
				for(k = 0; k < n_rows; k++)
					sum += (*this)[k][i] * (*this)[k][j] * (*W)[k];
				res[i][j] = sum;
			}
		}
	}

	// Copy to lower triangular elements
	for(i = 0; i < n_cols; i++)
		for(j = 0; j < i; j++)
			res[i][j] = res[j][i];

	return res;
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T> FRSmatrix<T>::Transpose_MultBy (FRSvector<T> *_W, 
	FRSmatrix<T> *B) const
{
	int i, j, k;
	int n_rows = this->size();
	int n_cols = this->begin()->size();
	int m_rows, m_cols;
	FRSmatrix<T> res;
	const FRSmatrix<T> *D = NULL;
	FRSvector<T> *W = NULL;

	assert( !_W || _W->size() == n_rows );
	assert( !B || !B->empty() && B->NumRows() == n_rows );

	if (B){
		m_rows = B->size();
		m_cols = B->begin()->size();
		D = B;
	}
	else{
		m_rows = n_rows;
		m_cols = n_cols;
		D = this;
	}

	if (_W){
		W = _W;
	}
	else
		W = new FRSvector<T> (n_cols, 1.);

	res.Define(n_cols, m_cols);
	for(i = 0; i < n_cols; i++)
		for(j = 0; j < m_cols; j++)
			for(k = 0; k < n_rows; k++)
				res[i][j] += (*this)[k][i] * (*D)[k][j] * (*W)[k];

	if (!_W) W->erase(W->begin(), W->end());
	return res;
}

//----------------------------------------------------------------------------
template<class T>
FRSmatrix<T> FRSmatrix<T>::MultByTranspose (FRSvector<T> *_W, 
	FRSmatrix<T> *B) const
{
	int i, j, k;
	int n_rows = this->size();
	int n_cols = this->begin()->size();
	int m_rows, m_cols;
	FRSmatrix<T> res;
	const FRSmatrix<T> *D = NULL;
	FRSvector<T> *W = NULL;

	assert( !B || B->NumColumns() == n_cols );
	assert( !_W || _W->size() == n_cols );

	if (B){
		m_rows = B->size();
		m_cols = B->begin()->size();
		D = B;
	}
	else{
		m_rows = n_rows;
		m_cols = n_cols;
		D = this;
	}

	if (_W){
		W = _W;
	}
	else
		W = new FRSvector<T>(n_cols, 1.);

	res.Define(n_rows, m_rows);
	for(i = 0; i < n_rows; i++)
		for(j = 0; j < m_rows; j++)
			for(k = 0; k < n_cols; k++)
				res[i][j] += (*this)[i][k] * (*D)[j][k] * (*W)[k];

	if (!_W) W->erase(W->begin(), W->end());
	return res;
}

//----------------------------------------------------------------------------
template<class T>
bool FRSmatrix<T>::CreateFromOuterProduct(FRSvector<T> *v, FRSvector<T> *w)
{

	int	m_rows = v->size();
	assert (m_rows) ;

	int m_cols = (w == NULL)? m_rows : w->size();

	if(this->empty())
		Define(m_rows, m_cols);

	int n_rows = this->size(); 
	int n_cols = this->begin()->size();

	if (m_rows != n_rows && m_cols != n_cols)
		Define(m_rows, m_cols);

	if (w == NULL){
		for(int i = 0; i < n_rows; i++)
			for(int j = 0; j < n_cols; j++)
				(*this)[i][j] = (*v)[i]*(*v)[j];
	}
	else{
		for(int i = 0; i < n_rows; i++)
			for(int j = 0; j < n_cols; j++)
				(*this)[i][j] = (*v)[i]*(*w)[j];
	}
	return true;
}

//----------------------------------------------------------------------------
template<class T>
bool FRSmatrix<T>::CreateProjectionMatrix(FRSvector<T> *v)
{
	FRSvector<T> loc( *v );
	T norm = loc.Norm();
	if( !norm )
		return false;
	loc /= norm;
	Identity( v->size() );
	for( int i=0; i<v->size(); i++ )
	{
		(*this)[i][i] -= (*v)[i]*(*v)[i];
		for( int j=i+1; j< v->size(); j++ )
			(*this)[j][i] = (*this)[i][j] = -(*v)[i]*(*v)[j];
	}
	return true;
}

//----------------------------------------------------------------------------
template<class T>
bool FRSmatrix<T>::CreateMirrorMatrix(FRSvector<T> *v)
{
	FRSvector<T> loc( *v );
	T norm = loc.Norm();
	if( !norm )
		return false;
	loc /= norm;
	Identity( v->size() );
	for( int i=0; i<v->size(); i++ )
	{
		(*this)[i][i] -= (T)2*(*v)[i]*(*v)[i];
		for( int j = i + 1; j < v->size(); j++ )
			(*this)[j][i] = (*this)[i][j] = -(T)2*(*v)[i]*(*v)[j];
	}
	return true;
}

//----------------------------------------------------------------------------
template<class T>
bool FRSmatrix<T>::AddOuterProduct(const FRSvector<T> *v, 
	int row0, int col0, 
	const FRSvector<T> *w)
{

	assert( v && !( v->empty() ) );

	int n_rows = this->size();
	int m_rows = v->size();
	assert( n_rows && m_rows );

	int n_cols = this->begin()->size();
	int m_cols = (w == NULL)? m_rows : w->size();

	assert( row0 + m_rows <= n_rows && col0 + m_cols <= n_cols );

	if (w == NULL){
		for(int i = 0; i < n_rows; i++)
			for(int j = 0; j < n_cols; j++)
				(*this)[i][j] += (*v)[i]*(*v)[j];
	}
	else{
		for(int i = 0; i < m_rows; i++)
			for(int j = 0; j < m_cols; j++){
				T tmp = (*this)[row0 + i][col0 + j], a = (*v)[i], b = (*w)[j];
				/// ???NP what is the above line good for?
				(*this)[row0 + i][col0 + j] += (*v)[i]*(*w)[j];
			}
	}
	return true;
}

//----------------------------------------------------------------------------

template<class T>
bool FRSmatrix<T>::AddPartial(const FRSmatrix<T> &v, int r0, int c0)
{
	assert( r0 >= 0 && c0 >= 0 );
	assert( v.size()+r0 <= this->size() && 
		v.begin()->size() + c0 <= this->begin()->size() );

	for (int i = 0; i < v.size(); i++)
		for (int j = 0; j < v.begin()->size(); j++)
			(*this)[r0 + i][c0 + j] += v[i][j];
	return true;
}

//----------------------------------------------------------------------------
template<class T>
bool FRSmatrix<T>::ReplacePartial(const FRSmatrix<T> &v, int r0, int c0)
{
	assert( r0 >= 0 && c0 >= 0 );
	assert( v.size()+r0 <= this->size() && 
		v.begin()->size() + c0 <= this->begin()->size() );

	for (int i = 0; i < v.size(); i++)
		for (int j = 0; j < v.begin()->size(); j++)
			(*this)[r0 + i][c0 + j] = v[i][j];
	return true;
}

//----------------------------------------------------------------------------

template<class T>
FRSmatrix<T> & FRSmatrix<T>::Insert(const FRSmatrix<T> &m, 
	int first_row, int first_col,
	int num_rows, int num_cols,
	int row_offset, int col_offset)
{
	assert(first_row >=0 && first_col >= 0);
	assert(first_row+num_rows <= m.NumRows() && 
		first_col+num_cols <= m.NumColumns());
	assert(row_offset >=0 && col_offset >= 0);
	assert(row_offset+num_rows <= NumRows() && 
		col_offset+num_cols <= NumColumns());

	for (int ir=0; ir<num_rows; ir++)
		for (int ic=0; ic<num_cols; ic++)
			Val(ir + row_offset, ic + col_offset)  = m.Val(ir + first_row, ic + first_col);

	return *this;
}

//----------------------------------------------------------------------------
template<class T>
void FRSmatrix<T>::Convert2NR(T *** m) const
{
	printf("Function FRSmatrix<T>::Convert2NR(T *** m) const is obsolete\n");
	exit(0);

	/* NR_REMOVED
	assert( sizeof(T) == sizeof(double) );
	// preliminary solution. Will fail on integers with 4 bytes, or on 
	// floats. Function matrix will have to be changed or replaced.

	long n_rows = this->size();
	long n_cols = this->begin()->size();

	*m = (T **) matrix(1L, n_cols, 1L, n_rows);
	if( !*m )
	std::cerr << "allocation failure in FRSmatrix<T>::Convert2NR\n";

	for (int i = 1; i <= n_rows; i++)
	for(int j = 1; j <= n_cols; j++)
	(*m)[j][i] = (*this)[i-1][j-1];
	NR_REMOVED END */
}

//----------------------------------------------------------------------------

template<class T>
void FRSmatrix<T>::Convert2LP( T **a ) const
{
	long row = NumRows();
	long col = NumColumns();

	*a = (T*) calloc( (row*col), sizeof(T) );
	if( !*a )
		std::cerr << "allocation failure in FRSmatrix<T>::Convert2LP\n";
	else
	{
		for( int i=0; i<row; i++ )
			for( int j=0; j<col; j++ )
				(*a)[ i*row+j ] = (*this)[i][j];
	}

	return;
}

//----------------------------------------------------------------------------

template<class T>
void FRSmatrix<T>::ConvertFromLP( const T *a, long row, long col )
{
	assert( row>=0 );
	assert( col>=0 );
	assert( a );

	Define( row, col );
	for( int i=0; i<row; i++ )
		for( int j=0; j<col; j++ )
			(*this)[i][j] = a[ i*row+j ];

	return;
}

//----------------------------------------------------------------------------
template<class T>
void FRSmatrix<T>::ConvertFromNR(FRSmatrix<T> *A, double ** m, 
	long n_rows, long n_cols)
{
	assert( 0 /* code missing */ );
}

//----------------------------------------------------------------------------

template<class T>
bool FRSmatrix<T>::Invert_3x3(FRSmatrix<T> &Ainv, T EPS) const
{

	assert( this->size() == 3 && this->begin()->size() == 3 );

	if (Ainv.size() != 3 || Ainv.begin()->size() != 3)
		Ainv.Define(3, 3);

	T a = (*this)[0][0], b = (*this)[0][1], c = (*this)[0][2];
	T                    e = (*this)[1][1], f = (*this)[1][2];
	T                                       i = (*this)[2][2];

	T t2 = f*f;
	T t4 = a*e;
	T t7 = b*b;
	T t9 = c*b;
	T t12 = c*c;

	T t15 = -t4*i+a*t2+t7*i-2.0*t9*f+t12*e;

	if(fabs(t15) <= EPS){
		return RankDef;
	}
	t15 = 1.0/t15;  // here is the division, if integer division is 
	// possible without remainder, than also
	// integer matrices are inverted correctly

	T t20 = (-b*i+c*f)*t15;
	T t24 = (b*f-c*e)*t15;
	T t30 = (a*f-t9)*t15;

	Ainv[0][0] = (-e*i+t2)*t15; Ainv[0][1] = -t20;           Ainv[0][2] = -t24;
	Ainv[1][0] = -t20;          Ainv[1][1] = -(a*i-t12)*t15; Ainv[1][2] = t30;
	Ainv[2][0] = -t24;          Ainv[2][1] = t30;            Ainv[2][2] = -(t4-t7)*t15;

	return FullRank;
}

//----------------------------------------------------------------------------
#include "Eigen/Dense"
//using namespace Eigen;

template<class T>
bool FRSmatrix<T>::InvertBySVD(FRSmatrix<T> & m_N1, int& rDef, T EPS) const
{
	// New code based on LINPACK
	int i,j;
	int row = NumRows();
	int col = NumColumns();
	Eigen::MatrixXd A(row,col);
	Eigen::MatrixXd InvA(row,col);

	for (i=0; i<row; ++i) {
		for (j=0; j<col; j++)
			A(i,j) = (*this)[i][j];
	}

	if(!A.fullPivLu().isInvertible())
		return false;

	InvA = A.fullPivLu().inverse();
	for (i=0; i<row; ++i) {
		for (j=0; j<col; j++)
			m_N1[i][j] = InvA(i,j);
	}
	return true;

	/*int    job, *pivot;
	double *z, det[2], cond;

	int row = NumRows();
	int col = NumColumns();

	assert( row == col ); // Check the matrix is square

	pivot = (int *) malloc(row * sizeof(int));
	z     = (double *) malloc(row * sizeof(double));

	double* a;
	Convert2LP( &a );

	dgeco_(a, &row, &col, pivot, &cond, z);

	if (1.0 + cond == 1.0)
		fprintf(stderr, "Warning: bad condition of matrix (%5.2e)\n", cond);

	job = 1;
	dgedi_(a, &row, &col, pivot, det, z, &job);

	m_N1.ConvertFromLP( a, row, col );

	free(pivot);  free(z); free(a);
	return true;*/

	// End of new code based on LINPACK

	/* Inversion by Gauss removed 
	printf("Inversion by SVD removed from library, inversion by Gauss\n");
	rDef = 0;
	FRSmatrix<T> locCopy = *this;

	if (!InvertByGauss(m_N1, EPS)){
	printf("Inversion failed, rDef has been set to 1\n");
	rDef = 1;
	return false;
	}
	return true;

	*/  

	/* NR_REMOVED	
	bool matrixType = FullRank;
	int i;

	long n_rows = this->size();
	rDef = 0;
	FRSmatrix<T> locCopy = *this;
	NRMat<T> loc( locCopy );
	NRVec<T> eVal( n_rows );
	NRVec<T> eValInv( n_rows );
	NRMat<T> eVec( n_rows, n_rows );
	NR::svdcmp( loc, eVal, eVec );
	for (i = 0; i < n_rows; i++)
	if (fabs(eVal[i]) > EPS)
	eValInv[i] = 1./eVal[i];
	else{
	eValInv[i] = 0.;
	matrixType = RankDef;
	rDef++;
	}

	if (m_N1.empty() || m_N1.size() != n_rows || m_N1.begin()->size() != n_rows)
	m_N1.Define(n_rows, n_rows);
	else
	m_N1 = 0.;
	// Creating the g-inverse
	for (i = 0; i < n_rows; i++)
	for (int j = 0; j < n_rows; j++){
	double sum = 0.;
	for (int k = 0; k < n_rows; k++)
	sum += eVec[i][k] * eValInv[k] * loc[j][k];
	m_N1[i][j] = sum;
	}
	return matrixType;
	NR_REMOVED END */
}

//----------------------------------------------------------------------------
//NR_REMOVED
/*
// get the singular value decomposition
template<class T>
void FRSmatrix<T>::GetSingularValues(FRSvector<T>& eigVal, FRSmatrix<T>& eigVec)const 
{
printf("Fuction FRSmatrix<T>::GetSingularValues(FRSvector<T>& eigVal, FRSmatrix<T>& eigVec)const\n");
printf("has been removed from the mapping library.\n");

// NR_REMOVED
FRSmatrix<T> locCopy = *this;
NRMat<T> loc( locCopy );
NRVec<T> eVal( eigVal );
NRMat<T> eVec( eigVec );
NR::svdcmp( loc, eVal, eVec );
//NR_REMOVED END 

// Determine the eigen values and eigen vectors with Eispack

int      matz=1, error;

FRSmatrix<T> locCopy = *this;
int n_rows = this->size();
int n_cols = this->begin()->size();

double   tmp1[n_cols], tmp2[n_cols], eigen_real[n_cols], eigen_imag[n_cols], eigen_vec[n_rows][n_cols],
central_moments[n_rows*n_cols];

rg_(n_rows, n_cols, (double *) real_matrix, eigen_real, eigen_imag, &matz,
(double *) eigen_vec, tmp1, tmp2, &error);


}
*/
//END_NR_REMOVED

//----------------------------------------------------------------------------
template<class T>
bool FRSmatrix<T>::Invert(FRSmatrix<T> & A, int inversionType, T EPS) 
{
	switch (inversionType){
    case Inversion_type::STANDARD:
		if (this->size() == 3){
			if (!Invert_3x3(A, EPS)){ 
				printf("Inversion failed\n");
				return false;
			}
		}
		else if (!InvertByGauss(A, EPS)){
			printf("Inversion failed\n");
			return false;
		}
		break;
    case Inversion_type::SVD: {
		int rankDeficit;
		printf("Inversion by SVD removed from library, inversion by Gauss\n");
		if (!InvertByGauss(A, EPS)){
			printf("Inversion failed\n");
			return false;
		}
			  }
			  break;
	default:
		return false;
		break;
	}
	return true;
}

//----------------------------------------------------------------------------
template<class T>
bool FRSmatrix<T>::InvertByGauss(FRSmatrix<T> &Ainv, T EPS) const
{
	int TempInt, i, j, p; 
	int LastCol; // Last column of the augmented matrix
	double Multiplier;

	int mvarColumns = this->size();
	int mvarRows = this->begin()->size();

	assert( mvarColumns == mvarRows );

	if (Ainv.size() != mvarRows || Ainv.begin()->size() != mvarRows)
		Ainv.Define(mvarRows, mvarRows);

	FRSmatrix<T> M; // row, colums
	intVector    PVT;

	// Initialise matrix M - Augmented matrix
	M.Define(mvarRows, 2 * mvarRows);

	// Initialise vector PVT
	int val=0;
	for(int row=0; row < mvarRows; row++)
		PVT.push_back(val);

	LastCol = 2 * mvarRows; // Last column of the augmented matrix

	// Create the Augmented matrix
	for(i = 0; i < mvarRows; i++){	// Copy matrix A to augmented matrix
		for(j = 0; j < mvarRows; j++)
			M[i][j] = (*this)[i][j];
		// Initialise the Identity matrix within the augmented matrix
		M[i][mvarRows + i] = 1.0; // Main diagonal=1
	}

	// Initialise the pivot vector
	for(i = 0; i < mvarRows; i++)
		PVT[i] = i;

	// Invert: Create an identity matrix on the left side of the augmented matrix
	for(p = 0; p < mvarRows; p++){
		// Pivot strategy: look for the element with the largest absolute value
		for(i = p + 1; i < mvarRows; i++){
			if(fabs(M[PVT[i]][p]) > fabs(M[PVT[p]][p])){
				TempInt = PVT[p];
				PVT[p] = PVT[i];
				PVT[i] = TempInt;
			}
		}
		if(fabs(M[PVT[p]][p]) <= EPS)//
		{printf("The matrix is singular, value = %f",M[PVT[p]][p]);
		return false;
		}

		for(i = p - 1; i >= 0; i--){ // Reduce upper values to zero
			Multiplier = M[PVT[i]][p] / M[PVT[p]][p];

			for(j = p + 1; j < LastCol; j++)
				M[PVT[i]][j] = M[PVT[i]][j] - Multiplier * M[PVT[p]][j];
		}

		for(i = p + 1; i < mvarRows; i++){ //Reduce lower values to zero
			Multiplier = M[PVT[i]][p] / M[PVT[p]][p];

			for(j = p + 1; j < LastCol; j++)
				M[PVT[i]][j] = M[PVT[i]][j] - Multiplier * M[PVT[p]][j];
		}
	}

	// Test for singularity
	if(M[PVT[mvarRows - 1]][mvarRows - 1] == 0)
		printf("The matrix is singular\n");
	return false;

	// Divide by diagonal of augmented matrix
	for(p = 0; p < mvarRows; p ++)
		for(j = mvarRows; j < 2 * mvarRows; j++)
			M[(int)PVT[p]][j] = M[(int)PVT[p]][j] / M[(int)PVT[p]][p];

	// Copy inverted matrix to A matrix
	for(j = 0; j < mvarRows; j++){
		i = mvarRows + j;
		for(p = 0; p < mvarRows; p++)
			Ainv[p][j] = M[(int)PVT[p]][i];
	}

	return FullRank;
}

#undef EPS

//----------------------------------------------------------------------------

template<class T>
FRSmatrix<T> & FRSmatrix<T>::Inverse( const FRSmatrix<T> &matrix, double &cond )
{
	int    job, *pivot;
	double *z, det[2];

	int row = matrix.NumRows();
	int col = matrix.NumColumns();

	assert( row == col ); // Check the matrix is square
	*this = matrix;              // Copy the matrix to be inverted

	pivot = (int *) malloc(row * sizeof(int));
	z     = (double *) malloc(row * sizeof(double));

	double* a;
	Convert2LP( &a );

	dgeco_(a, &row, &col, pivot, &cond, z);

	if (1.0 + cond == 1.0)
		fprintf(stderr, "Warning: bad condition of matrix (%5.2e)\n", cond);

	job = 1;
	dgedi_(a, &row, &col, pivot, det, z, &job);

	ConvertFromLP( a, row, col );

	free(pivot);  free(z); free(a);
	return(*this);
}

//----------------------------------------------------------------------------

template<class T>
bool FRSmatrix<T>::SparseMultiplication(FRSmatrix<T> & A, 
	std::vector< FRSmatrix<T> > &obj)
{
	typename std::vector< FRSmatrix<T> >::iterator matrx;
	int prm_counter = 0;

	assert( !A.empty() );
	// Size of the sparse matrix (sum of the sub matrices)
	for (matrx = obj.begin(); matrx != obj.end(); matrx++){
		assert( !matrx->empty() && matrx->size() == matrx->begin()->size() );
		prm_counter += matrx->size();
	}

	assert( prm_counter == A.begin()->size() );

	Define(A.size(), prm_counter);
	int prev_pos = 0;
	for (matrx = obj.begin(); matrx != obj.end(); matrx++){
		for (int i = 0; i < A.size(); i++)
			for(int j = 0; j < matrx->size(); j++)
				for(int k = 0; k < matrx->size(); k++)
					(*this)[i][prev_pos + j] += A[i][prev_pos + k]*(*matrx)[k][j];
		prev_pos += matrx->size();
	}
	return true;
}

//----------------------------------------------------------------------------

template<class T>
bool FRSmatrix<T>::SparseAddition(FRSmatrix<T> & A, 
	std::vector< FRSmatrix<T> > &obj)
{
	typename std::vector< FRSmatrix<T> >::iterator matrx;
	int prm_counter = 0;

	assert( !A.empty() );
	// Size of the sparse matrix (sum of the sub matrices)
	for (matrx = obj.begin(); matrx != obj.end(); matrx++){
		assert( !matrx->empty() && matrx->size() == matrx->begin()->size() );
		prm_counter += matrx->size();
	}

	assert( prm_counter == A.begin()->size() );

	(*this) = A;
	int prev_pos = 0;
	for (matrx = obj.begin(); matrx != obj.end(); matrx++){
		for (int i = 0; i < matrx->size(); i++)
			for(int j = 0; j < matrx->size(); j++)
				(*this)[prev_pos + i][prev_pos + j] += (*matrx)[i][j];
		prev_pos += matrx->size();
	}
	return true;
}

//----------------------------------------------------------------------------

template<class T>
bool FRSmatrix<T>::SparseSubtraction(FRSmatrix<T> & A, 
	std::vector< FRSmatrix<T> > &obj)
{
	typename std::vector< FRSmatrix<T> >::iterator matrx;
	int prm_counter = 0;

	assert( !A.empty() );
	// Size of the sparse matrix (sum of the syb matrices)
	for (matrx = obj.begin(); matrx != obj.end(); matrx++){
		assert( !matrx->empty() && matrx->size() == matrx->begin()->size() );
		if (matrx->empty() || matrx->size() != matrx->begin()->size())
			return false;
		prm_counter += matrx->size();
	}

	assert( prm_counter == A.begin()->size() );

	(*this) = A;
	int prev_pos = 0;
	for (matrx = obj.begin(); matrx != obj.end(); matrx++){
		for (int i = 0; i < matrx->size(); i++)
			for(int j = 0; j < matrx->size(); j++)
				(*this)[prev_pos + i][prev_pos + j] -= (*matrx)[i][j];
		prev_pos += matrx->size();
	}
	return true;
}

//----------------------------------------------------------------------------
template<class T>
T FRSmatrix<T>::Det() const
{
	int col = NumColumns();
	int row = NumRows();
	double det = 1;

	if (row != col)
		printf( "Determinant a non-square matrix!\n");

	FRSmatrix<double> temp(*this);

	for (int k = 0; k < row; k++)
	{
		int indx = temp.Pivot(k);
		if (indx == -1)
			return 0;
		if (indx != 0)
			det = - det;
		det = det * temp.Val(k, k);
		for (int i = k + 1; i < row; i++)
		{
			double piv = temp.Val(i, k) / temp.Val(k, k);
			for (int j = k + 1; j < row; j++)
				temp.Val(i, j) -= piv * temp.Val(k, j);
		}
	}
	return (T)det;
}

//----------------------------------------------------------------------------
template<class T>
int FRSmatrix<T>::Pivot(int r)
{
	int row = NumRows();
	int col = NumColumns();
	int k = r;
	T amax = -1, temp;

	for (int i = r; i < row; i++)
		if ((temp = fabs(Val(i, r))) > amax && temp != 0.0)
		{
			amax = temp;
			k = i;
		}

		if (Val(k, r) == 0)
			return -1;

		if (k != r)
		{
			// swap k and r rows
			for (int j = 0; j < col; j++)
			{
				T q = Val(k, j);
				Val(k , j) = Val(r, j);
				Val(r, j) = q;
			}  
			return k;
		}
		return 0;
}

//----------------------------------------------------------------------------
template<class T>
void FRSmatrix<T>::Print(char *format)const
{
	FRSmatrix<T>* locRef = (FRSmatrix<T>*)this;
	// ugly trick, in the above line the const-ness is casted away
	if( this->empty() )
		return;

	typename FRSmatrix<T>::iterator	row;
	typename FRSvector<T>::iterator	col;
	int n_rows = this->size();
	int n_cols = this->begin()->size();

	for (row = locRef->begin(); row != locRef->end(); row++){
		if (row->size() > 1) {
			for (col = row->begin(); col != row->end(); col++)
				printf(format, *col);
			printf ("\n");
		}
		else
		{
			printf(format, *(row->begin()));
			printf("\n");
		}
	}

	printf("***************\n");
}

//----------------------------------------------------------------------------
template<class T>
void FRSmatrix<T>::Print()const
{
	Print( " %8.2f" );
}

//----------------------------------------------------------------------------
template<class T>
std::ostream& operator << (std::ostream& s, const FRSmatrix<T>& mat)
{
	s << mat.NumRows() << "  " << mat.NumColumns() << "\n";
	for( int r=0; r<mat.NumRows(); r++ )
	{
		for( int c=0; c<mat.NumColumns(); c++ )
			s << mat[r][c] << ' ';
		s << "\n";
	}
	return s;
}

//----------------------------------------------------------------------------

template<class T>
T MultRowColumn(const FRSmatrix<T> &b1, int i, 
	const FRSmatrix<T> &b2, int j)
{
	T sum=(T)0.;

	assert(b1.NumColumns() == b2.NumRows());
	assert(i >= 0 && i < b1.NumRows());
	assert(j >= 0 && j < b2.NumColumns());

	for(int k = 0; k < b1.NumColumns(); k++)
		sum += b1[i][k] * b2[k][ j];

	return(sum);
}




//----------------------------------------------------------------------------
template <class T>inline
bool cmpGT(T ls , T rs){return (ls > rs) ? true : false;}
//----------------------------------------------------------------------------
template <class T> inline
bool cmpLT(T ls , T rs){return (ls < rs) ? true : false;}
//----------------------------------------------------------------------------
template <class T> inline
bool cmpGE(T ls , T rs){return (ls >= rs) ? true : false;}
//----------------------------------------------------------------------------
template <class T> inline
bool cmpLE(T ls , T rs){return (ls <= rs) ? true : false;}
//----------------------------------------------------------------------------
template <class T> inline
bool cmpEQ(T ls , T rs){return (ls == rs) ? true : false;}
//----------------------------------------------------------------------------
template <class T> inline
bool cmpNE(T ls , T rs){return (ls != rs) ? true : false;}


//----------------------------------------------------------------------------
/*bool vRecord::Multiply(const std::vector<vRecord> & m, const vRecord &v)
{
	assert (!m.empty() && m.begin()->size() != 0 &&
			!v.empty() && v.size() == m.begin()->size());

	int n_rows = m.size();
	int n_cols = m.begin()->size();
	int m_rows = v.size();

	if (size() != n_rows)
		*this = vRecord(n_rows);

	for(int i = 0; i < n_rows; i++){
		(*this)[i] = 0.;
		for(int k = 0; k < n_cols; k++)
			(*this)[i] += m[i][k] * v[k];
	}
	return true;
}*/

typedef FRSmatrix<double> vMatrix;

/*
//----------------------------------------------------------------------------
bool vMatrix::Define(int m_rows, int m_cols, double val)
{
	if(!empty())
		Erase();
	if (m_rows == 0 || m_cols == 0)
		return false;

	vRecord r(m_cols, val); //row of mvarRows Set to 0 is default but just in case
	for(int col=0; col < m_rows; col++)
		//		push_back(r);
		((std::vector<vRecord>*)this)->push_back( r );
	// for efficiency, the rows have all the same length, there is no point in checking it

	return true;
}

//----------------------------------------------------------------------------
/* This function is redundant. It's already specified in the template class */
/* Removed by George Vosselman 
void vMatrix::Erase()
{
vMatrix::iterator record;

// Clean up
//	printf("In Erase \n");
if(!empty()){
for(record = begin(); record != end(); record++)
record->erase(record->begin(),record->end());
erase(begin(),end());
}
}
*/
//----------------------------------------------------------------------------
/*
vMatrix::vMatrix(const vMatrix& rhs)
{
if (!empty()) Erase();
for (vMatrix::const_iterator row = rhs.begin(); row != rhs.end(); row++)
push_back(*row);
}
*/
//----------------------------------------------------------------------------
/*
vMatrix::vMatrix(const Rotation2D &rot)
{
Define(2, 2);
for (int ir=0; ir<2; ir++)
for (int ic=0; ic<2; ic++)
Val(ir, ic) = rot.R(ir, ic);
}
*/
//----------------------------------------------------------------------------
/*
vMatrix::vMatrix(const Rotation3D &rot)
{
Define(3, 3);
for (int ir=0; ir<3; ir++)
for (int ic=0; ic<3; ic++)
Val(ir, ic) = rot.R(ir, ic);
}
*/
//----------------------------------------------------------------------------
/*
void vMatrix::Identity(int m_rows, int m_cols)
{
	if (m_rows != m_cols)
		printf("Unit of a non-square matrix\n");

	Define(m_rows, m_cols);
	int bound=(m_rows < m_cols)?m_rows:m_cols;
	for(int i =0; i < bound; i++)
		(*this)[i][i] = 1.;
}


//----------------------------------------------------------------------------
vMatrix & vMatrix::operator = (const vMatrix &rhs)
{
	if (this == &rhs)
		return *this;
	if (!empty()) Erase();
	for (vMatrix::const_iterator row = rhs.begin(); row != rhs.end(); row++)
		push_back(*row);
	return(*this);
}

//----------------------------------------------------------------------------
vMatrix & vMatrix::operator = (double rhs)
{
	if (!empty())
		for (int i = 0; i < size(); i++)
			for (int j = 0; j < begin()->size(); j++)
				(*this)[i][j] = rhs;
	return *this;
}

//----------------------------------------------------------------------------
vMatrix & vMatrix::Transpose (const vMatrix &b)
{
	assert( this != &b );
	Define( b.NumColumns(), b.NumRows() );
	for( int i = 0; i<b.NumRows(); i++ )
		for( int j=0; j<b.NumColumns(); j++ )
			(*this)[j][i] = b[i][j];
	return *this;
}

//----------------------------------------------------------------------------
vMatrix & vMatrix::operator += (const vMatrix &rhs)
{
	assert( !empty() );
	assert( !rhs.empty() );
	assert( rhs.size() == size() );
	assert( rhs.begin()->size() == begin()->size() );

	for (int i = 0; i < size(); i++)
		for (int j = 0; j < begin()->size(); j++)
			(*this)[i][j] += rhs.Val(i, j);
	return *this;
}

//----------------------------------------------------------------------------
vMatrix & vMatrix::operator -= (const vMatrix &rhs)
{
	assert( !empty() );
	assert( !rhs.empty() );
	assert( rhs.size() == size() );
	assert( rhs.begin()->size() == begin()->size() );

	for (int i = 0; i < size(); i++)
		for (int j = 0; j < begin()->size(); j++)
			(*this)[i][j] -= rhs.Val(i, j);
	return *this;
}

//---------------------------------------------------------------------------- 
vMatrix & vMatrix::operator *= (const vMatrix &m)
{
	vMatrix res;
	res.Multiply(*this, m);
	*this = res;
	return *this;
}

//----------------------------------------------------------------------------
vMatrix & vMatrix::operator *= (double val)
{
	assert( !empty() );
	assert( begin()->size() );

	for (int i = 0; i < size(); i++)
		for (int j = 0; j < begin()->size(); j++)
			(*this)[i][j] *= val;
	return *this;
}

//----------------------------------------------------------------------------
// Multiply all matrix elements by a constant
vMatrix & vMatrix::operator * (double c)
{
	if (!empty() && begin()->size())
		for (int i = 0; i < size(); i++)
			for (int j = 0; j < begin()->size(); j++)
				(*this)[i][j] *= c;
	return *this;
}

//----------------------------------------------------------------------------
/// Add a constant to all matrix elements
vMatrix & vMatrix::operator + (double c)
{
	if (!empty() && begin()->size())
		for (int i = 0; i < size(); i++)
			for (int j = 0; j < begin()->size(); j++)
				(*this)[i][j] += c;
	return *this;
}

//----------------------------------------------------------------------------
/// Subtract a constant from all matrix elements
vMatrix & vMatrix::operator - (double c)
{
	if (!empty() && begin()->size())
		for (int i = 0; i < size(); i++)
			for (int j = 0; j < begin()->size(); j++)
				(*this)[i][j] -= c;
	return *this;
}

//----------------------------------------------------------------------------
bool vMatrix::IncrementDiagonal(const vRecord & vec)
{
	if(empty() || size() != begin()->size() || vec.size() || size())
		return false;

	for (int i = 0; i < size(); i++)
		(*this)[i][i] += vec[i];
	return true;

}

//----------------------------------------------------------------------------
bool vMatrix::IncrementDiagonal(double val)
{
	if(empty() || size() != begin()->size())
		return false;

	for (int i = 0; i < size(); i++)
		(*this)[i][i] += val;
	return true;
}

/*
//----------------------------------------------------------------------------
void vMatrix::Multiply(vMatrix &A, vMatrix &B, vMatrix &X, int mvarARows, int mvarAColumns, int mvarBColumns)
{
int i, j, k;
double sum;

//Multiply A with B and put the result in X
for(i = 0; i < mvarARows; i++){
for(j = 0; j < mvarBColumns; j++){
sum = 0.0;
for(k = 0; k < mvarAColumns; k++)
sum = sum + A[i][k] * B[k][j];
X[i][j] = sum;
}
}
}
*/

//----------------------------------------------------------------------------
/*
void vMatrix::Multiply(const vMatrix &A, const vMatrix &B)
{
int n_rows = A.size();
int n_cols = A.begin()->size();

int m_rows = B.size();
int m_cols = B.begin()->size();

assert( n_cols == m_rows );

Define(n_rows, m_cols);

//Multiply A with B
for(int i = 0; i < n_rows; i++)
for(int j = 0; j < m_cols; j++)
for(int k = 0; k < n_cols; k++)
(*this)[i][j] += A[i][k] * B[k][j];
}
*/
//----------------------------------------------------------------------------
/*
vRecord vMatrix::operator * (const vRecord &m) const
{
	vRecord	res;

	int n_rows = size();
	int n_cols = begin()->size();

	int m_rows = m.size();

	assert( n_cols == m_rows );

	res.reserve(n_rows);
	double sum;
	//Multiply A with B and put the result in res
	for(int i = 0; i < n_rows; i++){
		sum = 0.;
		for(int k = 0; k < n_cols; k++)
			sum += (*this)[i][k] * m[k];
		res.push_back(sum);
	}
	return res;
}

//----------------------------------------------------------------------------
vMatrix vMatrix::operator * (const vMatrix &m) const
{
	vMatrix	res;

	int n_rows = size();
	int n_cols = begin()->size();

	int m_rows = m.size();
	int m_cols = m.begin()->size();

	//	if (n_cols != m_rows)
	//return res; 
	assert( n_cols == m_rows );

	res.Define(n_rows, m_cols);

	//Multiply A with B and put the result in res
	for(int i = 0; i < n_rows; i++)
		for(int j = 0; j < m_cols; j++)
			for(int k = 0; k < n_cols; k++)
				res[i][j] += (*this)[i][k] * m[k][j];
	return res;
}

//----------------------------------------------------------------------------
vMatrix vMatrix::operator + (const vMatrix &rhs) const
{
	vMatrix	res;

	int n_rows = size(); 
	int n_cols = begin()->size();

	int m_rows = rhs.size();
	int m_cols = rhs.begin()->size();

	assert( n_rows == m_rows && n_cols == m_cols );

	res.Define(n_rows, n_cols);

	//Multiply A with B and put the result in res
	for(int i = 0; i < n_rows; i++){
		for(int j = 0; j < n_cols; j++){
			res[i][j] = (*this)[i][j] + rhs[i][j];
		}
	}
	return res;
}

//----------------------------------------------------------------------------
vMatrix vMatrix::operator - (const vMatrix &rhs) const
{
	vMatrix	res;

	int n_rows = size(); 
	int n_cols = begin()->size();

	int m_rows = rhs.size();
	int m_cols = rhs.begin()->size();

	assert( n_rows == m_rows && n_cols == m_cols );

	res.Define(n_rows, n_cols);

	//Multiply A with B and put the result in res
	for(int i = 0; i < n_rows; i++){
		for(int j = 0; j < n_cols; j++){
			res[i][j] = (*this)[i][j] - rhs[i][j];
		}
	}
	return res;
}

//----------------------------------------------------------------------------
vMatrix vMatrix::MultTranspose (const vRecord *W) const
{
	int i, j, k;
	double sum;
	int n_rows = size();
	int n_cols = begin()->size();

	assert( !W || W->size() == n_rows );

	vMatrix res;
	res.Define(n_cols, n_cols);

	if (W == NULL){
		for(i = 0; i < n_cols; i++){
			for(j = i; j < n_cols; j++){
				sum = 0.0;
				for(k = 0; k < n_rows; k++)
					sum += (*this)[k][i] * (*this)[k][j];
				res[i][j] = sum;
			}
		}
	}
	else{
		for(i = 0; i < n_cols; i++){
			for(j = i; j < n_cols; j++){

				sum = 0.0;
				for(k = 0; k < n_rows; k++)
					sum += (*this)[k][i] * (*this)[k][j] * (*W)[k];
				res[i][j] = sum;
			}
		}
	}

	// Copy to lower triangular elements
	for(i = 0; i < n_cols; i++)
		for(j = 0; j < i; j++)
			res[i][j] = res[j][i];

	return res;
}

//----------------------------------------------------------------------------
vMatrix vMatrix::Transpose_MultBy (vRecord *_W, vMatrix *B) const
{
	int i, j, k;
	int n_rows = size();
	int n_cols = begin()->size();
	int m_rows, m_cols;
	vMatrix res;
	const vMatrix *D = NULL;
	vRecord *W = NULL;

	assert( !_W || _W->size() == n_rows );
	assert( !B || !B->empty() && B->NumRows() == n_rows );

	if (B){
		m_rows = B->size();
		m_cols = B->begin()->size();
		D = B;
	}
	else{
		m_rows = n_rows;
		m_cols = n_cols;
		D = this;
	}

	if (_W){
		W = _W;
	}
	else
		W = new vRecord(n_cols, 1.);

	res.Define(n_cols, m_cols);
	for(i = 0; i < n_cols; i++)
		for(j = 0; j < m_cols; j++)
			for(k = 0; k < n_rows; k++)
				res[i][j] += (*this)[k][i] * (*D)[k][j] * (*W)[k];

	if (!_W) W->erase(W->begin(), W->end());
	return res;
}

//----------------------------------------------------------------------------
vMatrix vMatrix::MultByTranspose (vRecord *_W, vMatrix *B) const
{
	int i, j, k;
	int n_rows = size();
	int n_cols = begin()->size();
	int m_rows, m_cols;
	vMatrix res;
	const vMatrix *D = NULL;
	vRecord *W = NULL;

	assert( !B || B->NumColumns() == n_cols );
	assert( !_W || _W->size() == n_cols );

	if (B){
		m_rows = B->size();
		m_cols = B->begin()->size();
		D = B;
	}
	else{
		m_rows = n_rows;
		m_cols = n_cols;
		D = this;
	}

	if (_W){
		W = _W;
	}
	else
		W = new vRecord(n_cols, 1.);

	res.Define(n_rows, m_rows);
	for(i = 0; i < n_rows; i++)
		for(j = 0; j < m_rows; j++)
			for(k = 0; k < n_cols; k++)
				res[i][j] += (*this)[i][k] * (*D)[j][k] * (*W)[k];

	if (!_W) W->erase(W->begin(), W->end());
	return res;
}

//----------------------------------------------------------------------------
bool vMatrix::CreateFromOuterProduct(vRecord *v, vRecord *w)
{

	int	m_rows = v->size();
	assert (!m_rows) ;

	int m_cols = (w == NULL)? m_rows : w->size();

	if(empty())
		Define(m_rows, m_cols);

	int n_rows = size(); 
	int n_cols = begin()->size();

	if (m_rows != n_rows && m_cols != n_cols)
		Define(m_rows, m_cols);

	if (w == NULL){
		for(int i = 0; i < n_rows; i++)
			for(int j = 0; j < n_cols; j++)
				(*this)[i][j] = (*v)[i]*(*v)[j];
	}
	else{
		for(int i = 0; i < n_rows; i++)
			for(int j = 0; j < n_cols; j++)
				(*this)[i][j] = (*v)[i]*(*w)[j];
	}
	return true;
}

//----------------------------------------------------------------------------
bool vMatrix::AddOuterProduct(const vRecord *v, int row0, int col0, const vRecord *w)
{

	assert( v && !( v->empty() ) );

	int n_rows = size();
	int m_rows = v->size();
	assert( n_rows && m_rows );

	int n_cols = begin()->size();
	int m_cols = (w == NULL)? m_rows : w->size();

	assert( row0 + m_rows <= n_rows && col0 + m_cols <= n_cols );

	if (w == NULL){
		for(int i = 0; i < n_rows; i++)
			for(int j = 0; j < n_cols; j++)
				(*this)[i][j] += (*v)[i]*(*v)[j];
	}
	else{
		for(int i = 0; i < m_rows; i++)
			for(int j = 0; j < m_cols; j++){
				double tmp = (*this)[row0 + i][col0 + j], a = (*v)[i], b = (*w)[j];
				/// ???NP what is the above line good for?
				(*this)[row0 + i][col0 + j] += (*v)[i]*(*w)[j];
			}
	}
	return true;
}

//----------------------------------------------------------------------------

bool vMatrix::AddPartial(const vMatrix &v, int r0, int c0)
{
	assert( r0 >= 0 && c0 >= 0 );
	assert( v.size()+r0 <= size() && v.begin()->size() + c0 <= begin()->size() );

	for (int i = 0; i < v.size(); i++)
		for (int j = 0; j < v.begin()->size(); j++)
			(*this)[r0 + i][c0 + j] += v[i][j];
	return true;
}

//----------------------------------------------------------------------------
bool vMatrix::ReplacePartial(const vMatrix &v, int r0, int c0)
{
	assert( r0 >= 0 && c0 >= 0 );
	assert( v.size()+r0 <= size() && v.begin()->size() + c0 <= begin()->size() );

	for (int i = 0; i < v.size(); i++)
		for (int j = 0; j < v.begin()->size(); j++)
			(*this)[r0 + i][c0 + j] = v[i][j];
	return true;
}

//----------------------------------------------------------------------------

vMatrix & vMatrix::Insert(const vMatrix &m, int first_row, int first_col,
	int num_rows, int num_cols,
	int row_offset, int col_offset)
{
	assert(first_row >=0 && first_col >= 0);
	assert(first_row+num_rows <= m.NumRows() && first_col+num_cols <= m.NumColumns());
	assert(row_offset >=0 && col_offset >= 0);
	assert(row_offset+num_rows <= NumRows() && col_offset+num_cols <= NumColumns());

	for (int ir=0; ir<num_rows; ir++)
		for (int ic=0; ic<num_cols; ic++)
			Val(ir + row_offset, ic + col_offset)  = m.Val(ir + first_row, ic + first_col);

	return *this;
}

//----------------------------------------------------------------------------
void vMatrix::Convert2NR(double *** m)const
{
	//long n_rows = size();
	//long n_cols = begin()->size();

	//*m = (double **) matrix(1L, n_cols, 1L, n_rows);
	//if( !*m )
	//	std::cerr << "allocation failure in vMatrix::Convert2NR\n";

	//for (int i = 1; i <= n_rows; i++)
	//	for(int j = 1; j <= n_cols; j++)
	//		(*m)[j][i] = (*this)[i-1][j-1];
}

//----------------------------------------------------------------------------

void vMatrix::Convert2LP( double **a ) const
{
	long row = NumRows();
	long col = NumColumns();

	*a = (double*) calloc( (row*col), sizeof(double) );
	if( !*a )
		std::cerr << "allocation failure in vMatrix::Convert2LP\n";
	else
	{
		for( int i=0; i<row; i++ )
			for( int j=0; j<row; j++ )
				(*a)[ i*row+j ] = (*this)[i][j];
	}

	return;
}

//----------------------------------------------------------------------------

void vMatrix::ConvertFromLP( const double *a, long row, long col )
{
	assert( row>=0 );
	assert( col>=0 );
	assert( a );

	Define( row, col );
	for( int i=0; i<row; i++ )
		for( int j=0; j<col; j++ )
			(*this)[i][j] = a[ i*row+j ];

	return;
}

//----------------------------------------------------------------------------
void vMatrix::ConvertFromNR(vMatrix *A, double ** m, long n_rows, long n_cols)
{
	assert( 0  );
	//code missing 
}

//----------------------------------------------------------------------------
//#ifndef EPS 
//#define EPS 1.E-6
//#endif
/*
bool vMatrix::Invert_3x3(vMatrix &Ainv) const
{

assert( size() == 3 && begin()->size() == 3 );

if (Ainv.size() != 3 || Ainv.begin()->size() != 3)
Ainv.Define(3, 3);

double a = (*this)[0][0], b = (*this)[0][1], c = (*this)[0][2];
double                    e = (*this)[1][1], f = (*this)[1][2];
double                                       i = (*this)[2][2];

double t2 = f*f;
double t4 = a*e;
double t7 = b*b;
double t9 = c*b;
double t12 = c*c;

double t15 = -t4*i+a*t2+t7*i-2.0*t9*f+t12*e;

if(fabs(t15) <= EPS){
return RankDef;
}
t15 = 1.0/t15;

double t20 = (-b*i+c*f)*t15;
double t24 = (b*f-c*e)*t15;
double t30 = (a*f-t9)*t15;

Ainv[0][0] = (-e*i+t2)*t15; Ainv[0][1] = -t20;           Ainv[0][2] = -t24;
Ainv[1][0] = -t20;          Ainv[1][1] = -(a*i-t12)*t15; Ainv[1][2] = t30;
Ainv[2][0] = -t24;          Ainv[2][1] = t30;            Ainv[2][2] = -(t4-t7)*t15;

return FullRank;
}
*/
//----------------------------------------------------------------------------
/*
bool vMatrix::InvertBySVD(vMatrix & m_N1) const
{
double	**nrU;
double	**v;
double	*w, *w_inv;
bool	matrixType = FullRank;
int i;

Convert2NR(&nrU);
long n_rows = size();
v = (double **) matrix(1, n_rows, 1, n_rows);
w = (double *) NRvector(1,n_rows);
w = (double *) NRvector(1,n_rows);
w_inv = (double *) NRvector(1,n_rows);

//	svdcmp(nrU, n_rows, n_rows, w, v);
for (i = 1; i <= n_rows; i++)
if (fabs(w[i]) > EPS)
w_inv[i] = 1./w[i];
else{
w_inv[i] = 0.;
matrixType = RankDef;
}

if (m_N1.empty() || m_N1.size() != n_rows || m_N1.begin()->size() != n_rows)
m_N1.Define(n_rows, n_rows);
else
m_N1 = 0.;
// Creating the g-inverse
for (i = 1; i <= n_rows; i++)
for (int j = 1; j <= n_rows; j++){
double sum = 0.;
for (int k = 1; k <= n_rows; k++)
sum += nrU[i][k] * w_inv[k] * v[j][k];
m_N1[i-1][j-1] = sum;
}
free_vector(w, 1, n_rows);
free_vector(w_inv, 1, n_rows);
free_matrix(v, 1, n_rows, 1, n_rows);
free_matrix(nrU, 1, n_rows, 1, n_rows);
return matrixType;
}
*/

//----------------------------------------------------------------------------
/*
bool vMatrix::Invert(vMatrix & A, int inversionType) 
{
switch (inversionType){
case STANDARD:
if (size() == 3){
if (!Invert_3x3(A)) 
return false;
}
else if (!Inverse(A)) 
return false;
break;
case SVD:
if (!InvertBySVD(A))
return false;
break;
default:
return false;
break;
}
return true;
}
*/
//----------------------------------------------------------------------------
/*
bool vMatrix::Inverse(vMatrix &Ainv) const
{
int TempInt, i, j, p; 
int LastCol; // Last column of the augmented matrix
double Multiplier;

int mvarColumns = size();
int mvarRows = begin()->size();

assert( mvarColumns == mvarRows );

if (Ainv.size() != mvarRows || Ainv.begin()->size() != mvarRows)
Ainv.Define(mvarRows, mvarRows);

vMatrix M; // row, colums
vVector PVT;

// Initialise matrix M - Augmented matrix
M.Define(mvarRows, 2 * mvarRows);

// Initialise vector PVT
int val=0;
for(int row=0; row < mvarRows; row++)
PVT.push_back(val);

LastCol = 2 * mvarRows; // Last column of the augmented matrix

// Create the Augmented matrix
for(i = 0; i < mvarRows; i++){	// Copy matrix A to augmented matrix
for(j = 0; j < mvarRows; j++)
M[i][j] = (*this)[i][j];
// Initialise the Identity matrix within the augmented matrix
M[i][mvarRows + i] = 1.0; // Main diagonal=1
}

// Initialise the pivot vector
for(i = 0; i < mvarRows; i++)
PVT[i] = i;

// Invert: Create an identity matrix on the left side of the augmented matrix
for(p = 0; p < mvarRows; p++){
// Pivot strategy: look for the element with the largest absolute value
for(i = p + 1; i < mvarRows; i++){
if(fabs(M[PVT[i]][p]) > fabs(M[PVT[p]][p])){
TempInt = PVT[p];
PVT[p] = PVT[i];
PVT[i] = TempInt;
}
}
if(fabs(M[PVT[p]][p]) <= EPS)//print "The matrix is singular"
return false;
for(i = p - 1; i >= 0; i--){ // Reduce upper values to zero
Multiplier = M[PVT[i]][p] / M[PVT[p]][p];

for(j = p + 1; j < LastCol; j++)
M[PVT[i]][j] = M[PVT[i]][j] - Multiplier * M[PVT[p]][j];
}

for(i = p + 1; i < mvarRows; i++){ //Reduce lower values to zero
Multiplier = M[PVT[i]][p] / M[PVT[p]][p];

for(j = p + 1; j < LastCol; j++)
M[PVT[i]][j] = M[PVT[i]][j] - Multiplier * M[PVT[p]][j];
}
}

// Test for singularity
if(M[PVT[mvarRows - 1]][mvarRows - 1] == 0)
//MsgBox "The matrix is singular"
return false;

// Divide by diagonal of augmented matrix
for(p = 0; p < mvarRows; p ++)
for(j = mvarRows; j < 2 * mvarRows; j++)
M[PVT[p]][j] = M[PVT[p]][j] / M[PVT[p]][p];

// Copy inverted matrix to A matrix
for(j = 0; j < mvarRows; j++){
i = mvarRows + j;
for(p = 0; p < mvarRows; p++)
Ainv[p][j] = M[PVT[p]][i];
}
return FullRank;
}
*/
//#undef EPS

//----------------------------------------------------------------------------
/*
vMatrix & vMatrix::Inverse( const vMatrix &matrix, double &cond )
{
int    job, *pivot;
double *z, det[2];

int row = matrix.NumRows();
int col = matrix.NumColumns();

assert( row == col ); // Check the matrix is square
*this = matrix;              // Copy the matrix to be inverted

pivot = (int *) malloc(row * sizeof(int));
z     = (double *) malloc(row * sizeof(double));

double* a;
Convert2LP( &a );

#if defined linux
dgeco_(a, &row, &col, pivot, &cond, z);
#else
#if defined hpux
dgeco(a, &row, &col, pivot, &cond, z);
#else
_dgeco(a, &row, &col, pivot, &cond, z);
#endif
#endif

if (1.0 + cond == 1.0)
fprintf(stderr, "Warning: bad condition of matrix (%5.2e)\n", cond);

job = 1;
#if defined linux
dgedi_(a, &row, &col, pivot, det, z, &job);
#else
#if defined hpux
dgedi(a, &row, &col, pivot, det, z, &job);
#else
_dgedi(a, &row, &col, pivot, det, z, &job);
#endif
#endif

ConvertFromLP( a, row, col );

free(pivot);  free(z); free(a);
return(*this);
}
*/
//----------------------------------------------------------------------------
/*
bool vMatrix::SparseMultiplication(vMatrix & A, std::vector<vMatrix> &obj)
{
	std::vector<vMatrix>::iterator matrx;
	int prm_counter = 0;

	assert( !A.empty() );
	// Size of the sparse matrix (sum of the sub matrices)
	for (matrx = obj.begin(); matrx != obj.end(); matrx++){
		assert( !matrx->empty() && matrx->size() == matrx->begin()->size() );
		prm_counter += matrx->size();
	}

	assert( prm_counter == A.begin()->size() );

	Define(A.size(), prm_counter);
	int prev_pos = 0;
	for (matrx = obj.begin(); matrx != obj.end(); matrx++){
		for (int i = 0; i < A.size(); i++)
			for(int j = 0; j < matrx->size(); j++)
				for(int k = 0; k < matrx->size(); k++)
					(*this)[i][prev_pos + j] += A[i][prev_pos + k]*(*matrx)[k][j];
		prev_pos += matrx->size();
	}
	return true;
}

//----------------------------------------------------------------------------

bool vMatrix::SparseAddition(vMatrix & A, std::vector<vMatrix> &obj)
{
	std::vector<vMatrix>::iterator matrx;
	int prm_counter = 0;

	assert( !A.empty() );
	// Size of the sparse matrix (sum of the sub matrices)
	for (matrx = obj.begin(); matrx != obj.end(); matrx++){
		assert( !matrx->empty() && matrx->size() == matrx->begin()->size() );
		prm_counter += matrx->size();
	}

	assert( prm_counter == A.begin()->size() );

	(*this) = A;
	int prev_pos = 0;
	for (matrx = obj.begin(); matrx != obj.end(); matrx++){
		for (int i = 0; i < matrx->size(); i++)
			for(int j = 0; j < matrx->size(); j++)
				(*this)[prev_pos + i][prev_pos + j] += (*matrx)[i][j];
		prev_pos += matrx->size();
	}
	return true;
}

//----------------------------------------------------------------------------

bool vMatrix::SparseSubtraction(vMatrix & A, std::vector<vMatrix> &obj)
{
	std::vector<vMatrix>::iterator matrx;
	int prm_counter = 0;

	assert( !A.empty() );
	// Size of the sparse matrix (sum of the syb matrices)
	for (matrx = obj.begin(); matrx != obj.end(); matrx++){
		assert( !matrx->empty() && matrx->size() == matrx->begin()->size() );
		if (matrx->empty() || matrx->size() != matrx->begin()->size())
			return false;
		prm_counter += matrx->size();
	}

	assert( prm_counter == A.begin()->size() );

	(*this) = A;
	int prev_pos = 0;
	for (matrx = obj.begin(); matrx != obj.end(); matrx++){
		for (int i = 0; i < matrx->size(); i++)
			for(int j = 0; j < matrx->size(); j++)
				(*this)[prev_pos + i][prev_pos + j] -= (*matrx)[i][j];
		prev_pos += matrx->size();
	}
	return true;
}

//----------------------------------------------------------------------------
double vMatrix::Det() const
{
	int col = NumColumns();
	int row = NumRows();
	double piv, det = 1;

	if (row != col)
		printf( "Determinant a non-square matrix!\n");

	vMatrix temp(*this);

	for (int k = 0; k < row; k++)
	{
		int indx = temp.Pivot(k);
		if (indx == -1)
			return 0;
		if (indx != 0)
			det = - det;
		det = det * temp.Val(k, k);
		for (int i = k + 1; i < row; i++)
		{
			double piv = temp.Val(i, k) / temp.Val(k, k);
			for (int j = k + 1; j < row; j++)
				temp.Val(i, j) -= piv * temp.Val(k, j);
		}
	}
	return det;
}

//----------------------------------------------------------------------------
/*
int vMatrix::Pivot(int r)
{
int row = NumRows();
int col = NumColumns();
int k = r;
double amax = -1, temp;

for (int i = r; i < row; i++)
if ((temp = fabs(Val(i, r))) > amax && temp != 0.0)
{
amax = temp;
k = i;
}

if (Val(k, r) == 0)
return -1;

if (k != r)
{
// swap k and r rows
for (int j = 0; j < col; j++)
{
double q = Val(k, j);
Val(k , j) = Val(r, j);
Val(r, j) = q;
}  
return k;
}
return 0;
}
*/
//----------------------------------------------------------------------------
/*
void vMatrix::Print(char *format)const
{
	vMatrix* locRef = (vMatrix*)this;
	// ugly trick, in the above line the const-ness is casted away
	if( empty() )
		return;

	vMatrix::iterator	row;
	vRecord::iterator	col;
	int n_rows = size();
	int n_cols = begin()->size();

	for (row = locRef->begin(); row != locRef->end(); row++){
		if (row->size() > 1) {
			for (col = row->begin(); col != row->end(); col++)
				printf(format, *col);
			printf ("\n");
		}
		else
		{
			printf(format, *(row->begin()));
			printf("\n");
		}
	}

	printf("***************\n");
}

//----------------------------------------------------------------------------
void vMatrix::Print()const
{
	Print( " %8.2f" );
}

//----------------------------------------------------------------------------
std::ostream& operator << (std::ostream& s, const vMatrix& mat)
{
	s << mat.NumRows() << "  " << mat.NumColumns() << "\n";
	for( int r=0; r<mat.NumRows(); r++ )
	{
		for( int c=0; c<mat.NumColumns(); c++ )
			s << mat[r][c] << ' ';
		s << "\n";
	}
	return s;
}

double MultRowColumn(const vMatrix &b1, int i, const vMatrix &b2, int j)
{
	double sum=0.0;

	assert(b1.NumColumns() == b2.NumRows());
	assert(i >= 0 && i < b1.NumRows());
	assert(j >= 0 && j < b2.NumColumns());

	for(int k = 0; k < b1.NumColumns(); k++)
		sum += b1[i][k] * b2[k][ j];

	return(sum);
}

//#include "vMatrix.tcc"
*/

#endif


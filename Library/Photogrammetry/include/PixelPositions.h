#ifndef _PixelPositions_h_
#define _PixelPositions_h_

/*--------------------------------------------------------------------------------
This file contains the definitions of the following classes:

  PixelPositions  - A set of positions in a digital image

--------------------------------------------------------------------------------*/

#include <vector>
#include "PixelPosition.h"
#include "Photogrammetry.h"

//------------------------------------------------------------------------------
/// A vector of positions in a digital image
//------------------------------------------------------------------------------

class PHO_DLL PixelPositions : public std::vector<PixelPosition>
{
  public:

    /// Default constructor
    PixelPositions() : std::vector<PixelPosition>() {}

    /// Default destructor
    ~PixelPositions() {};
};
#endif /* _PixelPositions_h_ */   /* Do NOT add anything after this line */

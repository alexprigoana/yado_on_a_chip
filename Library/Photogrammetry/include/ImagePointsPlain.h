#ifndef _ImagePointsPlain_h_
#define _ImagePointsPlain_h_

/* --------------------------------------------------------------------------------
This file contains the definitions of the following classes:

  ImagePointsPlain  - A set of non-stochastic image points

-------------------------------------------------------------------------------- */
#include "VectorPoint.h"
#include "ImagePointPlain.h"
#include "Photogrammetry.h"

//------------------------------------------------------------------------------
/// A vector of plain image points
//------------------------------------------------------------------------------

class PHO_DLL ImagePointsPlain : public VectorPoint<ImagePointPlain>
{
  public:

  /// Default constructor
  ImagePointsPlain() : VectorPoint<ImagePointPlain>() {}

  /// Default destructor
  ~ImagePointsPlain() {};
};
#endif /* _ImagePointsPlain_h_ */   

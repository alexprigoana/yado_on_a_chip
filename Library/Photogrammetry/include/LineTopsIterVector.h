#ifndef LINETOPSITERVECTOR_H
#define LINETOPSITERVECTOR_H

#include <vector>
#include "LineTopologies.h"
#include "Photogrammetry.h"

//it is very very very very very very stupid to store iterators!!!!!!!!!!!!!!!!!!!!!!!!
class PHO_DLL LineTopsIterVector: public std::vector <LineTopologies::iterator>
{
public:
	/// Default constructor
	LineTopsIterVector() : std::vector <LineTopologies::iterator>() {}

	/// Default destructor
	~LineTopsIterVector() {};

	/// Delete all entries
	void Clear()
	{if (!empty()) erase(begin(), end());}

	/// Check if a topology is in the vector
	bool Contains(const LineTopology &top) const;

	/// Check if a topologies iterator is part of the vector
	bool Contains(LineTopologies::const_iterator top) const;

	/// Remove a topologies iterator
	bool Remove(LineTopologies::iterator top);

	/// Return the reference
	LineTopsIterVector &LineTopsIterVectorRef()
	{ return *this; }

	/// Return the const reference
	const LineTopsIterVector &LineTopsIterVectorRef() const
	{ return *this; }
};
#endif // LINETOPSITERVECTOR_H

#ifndef _ModelPoints_h_
#define _ModelPoints_h_

#include "Photogrammetry.h"
#include "VectorPoint.h"
#include "ModelPoint.h"
struct ModelPts;

//------------------------------------------------------------------------------
/// A vector of model points
//------------------------------------------------------------------------------

class PHO_DLL ModelPoints : public VectorPoint<ModelPoint>
{
  public:

    /// Default constructor
    ModelPoints() : VectorPoint<ModelPoint>() {}

    /// Construct by converting a C object
    ModelPoints(ModelPts *modelpts) : VectorPoint<ModelPoint>()
       { C2Cpp(modelpts); }

    /// Destructor
    ~ModelPoints() {};

    /// Conversion from C to C++ object
    void C2Cpp(ModelPts *);

    /// Conversion from C++ to C object
    void Cpp2C(ModelPts **) const;

    /// Read the model points from a file
    /** @param filename File with model points
        @return 0 - failure, 1 - success
    */
    int Read(char *filename);

    /// Write the model points to a file
    /** @param filename File for model points
        @return 0 - failure, 1 - success
    */
    int Write(char *filename) const;

    /// Print the model points to stdout
    void Print() const;
};
#endif /* _ModelPoints_h_ */   /* Do NOT add anything after this line */

#ifndef _ImagePoints_h_
#define _ImagePoints_h_

/* --------------------------------------------------------------------------------
This file contains the definitions of the following classes:

  ImagePoints  - A set of stochastic image points

-------------------------------------------------------------------------------- */


#include "VectorPoint.h"
#include "ImagePoint.h"
#include "Photogrammetry.h"

class CameraPoints;
class InteriorOrientation;
struct ImgPts;
//------------------------------------------------------------------------------
/// A vector of image points
//------------------------------------------------------------------------------

class PHO_DLL ImagePoints : public VectorPoint<ImagePoint>
{
    /// Name of the data set
    char *name;
    	
  public:

    /// Default constructor
    ImagePoints() : VectorPoint<ImagePoint>()
      { name = NULL; } 
    
    /// Construct by reading image points from a file
    /** @param filename File with image points
        @param success  Success status of the read function,
                        0 - failure, 1 - success.
    */
    ImagePoints(char *filename, int *success) : VectorPoint<ImagePoint>()
      { *success = Read(filename); }

    /// Construct by converting a C object
    ImagePoints(ImgPts *imgpts) : VectorPoint<ImagePoint>()
      { C2Cpp(imgpts); }
    
    /// Construct by transforming camera points into image points
    /** This transformation converts the mm coordinates to coordinates in
        pixels and adds the lens distortion.
        @param campts A vector of camera points
        @param intor  Interior orientation
    */
    ImagePoints(const CameraPoints *campts, const InteriorOrientation *intor)
      : VectorPoint<ImagePoint>()
      { Camera2ImagePoints(campts, intor); } 

    /// Destructor
    ~ImagePoints() 
       { delete [] name; }

    /// Conversion from C to C++ object
    void C2Cpp(ImgPts *);

    /// Conversion from C++ to C object
    void Cpp2C(ImgPts **) const;

    /// Read the image points from a file
    /** @param filename File with image points
        @return         Success status, 0 - failure, 1 - success
    */
    int Read(char *filename);

    /// Write the image points to a file
    /** @param filename File for the image points
        @return         Success status, 0 - failure, 1 - success
    */
    int Write(char *filename) const;

    /// Print the image points to stdout
    void Print() const;
    
    /// Return the name of the data set
    const char *Name() const
    	{ return name; }	    

    /// Transform the camera points into image points
    /** This transformation converts the mm coordinates to coordinates in
        pixels and adds the lens distortion.
        @param campts A vector of camera points
        @param intor  Interior orientation
    */
    void Camera2ImagePoints(const CameraPoints *campts,
                            const InteriorOrientation *intor);

    /// Transform the image points into camera points
    /** This transformation converts the pixel coordinates to
        coordinates in mm and corrects for the lens distortion.
        @param intor  Interior orientation
        @return       A pointer to a vector of camera points
    */
    CameraPoints *Image2CameraPoints(const InteriorOrientation *intor) const;
};

#endif /* _ImagePoints_h_ */   /* Do NOT add anything after this line */

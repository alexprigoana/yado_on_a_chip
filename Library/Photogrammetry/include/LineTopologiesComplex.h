#ifndef _LineTopologies_Complex_h_
#define _LineTopologies_Complex_h_

#include "Photogrammetry.h"
#include "LineTopology.h"
#include "LineTopologies.h"
class ObjectPoints;
class Position2D;
class Vector3D;
class Vector2D;
class Position3D;

class PHO_DLL LineTopologyComplex
{
public:
	LineTopologyComplex() {};
	LineTopologyComplex(const LineTopology& outerRing, const LineTopologies& innerRinges)
		:m_outerRing(outerRing), m_innerRinges(innerRinges) {};
	~LineTopologyComplex() {};

public:
	LineTopology& OuterRing(){return m_outerRing;}
	LineTopologies& InnerRings(){return m_innerRinges;}
	LineTopologies ToLineTopologies();

	bool ReplaceOuterRing(const LineTopology& outerRing);
	bool AddInnerRing(const LineTopology& innerRing);

	bool CoverPoint(const ObjectPoints& objPnts, const Position3D& pnt) const;
	bool CoverPoint(const ObjectPoints& objPnts, const Position2D& pnt) const;
	bool CoverPoint(const ObjectPoints& objPnts, const Vector3D& pnt) const;
	bool CoverPoint(const ObjectPoints& objPnts, const Vector2D& pnt) const;
	DataBounds2D DeriveBound(const ObjectPoints& objPnts);
	double Area(ObjectPoints objPnts) const;
	double Distance2Pnt(const ObjectPoints& objPnts, const Vector3D& inPnt) const;

	TIN DeriveTIN(const ObjectPoints& boundPnts, const ObjectPoints& otherPoints);

	//Dong
	void SetAttribute(const LineTopologyTag tag, const int value);
	int GetAttribute(const LineTopologyTag tag);

private:
	bool InsidePolygonJordan(const Vector2D& curPnt, const ObjectPoints& pts, const PointNumberList& top) const;

protected:
	LineTopology m_outerRing;
	LineTopologies m_innerRinges;
};

class PHO_DLL LineTopologiesComplex : public std::vector<LineTopologyComplex>
{
public:
	LineTopologiesComplex() {}
	LineTopologiesComplex(const LineTopologies& inLineTops);

	//Dong
	LineTopologies ToLineTopologies();
	void SetAttribute(const LineTopologyTag tag, const int value);
	
private:

};

#endif

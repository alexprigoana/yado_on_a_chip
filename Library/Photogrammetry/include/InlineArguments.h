#ifndef __IN_LINE_ARGUMENTS_H__
#define __IN_LINE_ARGUMENTS_H__

#include "Photogrammetry.h"

class PHO_DLL InlineArguments 
{
  protected:
            
    /// Number of inline arguments
    int argc;
    
    /// Inline arguments
    char **argv;
    
  public:
    /// Default constructor
    InlineArguments()
      {argc = 0; argv = NULL;}
      
    /// Constructor
    InlineArguments(int c, char **v)
      {argc = c; argv = v;}
      
    /// Return the index of an argument
    int Index(const char *wanted_arg) const;
    
    /// Check if an argument exists
    bool Contains(const char *wanted_arg) const;
    
    /// Return the integer value of an argument
    int Integer(const char *wanted_arg, int default_value) const;

    /// Return the float value of an argument
    float Float(const char *wanted_arg, float default_value) const;

    /// Return the double value of an argument
    double Double(const char *wanted_arg, double default_value) const;

    /// Return the string value of an argument
    const char * String(const char *wanted_arg,
                        const char *default_value) const;

    /// Return the string value of an argument
    char * String(const char *wanted_arg) const;
};

#endif __IN_LINE_ARGUMENTS_H__
#ifndef _Point2D_h_
#define _Point2D_h_

/*--------------------------------------------------------------------------------
This file contains the definitions of the following classes:

  Point2D  - A stochastic point in a two-dimensional coordinate system

--------------------------------------------------------------------------------*/

#include "Photogrammetry.h"
#include "Point2DPlain.h"
#include "Covariance2D.h"

//------------------------------------------------------------------------------
/// A stochastic point in a two-dimensional coordinate system
//------------------------------------------------------------------------------

class PHO_DLL Point2D : public Point2DPlain, public Covariance2D 
{
  public:

    /// Default constructor
    Point2D() : Point2DPlain(), Covariance2D() {}

    /// Construct from coordinates, a point number and (co-)variances
    Point2D(double x, double y, int num, double v_x, double v_y, double cv_xy) 
	: Point2DPlain(x, y, num), 
	  Covariance2D(v_x, v_y, cv_xy) {} ;
	 
    /// Construct from a vector, a point number and a covariance matrix
    Point2D(const Vector2D &vec, const PointNumber &n, const Covariance2D &var)
    	: Point2DPlain(vec, n), 
    	  Covariance2D(var) {};

    /// Default destructor
    ~Point2D() {};
};
#endif /* _Point2D_h_ */   /* Do NOT add anything after this line */

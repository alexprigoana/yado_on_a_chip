#ifndef _Orientation2D_h_
#define _Orientation2D_h_

/*--------------------------------------------------------------------------------
This file contains the definitions of the following classes:

  Orientation2D         - An orientation in a two-dimensional coordinate system

--------------------------------------------------------------------------------*/

#include "Photogrammetry.h"
#include "Rotation2D.h"

class PHO_DLL Orientation2D : public Rotation2D 
{
  public:
    /// Default constructor
    Orientation2D() : Rotation2D() {}

    /// Default destructor
    ~Orientation2D() {}
};
#endif

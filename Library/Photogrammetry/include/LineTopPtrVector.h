#ifndef LINETOPPTRVECTOR_H
#define LINETOPPTRVECTOR_H

#include <vector>
#include "Photogrammetry.h"
#include "LineTopologies.h"

class PHO_DLL LineTopPtrVector : public std::vector <LineTopology *>
{
public:
	LineTopPtrVector() : std::vector <LineTopology *>() {}

	~LineTopPtrVector() {};

	void Clear()
	{if (!empty()) erase(begin(), end());}
};

#endif // LINETOPPTRVECTOR_H

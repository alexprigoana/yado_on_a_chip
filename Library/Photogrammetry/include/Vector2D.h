
/*
    Copyright 2010 University of Twente and Delft University of Technology
 
       This file is part of the Mapping libraries and tools, developed
  for research, education and projects in photogrammetry and laser scanning.

  The Mapping libraries and tools are free software: you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of the License,
                   or (at your option) any later version.

 The Mapping libraries and tools are distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
                GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
          along with the Mapping libraries and tools.  If not, see
                      <http://www.gnu.org/licenses/>.

----------------------------------------------------------------------------*/



/*!
 * \file
 * \brief Interface to Class Vector2D - A vector in a two-dimensional coordinate system
 *
 */
/*!
 * \class Vector2D
 * \ingroup Photogrammetry
 * \brief Interface to Class Vector2D - A vector in a two-dimensional coordinate system
 *
 * <table width="100%" border=0 cellpadding=0 cellspacing=2 bgcolor="#eeeeee"><tr><td>
 *
 * \author        \Pierre_Ermes
 * \date		January 1998 (Created)
 *
 * \remark \li Define base geometries, Vector2D.
 *
 * \todo None
 *
 * \bug None
 *
 * \warning None
 *
 *
 */

#ifndef _VECTOR2D_H_
#define _VECTOR2D_H_ 
/*--------------------------------------------------------------------
*   Project   : STW, close range photogrammetry, piping installations
*
*   File made : januari 1998
*   Author    : Pierre Ermes
*	Modified  :
*   Purpose   : Define base geometries, Vector2D.
*
*
*--------------------------------------------------------------------*/

//#include <stdlib.h>
//#include <limits.h>
#include <math.h>
//#include <strings.h>
//#include <iostream>
#include "Photogrammetry.h"
#include <assert.h>

class Vector3D;

/// A vector in a two-dimensional coordinate system
class PHO_DLL Vector2D 
{
protected:
        /// The two elements of the vector
	double x[2];
	//double x[0], x[1];

public:
	Vector2D() { x[0] = x[1] = 0; }
	Vector2D(double xv, double yv) { x[0] = xv; x[1] = yv;}
	Vector2D(const double *v) { x[0] = v[0]; x[1] = v[1]; }
	Vector2D(const Vector3D &v);
	Vector2D(const Vector2D &v) { x[0] = v.x[0]; x[1] = v.x[1]; }
	~Vector2D() {}

    Vector2D& operator=(const Vector2D &v);
//	double X(int i) const { return x[i]; }
	double X() const { return x[0]; }
	double Y() const { return x[1]; }
	double & operator[] (int i) { assert(i>=0&&i<2); return x[i];}
	double operator[] (int i) const {assert(i>=0&&i<2); return x[i];}
//	double &X(int i) { return x[i]; }
	double &X() { return x[0]; }
	double &Y() { return x[1]; }

	/// Squared length.
	double SqLength() const;
	double Length() const;
	Vector2D PartialDeriv(int param) const;
	const class Vector2D &vect() const { return *this; }
	Vector2D &vect() { return *this; }
	const Vector2D &operator = (const Vector3D &v);
	double DotProduct(const Vector2D &v) const;
	Vector2D Align(const Vector2D &v) const;
    int AreaSign(const Vector2D &p1, const Vector2D &p2) const;
	Vector2D Normalize() const;
	Vector2D Normal() const;
	
	const Vector2D &operator += (const Vector2D &v);
	const Vector2D &operator -= (const Vector2D &v);
	const Vector2D &operator *= (double d);
	const Vector2D &operator /= (double d);

	PHO_DLL friend Vector2D operator +(const Vector2D &v1, const Vector2D &v2);
	PHO_DLL friend Vector2D operator -(const Vector2D &v1, const Vector2D &v2);
	PHO_DLL friend Vector2D operator *(const Vector2D &v1, double d);
	PHO_DLL friend Vector2D operator *(double d, const Vector2D &v1);
	PHO_DLL friend Vector2D operator /(const Vector2D &v1, double d);
	PHO_DLL friend bool operator == (const Vector2D &v1, const Vector2D &v2);
	PHO_DLL friend bool operator != (const Vector2D &v1, const Vector2D &v2) { return !(v1==v2); }
	PHO_DLL friend double Angle2D(const Vector2D &v1, const Vector2D &v2);
};

#endif

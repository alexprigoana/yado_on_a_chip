#ifndef __PHOTOGRAMMETRY_H__
#define __PHOTOGRAMMETRY_H__

// GV: In the next line I replaced WIN32 by __MSVC__
// to avoid API stuff for GNU C++
// #ifdef WIN32
//#ifdef __MSVC__  
//----------------------------------------------------------------------
// For Microsoft Visual C++, externally accessible symbols must be
// explicitly indicated with DLL_API, which is somewhat like "extern."
//
// The following ifdef block is the standard way of creating macros
// which make exporting from a DLL simpler. All files within this DLL
// are compiled with the DLL_EXPORTS preprocessor symbol defined on the
// command line. In contrast, projects that use (or import) the DLL
// objects do not define the DLL_EXPORTS symbol. This way any other
// project whose source files include this file see DLL_API functions as
// being imported from a DLL, wheras this DLL sees symbols defined with
// this macro as being exported.
//----------------------------------------------------------------------
#ifdef WIN32
#ifdef PHOTOGRAMMETRY_EXPORTS
#define PHO_DLL __declspec(dllexport)
#else
#define PHO_DLL __declspec(dllimport)
#endif
#else
#define PHO_DLL //ATTENTION, this change is already contained in the Working Version 1!!
#endif
//----------------------------------------------------------------------
// DLL_API is ignored for all other systems
//----------------------------------------------------------------------
//#else
//#define PHO_DLL
//#endif

#endif

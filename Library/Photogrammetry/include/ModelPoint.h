#ifndef _ModelPoint_h_
#define _ModelPoint_h_

#include "Photogrammetry.h"
#include "Point3D.h"

struct ModelPt;

class PHO_DLL  ModelPoint : public Point3D {

  public:

    /// Default constructor
    ModelPoint() : Point3D() {}

    /// Construct with specified values
    /** @param x     X-coordinate
        @param y     Y-coordinate
        @param z     Z-coordinate
        @param num   Model point number
        @param v_x   Variance of the X-coordinate
        @param v_y   Variance of the Y-coordinate
        @param v_z   Variance of the Z-coordinate
        @param cv_xy Co-variance of the X- and Y-coordinate
        @param cv_xz Co-variance of the X- and Z-coordinate
        @param cv_yz Co-variance of the Y- and Z-coordinate
    */
    ModelPoint(double x, double y, double z, int num, 
	    double v_x, double v_y, double v_z,
	    double cv_xy, double cv_xz, double cv_yz)
	: Point3D(x, y, z, num, v_x, v_y, v_z, cv_xy, cv_xz, cv_yz) {}
	
    /// Construct by converting a C object
    ModelPoint(ModelPt *modelpt) : Point3D()
      { C2Cpp(modelpt); }

    /// Construct from a vector, point number and a covariance matrix
    /** @param pos Coordinates of the model point
        @param num Point number of the model point
        @param cov Co-variance matrix of the model point
    */
    ModelPoint(const Vector3D &pos, const PointNumber &num,
               const Covariance3D &cov)
    	: Point3D(pos, num, cov) {}

    /// Default destructor
    ~ModelPoint() {}
      
    /// Copy assignment
    ModelPoint & operator = (const ModelPoint & point);
      
    /// Conversion from C to C++ object
    void C2Cpp(ModelPt *);

    /// Conversion from C++ to C object
    void Cpp2C(ModelPt **) const;
};
#endif /* _ModelPoint_h_ */   /* Do NOT add anything after this line */

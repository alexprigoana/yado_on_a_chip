#ifndef _ObjectPoints2D_h_
#define _ObjectPoints2D_h_


#include "VectorPoint.h"
#include "ObjectPoint2D.h"
#include "ImagePoints.h"
#include "ImageGrid.h"
//#include "Database4Cpp.h"
struct ObjPts2D;


//------------------------------------------------------------------------------
/// A vector of two-dimensional object points
//------------------------------------------------------------------------------

class PHO_DLL ObjectPoints2D : public VectorPoint<ObjectPoint2D>
{
  public:

    /// Default constructor
    ObjectPoints2D() : VectorPoint<ObjectPoint2D>() {}

    /// Construct by reading a file
    ObjectPoints2D(const char *filename) : VectorPoint<ObjectPoint2D>()
    	{ Read(filename); }	     	

    /// Construct by converting a C object
    ObjectPoints2D(ObjPts2D *objpts) : VectorPoint<ObjectPoint2D>()
	{ C2Cpp(objpts); }

    /// Construct from image points and an image grid definition
    /** The row-column coordinates of the image points are converted to the
        X- en Y-coordinates of the object points through the image grid
        definition.
        @param imgpts Image points
        @param grid   Image grid definition
    */
    ObjectPoints2D(const ImagePoints &imgpts, const ImageGrid &grid);

    /// Default destructor
    ~ObjectPoints2D() {};

    /// Copy assignment
    ObjectPoints2D & operator = (const ObjectPoints2D &);

    /// Return the readable reference
    const ObjectPoints2D &RefObjectPoints2D() const
        { return *this; } 

    /// Return the writable reference
    ObjectPoints2D &RefObjectPoints2D()
        { return *this; } 

    /// Convert from C to C++ object
    void C2Cpp(ObjPts2D *);

    /// Convert from C++ to C object
    void Cpp2C(ObjPts2D **) const;

    /// Read object points from a file
    /** @param filename Name of the file with 2D object points
        @return 0 - failure, 1 - success
    */
    int Read(const char *filename);

    /// Write object points to a file
    /** @param filename Name of the file for the 2D object points
        @return 0 - failure, 1 - success
    */
    int Write(const char *filename) const;

    /// Print the object points to stdout
    void Print() const;

    /// Write the header of a point file
    /** The file is opened by this function and left open. Usage unknown.
        @param filename Name of the 2D point file to be created
        @return 0 - failure, 1 - success.
    */
    int WriteHeader(const char *filename) const;
};
#endif /* _ObjectPoints2D_h_ */   /* Do NOT add anything after this line */

#ifndef _Plane_h_
#define _Plane_h_

#include "Photogrammetry.h"
#include "Position3D.h"
#include "FeatureNumber.h"
#include "Line3D.h"

class ObjectPoints;
class LineTopology;
//the plane is constructed with the equation ax+by+cz=d
//normal is (a, b, c), distance is d

class PHO_DLL Plane : public FeatureNumber
{
protected:

	/// Normal vector
	Vector3D normal;

	/// Distance of plane to the origin
	double distance;

	/// Label of the plane
	int label;

	/// Number of points used for sum and moment values
	int num_pts;

	/// Sum of the coordinates of the points in the plane
	Vector3D *coord_sum;

	/// Square sums of the coordinates of the points in the plane
	double *moments;

	/// Offset for moment computations
	Vector3D *offset;

	/// Smallest eigenvalue of plane determination from moments
	double smallest_eigenvalue;

public:
	/// Default constructor
	Plane();

	/// Construct plane from point and normal vector
	Plane(const Vector3D& point, const Vector3D& normalVec);

	/// Construct plane from three positions
	Plane(const Position3D &p1, const Position3D &p2, const Position3D &p3);
	Plane(const ObjectPoints& objPnts, const LineTopology& lineTop);

	/// Copy constructor
	Plane(const Plane &pl) : FeatureNumber() { InitialisePointers(); *this = pl; } 

	/// Default destructor
	~Plane() {Erase();}

private:
	/// Initialise the pointers (used in constructors only)
	void InitialisePointers();

public:
	/// Initialisation
	void Initialise();

	/// Copy assignament
	Plane& operator=(const Plane& pl); 
	PHO_DLL friend bool operator == (const Plane& lhr, const Plane& rhr);

	/// Return the readable reference
	const Plane &PlaneReference() const {return(*this);}

	/// Return the writable reference
	Plane &PlaneReference() {return(*this);}

	/// Print the plane to stdout
	void Print() const;

	/// Set the normal vector of the plane as the cross product of two vectors
	void SetNormal(const Vector3D &vec1, const Vector3D &vec2);	

	/// Use the specified vector as the normal vector of this plane
	void SetNormal(const Vector3D &vec) {normal = vec;}

	/// Set the distance of the plane to the origin
	void SetDistance(double dist) {distance = dist;}

	/// Set the distance of a non-vertical plane
	/** This function sets the distance to c - XGrad() * vec.X() -
	YGrad() * vec.Y(). The usage of this function is discouraged
	*/
	void SetDistance(double c, const Vector3D &vec);	

	// Not documented. Function used for interpolation in planes.
	double Interp(double, double) const;

	/// Gradient in X-direction. Do not use this for vertical planes!
	/** The function returns the negative X-component of the normal vector
	divided by the Z-component of the normal vector. It does NOT check
	whether the Z-component is zero, and causes then a division by zero!
	*/
	double XGrad() const {return (-normal.X()/normal.Z());}

	/// Gradient in Y-direction. Do not use this for vertical planes!
	/** The function returns the negative Y-component of the normal vector
	divided by the Z-component of the normal vector. It does NOT check
	whether the Z-component is zero, and causes then a division by zero!
	*/
	double YGrad() const {return (-normal.Y()/normal.Z());}

	/// Return the readable label
	int Label() const { return label; }

	/// Return the writable label
	int &Label() { return label; }

	/// Return the readable distance
	double Distance() const { return distance; }

	/// Return the writable distance
	double &Distance() { return distance; }

	/// Return the readable normal vector
	const Vector3D& Normal() const { return normal; }

	/// Return the writable normal vector
	Vector3D &Normal() { return normal; }

	/// Return the readable smallest eigenvalue
	double SmallestEigenvalue() const { return smallest_eigenvalue; }

	/// Determine the distance of a point to the plane
	double Distance(const Position3D &) const;

	/// Calculate Z-coordinate at the specified XY-location
	/** @param X X-coordinate
	@param Y Y-coordinate
	@param success 0 - failure because the plane is vertical. 1 - success.
	@return Z-coordinate at the specified XY-location
	*/
	double Z_At(double X, double Y, int *success) const;

	/// Project a 3D position onto the plane
	Position3D Project(const Position3D &pos) const;

	/// Determine the intersection point of a line and a plane
	/** @param line Line to be intersected with the plane
	@param plane Plane to be intersected with the line
	@param pos Calculated intersection point
	@return 0 - failure (line parallel to plane), 1 - success.
	*/
	PHO_DLL friend bool IntersectLine3DPlane(const Line3D &line, const Plane &plane,
		Position3D &pos);
	/// Determine the intersection line of two planes
	/** @param plane1 First plane
	@param plane2 Second plane
	@param line   Calculated intersection line
	@return 0 - failure (parallel planes), 1 - success.
	*/
	PHO_DLL friend bool Intersect2Planes(const Plane &plane1, const Plane &plane2, Line3D &line);

	/// Check if plane is horizontal
	/** A plane is classified horizontal if the angle of the normal vector
	with the positive or negative Z-axis is less than or equal to the
	specified tolerance.
	@param angle_tolerance Maximum allowed angle in radians
	@return 0 - Plane is not horizontal. 1- Plane is horizontal.
	*/
	bool IsHorizontal(double angle_tolerance) const;


	/// Check if plane is vertical
	/** A plane is classified vertical if the inner product of the normal vector
	with the positive or negative Z-axis is less than or equal to the
	specified tolerance.
	@param angle_tolerance Maximum allowed angle in radians
	@return 0 - Plane is not vertical. 1- Plane is vertical.
	*/
	bool IsVertical(double angle_tolerance) const;


	/// Add a point to the plane and (optionally) recalculate the parameters
	bool AddPoint(const Position3D &pos, bool recalculate=true);

	/// Remove a point from the plane and (optionally) recalculate 
	bool RemovePoint(const Position3D &pos, bool recalculate=true);

	/// Add or remove a point from the plane and (optionally) recalculate 
	bool ChangePoint(const Position3D &pos, bool add, bool recalculate=true);

	/// Recalculate plane parameters from moments
	bool Recalculate();

	/// Return the number of points in a plane
	int NumberOfPoints() const {return num_pts;}

	/// Return the centre of gravity of points added to the plane
	/** If no points have been added using AddPoint, the position returned
	contains coordinates INF
	*/
	Position3D CentreOfGravity() const;

	/// Erase plane data
	void Erase();

	/// Swap the normal vector direction
	void SwapNormal();
};
#endif /* _Plane_h_ */  /* Don't add after this point */

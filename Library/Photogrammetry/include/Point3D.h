#ifndef _Point3D_h_
#define _Point3D_h_

#include "Photogrammetry.h"
#include "Point3DPlain.h"
#include "Covariance3D.h"

//------------------------------------------------------------------------------
/// A stochastic point in a three-dimensional coordinate system
//------------------------------------------------------------------------------

class PHO_DLL Point3D : public Point3DPlain, public Covariance3D {

  public:

    /// Default constructor
    Point3D() : Point3DPlain(), Covariance3D() {}
      
    /// Construct from coordinates, a point number and (co-)variances
    Point3D(double x, double y, double z, int num, 
	    double v_x, double v_y, double v_z,
	    double cv_xy, double cv_xz, double cv_yz)
	: Point3DPlain(x, y, z, num), 
	  Covariance3D(v_x, v_y, v_z, cv_xy, cv_xz, cv_yz) {}

    /// Construct from a non-stochastic point and a covariance matrix
    Point3D(const Point3DPlain &point, const Covariance3D &cv)
    	: Point3DPlain(point),
    	  Covariance3D(cv) {}
   
    /// Construct from a vector, a point number and a covariance matrix
    Point3D(const Vector3D &vec, const PointNumber &n, const Covariance3D &cv)
    	: Point3DPlain(vec, n),
    	  Covariance3D(cv)
    	  {}
    	 	  
    /// Default destructor
    ~Point3D() {};
};
#endif /* _Point3D_h_ */   /* Do NOT add anything after this line */

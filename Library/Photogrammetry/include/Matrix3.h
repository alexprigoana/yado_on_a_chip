#ifndef _Matrix_h_
#define _Matrix_h_

#include "Photogrammetry.h"

class Vector2D;
class Vector3D;

/*--------------------------------------------------------------------------------
This file contains the definitions of the following class:

Matrix3  - A 3x3 matrix class

--------------------------------------------------------------------------------*/

class PHO_DLL Matrix3
{

public:
	Matrix3()			/* Default constructor                      */	
	{
		m[0][0] = 0; m[0][1] = 0; m[0][2] = 0; 
		m[1][0] = 0; m[1][1] = 0; m[1][2] = 0; 
		m[2][0] = 0; m[2][1] = 0; m[2][2] = 0;
	} 

	Matrix3(double a11, double a12, double a13, 
		double a21, double a22, double a23,
		double a31, double a32, double a33) { 
			m[0][0] = a11; m[0][1] = a12; m[0][2] = a13; 
			m[1][0] = a21; m[1][1] = a22; m[1][2] = a23; 
			m[2][0] = a31; m[2][1] = a32; m[2][2] = a33;
	}

	Matrix3(double[3][3]);

	Matrix3(double *);	  

	Matrix3 &operator=(const Matrix3&);    /* Copy assignment             */			

	~Matrix3() {};		/* Default destructor                       */

	double Det();

	Matrix3 Transpose();

	Matrix3 Inverse();

	Vector2D Nullspace();

	void Print();

	PHO_DLL friend Matrix3 operator*(double, const Matrix3 &);

	PHO_DLL friend Matrix3 operator*(const Matrix3 &, const Matrix3 &);

	PHO_DLL friend Vector3D operator*(const Matrix3 &, const Vector3D &); 

private:
	double m[3][3];

};

#endif /* _Matrix_h_ */   /* Do NOT add anything after this line */

/* 
  dummy internal.h file for the compilation 

  under KHOROS in the CANTATA environment 
  internals.h is a file in the source directory 
  (*.c, *.f, *.cc) with some internal definitions. 

  These definitions are not necessary under 
  the compilation in the CVS environment. 
  Therefore this file is empty. 

  However, some functions require NULL to be defined 
  or reference stderr, which is written in non of the
  include-files und CANTATA (or at least not obviously).
  These definitions are inserted here. 

*/
#ifndef __INTERNALS_H
#define __INTERNALS_H 

#include <stdio.h>

#ifndef NULL
#define NULL 0x0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif


#endif



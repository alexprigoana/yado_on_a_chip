#ifndef _ImagePointPlain_h_
#define _ImagePointPlain_h_


#include "Photogrammetry.h"
#include "PixelPosition.h"
#include "PointNumber.h"

//------------------------------------------------------------------------------
/// A non-stochastic point in a digital image
/** This point is defined by a pixel position (row, column) and a point number
*/
//------------------------------------------------------------------------------

class PHO_DLL ImagePointPlain : public PixelPosition, public PointNumber {

  public:

    /// Default constructor
    ImagePointPlain() : PixelPosition(), PointNumber() {}

    /// Construct from specified values
    ImagePointPlain(double r, double c, int num)
      : PixelPosition(r, c), PointNumber(num) {}
	  
    /// Construct from a 2D vector and a point number
    ImagePointPlain(const Vector2D &v, const PointNumber &n)
      : PixelPosition(v), PointNumber(n) {}	
	  
    /// Default destructor
    ~ImagePointPlain() {};
};
#endif /* _ImagePointPlain_h_ */   /* Do NOT add anything after this line */

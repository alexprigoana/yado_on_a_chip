#ifndef _MX_BOUND_QSLIM_INCLUDED // -*- C++ -*-
#define _MX_BOUND_QSLIM_INCLUDED
#if !defined(__GNUC__)
#  pragma once
#endif

/************************************************************************
Surface simplification using quadric error metrics
Force to keep boundary point
vertex adjacent to boundaries could only be merged to boundaries

Initialize creation: Biao Xiong
Data: Jan 6, 2013
************************************************************************/

#include "MxStdSlim.h"
#include "MxQSlim.h"
#include "MxQMetric3.h"

#define V_NORMAL 0
#define V_RIDGE 1
#define V_CORNER 2
#define V_BOUND 3

class MxBQSlimEdge : public MxEdge, public MxHeapable
{
public:
	float vnew[3];
};

class MK_DLL MxBoundQSlim : public MxQSlim
{
private:
	typedef MxSizedDynBlock<MxBQSlimEdge*, 6> edge_list;

	MxBlock<edge_list> edge_links;

	//
	// Temporary variables used by methods
	MxVertexList star, star2;
	MxPairContraction conx_tmp;

protected:
	double check_local_compactness(uint v1, uint v2, const float *vnew);
	double check_local_inversion(uint v1, uint v2, const float *vnew);
	uint check_local_validity(uint v1, uint v2, const float *vnew);
	uint check_local_degree(uint v1, uint v2, const float *vnew);
	void apply_mesh_penalties(MxBQSlimEdge *);
	void create_edge(MxVertexID i, MxVertexID j);
	void collect_edges();

	void compute_target_placement(MxBQSlimEdge *);
	void finalize_edge_update(MxBQSlimEdge *);

	virtual void compute_edge_info(MxBQSlimEdge *);
	virtual void update_pre_contract(const MxPairContraction&);
	virtual void update_post_contract(const MxPairContraction&);
	virtual void update_pre_expand(const MxPairContraction&);
	virtual void update_post_expand(const MxPairContraction&);

public:
	MxBoundQSlim(MxStdModel&);
	virtual ~MxBoundQSlim();

	void initialize();
	void initialize(const MxEdge *edges, uint count);
	bool decimate(uint target);

	void apply_contraction(const MxPairContraction& conx);
	void apply_expansion(const MxPairContraction& conx);

	uint edge_count() const { return heap.size(); }
	const MxBQSlimEdge *edge(uint i) const {return (MxBQSlimEdge *)heap.item(i);}

public:
	void (*contraction_callback)(const MxPairContraction&, float);
};

// MXQSLIM_INCLUDED
#endif

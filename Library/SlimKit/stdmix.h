#ifndef STDMIX_INCLUDED // -*- C++ -*-
#define STDMIX_INCLUDED
#if !defined(__GNUC__)
#  pragma once
#endif

/************************************************************************

  Standard base include file for the MIX library.  This defines various
  common stuff that is used elsewhere.

  Copyright (C) 1998 Michael Garland.  See "COPYING.txt" for details.
  
  $Id: stdmix.h,v 1.21.2.1 2002/01/31 18:38:37 garland Exp $

 ************************************************************************/

#include <cstdlib>
#include <cmath>
#include <climits>
#include <iostream>
//#include <gfx/gfx.h>
using namespace std;
#include <string.h>

#ifdef UINT_MAX
#  define MXID_NIL UINT_MAX
#else
#  define MXID_NIL 0xffffffffU
#endif

//export
#ifdef WIN32
#ifdef MK_EXPORTS
#define MK_DLL __declspec(dllexport)
#else
#define MK_DLL __declspec(dllimport)
#endif
#else
#define MK_DLL
#endif

#if !defined(HAVE_UINT) && !defined(uint)
typedef unsigned int uint;
#endif

#if !defined(HAVE_USHORT) && !defined(ushort)
typedef unsigned short ushort;
#endif

#ifdef MIN
#undef MIN
#endif
#ifdef MAX
#undef MAX
#endif

// Microsoft doesn't define std::min() & std::max() because it conflicts with
// <windef.h>, and their _MIN/_MAX macro workarounds don't add the std::
// namespace qualification.  These macros provide a uniform way of getting
// around this problem.
//
#define MIN(a,b) (((a)>(b))?(b):(a))
#define MAX(a,b) (((a)>(b))?(a):(b))

#ifndef ABS
#  define ABS(x) (((x)<0)?-(x):(x))
#endif

#ifndef MIX_NO_AXIS_NAMES
enum Axis {X=0, Y=1, Z=2, W=3};
#endif

inline bool streq(const char *a, const char *b) { return !strcmp(a,b); }

////////////////////////////////////////////////////////////////////////
//
// Optimization control, debugging, and error reporting facilities
//

//
// Safety levels:
//
//       -2 Reckless
//       -1 Optimized
//        0 Normal
//        1 Cautious
//        2 Paranoid
//
#ifndef SAFETY
// Default safety policy is to take the middle of the road
#define SAFETY 0
#endif

//#include "mixmsg.h"

#define fatal_error(s) //mxmsg_signal(MXMSG_FATAL, s, NULL, __FILE__, __LINE__)

#ifdef assert
#  undef assert
#endif

#if SAFETY >= 0
//#  define assert(i) (i)?((void)NULL):mxmsg_signal(MXMSG_ASSERT, # i, \
//						  NULL, __FILE__, __LINE__)
#  define assert(i) 
#  define CAREFUL(x) x
#else
#  define assert(i)
#  define CAREFUL(x)
#endif

#if SAFETY==2
#  define SanityCheck(t) assert(t)
#  define PARANOID(x) x
#else
#  define SanityCheck(t)
#  define PARANOID(x)
#endif

#if SAFETY > 0
#  define AssertBound(t)  assert(t)
#  define PRECAUTION(x) x
#else
#  define AssertBound(t)
#  define PRECAUTION(x)
#endif


////////////////////////////////////////////////////////////////////////
//
// Standards notwithstanding, not all platforms define exactly the
// same things in their header files.  We try to compensate here.
//


#ifndef M_PI
#  define M_PI 3.14159265358979323846264338327950288419716939937510582097494459
#endif

#if !defined(HUGE) && defined(HUGE_VAL)
#  define HUGE HUGE_VAL
#endif

#if !defined(HAVE_RINT)
inline double rint(double x) { return floor(x + 0.5); }
#endif

#if defined(HAVE_RANDOM)
inline double random1() { return (double)random() / (double)LONG_MAX; }
inline char   random_byte() { return (char)(random() & 0xff); }
#else
inline double random1() { return (double)rand() / (double)RAND_MAX; }
inline char   random_byte() { return (char)(rand() & 0xff); }
#endif

const double FEQ_EPS = 1e-6;
const double FEQ_EPS2 = 1e-12;

inline bool  FEQ(double a, double b, double e=FEQ_EPS)  {return fabs(a-b)<e;}
inline bool FEQ2(double a, double b, double e=FEQ_EPS2) {return fabs(a-b)<e;}

#define TIMING(t, cmd) { t=get_cpu_time(); cmd; t=get_cpu_time() - t; }
extern double get_cpu_time();

// STDMIX_INCLUDED
#endif

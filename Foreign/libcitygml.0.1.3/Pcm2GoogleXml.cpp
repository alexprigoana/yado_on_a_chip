#include "Buildings.h"
#include "ObjectPoints.h"
#include "googleXml.h"
#include "googleXmlParser.h"


bool pcm2GoogleXml(Buildings& blds, ObjectPoints& mapObjPts)
{
	citygml::ParserParams params;
	char* filename = args->String("-i");
	char* strOutObjPntsPath = args->String("-oo");
	char* strOutTopLinesPath = args->String("-ol");
	citygml::CityModel *city = citygml::load(filename, params );

	const citygml::CityObjectsMap& cityObjectsMap = city->getCityObjectsMap();
	citygml::CityObjectsMap::const_iterator it = cityObjectsMap.begin();

	for ( ; it != cityObjectsMap.end(); it++ )
	{
		const citygml::CityObjects& v = it->second;
		std::cout << ( log ? " Analyzing " : " Found " ) << v.size() << " " << citygml::getCityObjectsClassName( it->first ) << ( ( v.size() > 1 ) ? "s" : "" ) << "..." << std::endl;

		if ( log ) 
		{
			for ( unsigned int i = 0; i < v.size(); i++ )
			{
				std::cout << "  + found object " << v[i]->getId();
				if ( v[i]->getChildCount() > 0 ) std::cout << " with " << v[i]->getChildCount() << " children";
				std::cout << " with " << v[i]->size() << " geometr" << ( ( v[i]->size() > 1 ) ? "ies" : "y" );
				std::cout << std::endl;
			}
		}
	}
}
/* -*-c++-*- libcitygml - Copyright (c) 2010 Joachim Pouderoux, BRGM
*
* This file is part of libcitygml library
* http://code.google.com/p/libcitygml
*
* libcitygml is free software: you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as published by
* the Free Software Foundation, either version 2.1 of the License, or
* (at your option) any later version.
*
* libcitygml is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*/

// This is the implementation file for LibXml2 parser



#include "parser.h"
#include "tinyxml.h"
//#define TIXML_USE_STL;

using namespace citygml;

// CityGML LibXml2 SAX parsing handler
#define MAX_ATTRIBUTE_NUM 40
#define MAX_ATTRIBUTE_LENGTH 256
	


class CityGMLHandlerTinyXml : public CityGMLHandler
{
public:
	CityGMLHandlerTinyXml( const ParserParams& params ) : CityGMLHandler( params ) {}

	void startElement( const char* name, char** attrs ) {
		CityGMLHandler::startElement( wstos( name ), attrs );
	}
	void endElement( const char* name ) {
		CityGMLHandler::endElement( wstos( name ) );
	}

	void characters( const char *chars, int length ) {
		for ( int i = 0; i < length; i++ ) _buff << (char)chars[i]; 
	}
	static inline std::string wstos( const char* const str ) {
		return std::string( (const char*)str );
	}

protected:
	std::string getAttribute( void* attributes, const std::string& attname, const std::string& defvalue = "" ) 	{
		const char **attrs = (const char**)attributes;
		if ( !attrs ) return "";
		for ( int i = 0; attrs[i] != NULL; i += 2 ) 
			if ( wstos( attrs[i] ) == attname ) return wstos( attrs[ i + 1 ] );
		return defvalue;
	}
};


class CityGmlVisitor : public TiXmlVisitor
{
public:
	CityGmlVisitor(CityGMLHandlerTinyXml* handler);
	~CityGmlVisitor(){};

	/// Visit a document.
	inline bool VisitEnter( const TiXmlDocument& /*doc*/ );
	/// Visit a document.
	inline bool VisitExit( const TiXmlDocument& /*doc*/ );

	/// Visit an element.
	bool VisitEnter( const TiXmlElement& /*element*/, const TiXmlAttribute* /*firstAttribute*/ );
	/// Visit an element.
	bool VisitExit( const TiXmlElement& /*element*/ );

	/// Visit a declaration
	bool Visit( const TiXmlDeclaration& /*declaration*/ )	{ return true; }
	/// Visit a text node
	bool Visit( const TiXmlText& /*text*/ )					{ return true; }
	/// Visit a comment node
	bool Visit( const TiXmlComment& /*comment*/ )			{ return true; }
	/// Visit an unknown node
	bool Visit( const TiXmlUnknown& /*unknown*/ )			{ return true; }
private:
	CityGmlVisitor() {};

public:
	inline bool SetGMLParameter(const ParserParams& params) {_params = params;}
private:
	ParserParams _params;
	CityGMLHandlerTinyXml* _handler;
};

CityGmlVisitor::CityGmlVisitor(CityGMLHandlerTinyXml* handler)
{
	if (handler)
		_handler = handler;
	else 
		_handler = 0;
}

bool CityGmlVisitor::VisitEnter(const TiXmlDocument& doc)
{
	return true;
}

bool CityGmlVisitor::VisitExit( const TiXmlDocument& doc )
{
	return true;
}

/// Visit an element.
bool CityGmlVisitor::VisitEnter( const TiXmlElement& element, const TiXmlAttribute* firstAttribute )
{
	if (!_handler) return false;
	
	const char* attriName;
	const char* attriValue;
	const TiXmlAttribute* pXmlAttri = firstAttribute;
	int iAttri = 0;
	char** attris = 0;

	if (pXmlAttri)	{
		attris = new char* [MAX_ATTRIBUTE_NUM];
		for (int i=0; i<MAX_ATTRIBUTE_NUM; ++i) {
			attris[i] = new char [MAX_ATTRIBUTE_LENGTH];
		}
	}
	
	while (pXmlAttri) {
		//do something
		if (2*iAttri>MAX_ATTRIBUTE_NUM) {
			printf( "Too much attribute in %s\n", element.Value() );
			break;//too much attribute
		}
		attriName = pXmlAttri->Name();
		attriValue = pXmlAttri->Value();
		memcpy(attris[2*iAttri], attriName, strlen(attriName)*sizeof(char)+1);
		memcpy(attris[2*iAttri+1], attriValue, strlen(attriValue)*sizeof(char)+1);
	//	strcast();

		pXmlAttri = pXmlAttri->Next();
		iAttri++;
	}


	if (0 == strcmp( element.Value(), "gml:posList")) {
		int aaa = 0;
	}

	const char* xmlText = element.GetText();
	if (xmlText) 
		_handler->characters(xmlText, strlen(xmlText));
	_handler->startElement(element.Value(), attris);

	//free memory
	if (attris)	{
		for (int i=0; i<MAX_ATTRIBUTE_NUM; ++i) {
			if (attris[i])
				delete [] attris[i];
		}
		delete [] attris;
	}
	
	return true;
}

/// Visit an element.
bool CityGmlVisitor::VisitExit( const TiXmlElement& element )
{
	if (!_handler) return false;
	_handler->endElement(element.Value());
	return true;
}

/*
void startDocument( void *user_data ) 
{
	_xmlParserCtxt* context = static_cast<_xmlParserCtxt*>(user_data);
	static_cast<CityGMLHandlerTinyXml*>(context->_private)->startDocument();
}

void endDocument( void *user_data ) 
{
	_xmlParserCtxt* context = static_cast<_xmlParserCtxt*>(user_data);
	static_cast<CityGMLHandlerTinyXml*>(context->_private)->endDocument();
}

void startElement( void *user_data, const char *name, const char **attrs ) 
{
	_xmlParserCtxt* context = static_cast<_xmlParserCtxt*>(user_data);
	static_cast<CityGMLHandlerTinyXml*>(context->_private)->startElement( name, attrs );
}

void endElement( void *user_data, const char *name )
{
	_xmlParserCtxt* context = static_cast<_xmlParserCtxt*>(user_data);
	static_cast<CityGMLHandlerTinyXml*>(context->_private)->endElement( name );
}

void characters( void *user_data, const char *ch, int len )
{
	_xmlParserCtxt* context = static_cast<_xmlParserCtxt*>(user_data);
	static_cast<CityGMLHandlerTinyXml*>(context->_private)->characters( ch, len );
}

void fatalError( void *user_data, const char *msg, ... ) 
{
	_xmlParserCtxt* context = static_cast<_xmlParserCtxt*>(user_data);
	std::string error = "Parsing error!";

	va_list args = NULL;
	va_start( args, msg );
	size_t len = _vscprintf( msg, args ) + 1;
	std::vector<char> buffer( len, '\0' );
	if ( _vsnprintf_s( &buffer[0], buffer.size(), len, msg, args ) ) error = &buffer[0];
	va_end( args );

	static_cast<CityGMLHandlerTinyXml*>(context->_private)->fatalError( error );
	throw new std::string( error );
}
*/

// Parsing methods
namespace citygml {

/*	CityModel* load( std::istream& stream, const ParserParams& params )
	{
		CityGMLHandlerTinyXml* handler = new CityGMLHandlerTinyXml( params );

		TiXmlNode* pXmlNode;
		TiXmlElement* pXmlElement;

		TiXmlDocument xmlDoc;
		xmlDoc.Parse( stream );
		stream.get();

		if ( doc.Error() )
		{
			printf( "Error in %s: %s\n", doc.Value(), doc.ErrorDesc() );
			exit( 1 );
		}

		xmlDoc.FirstChild();

		CityModel* model = handler->getModel();
		delete handler;

		return model;	
	}


	CityModel* load( const std::string& fname, const ParserParams& params )
	{
		std::ifstream file;
		file.open( fname.c_str(), std::ifstream::in );
		if ( file.fail() ) { std::cerr << "CityGML: Unable to open file " << fname << "!" << std::endl; return NULL; }
		CityModel* model = load( file, params );
		file.close();
		return model;
	}*/

/*	CityModel* load( const std::string& fname, const ParserParams& params )
	{
		CityGMLHandlerTinyXml* handler = new CityGMLHandlerTinyXml( params );
		TiXmlNode* pXmlNode;
		TiXmlElement* pXmlElement;
		TiXmlAttribute* pXmlAttri;
		TiXmlDocument xmlDoc(fname.c_str());
		const char* attriName;
		const char* attriValue;
		const char* nodeName;
		int nMaxAttrNum = 20;
		int nMaxAttrLen = 256;
		int iAttri;
		char** attris = new char* [nMaxAttrNum];
		for (int i=0; i<nMaxAttrNum; ++i) {
			attris[i] = new char [nMaxAttrLen];
		}

		if(!xmlDoc.LoadFile())	{
			printf( "Error in %s: %s\n", xmlDoc.Value(), xmlDoc.ErrorDesc() );
			exit( 1 );
		}

		pXmlNode = xmlDoc.FirstChild();
		while(pXmlNode) {
			//do something
			pXmlElement = pXmlNode->ToElement();
			if (!pXmlElement) continue;

			pXmlAttri = pXmlElement->FirstAttribute();
			iAttri = 0;
			while (pXmlAttri) {
				//do someting
				attriName = pXmlAttri->Name();
				attriValue = pXmlAttri->Value();
				memcpy(attris[2*iAttri], attriName, strlen(attriName)*sizeof(char));
				memcpy(attris[2*iAttri+1], attriValue, strlen(attriValue)*sizeof(char));

				pXmlAttri = pXmlAttri->Next();
			}

			//CityGMLHandler.startElement(pXmlNode->,attris);

			pXmlNode = pXmlNode->NextSibling();
		}


		CityModel* model = handler->getModel();
		delete handler;

		for (int i=0; i<nMaxAttrNum; ++i) 
			delete [] attris[i];
		delete [] attris;

		return model;	
	}*/



LIBCITYGML_EXPORT 
	CityModel* load( const char* fileName, const ParserParams& params )
	{
		TiXmlDocument xmlDoc(fileName);
		if(!xmlDoc.LoadFile())	{
			printf( "Error in %s: %s\n", xmlDoc.Value(), xmlDoc.ErrorDesc() );
			exit( 1 );
		}

		CityGMLHandlerTinyXml* handle = new CityGMLHandlerTinyXml(params);
		CityGmlVisitor visitor(handle);
		xmlDoc.Accept(static_cast<TiXmlVisitor*>(&visitor));
		CityModel* cityModel = handle->getModel();
		int n = cityModel->size();
		delete handle;
		return cityModel;
		//xmlDoc.Parse();
	}
}


//============================================================================
// Name        : license-manager-cpp.cpp
// Author      : 
// Version     :
// Copyright   : BSD
//============================================================================

#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include "license++.h"
#include "LicenseReader.h"
#include "EventRegistry.h"
#include <vector>

using namespace std;

OLM_API void print_error(char out_buffer[256], LicenseInfo* licenseInfo) {

}

OLM_API void identify_pc(IDENTIFICATION_STRATEGY pc_id_method, char chbuffer[PC_IDENTIFIER_SIZE + 1]) {

}

static void mergeLicenses(vector<openLM::FullLicenseInfo> licenses, LicenseInfo* license) 
{
	if (license != NULL) {
		time_t curLicense_exp = 0;
		for (auto it = licenses.begin(); it != licenses.end(); it++) {
			//choose the license that expires later...
			if (!it->has_expiry) {
				it->toLicenseInfo(license);
				break;
			} else if (curLicense_exp < it->expires_on()) {
				curLicense_exp = it->expires_on();
				it->toLicenseInfo(license);
			}
		}
	}
}

EVENT_TYPE verify_license(const char * product, LicenseLocation licenseLocation, LicenseInfo* license) 
{
	openLM::LicenseReader lr = openLM::LicenseReader(licenseLocation);
	vector<openLM::FullLicenseInfo> licenses;
	openLM::EventRegistry er = lr.readLicenses(string(product), licenses);
	EVENT_TYPE result;

	if (licenses.size() > 0) {
		vector<openLM::FullLicenseInfo> licenses_with_errors;
		vector<openLM::FullLicenseInfo> licenses_ok;
		
		for (auto it = licenses.begin(); it != licenses.end(); it++) {
			openLM::EventRegistry validation_er = it->validate(0);
			if (validation_er.isGood()) {
				licenses_ok.push_back(*it);
			} else {
				licenses_with_errors.push_back(*it);
			}
			er.append(validation_er);
		}

		if (licenses_ok.size() > 0) {
			er.turnErrosIntoWarnings();
			result = LICENSE_OK;
			mergeLicenses(licenses_ok, license);
		} else {
			result = er.getLastFailure()->event_type;
			mergeLicenses(licenses_with_errors, license);
		}
	} else {
		if ( er.getLastFailure())
			result = er.getLastFailure()->event_type;
		else 
			result = LICENSE_FILE_NOT_FOUND;
	}

	if (license != NULL) {
		er.exportLastEvents(license->status, 5);
	}

	return result;
}

OLM_API EVENT_TYPE confirm_license(char * product,
		LicenseLocation licenseLocation) 
{
	return LICENSE_OK;
}

OLM_API EVENT_TYPE release_license(char * product,
		LicenseLocation licenseLocation) 
{
	return LICENSE_OK;
}

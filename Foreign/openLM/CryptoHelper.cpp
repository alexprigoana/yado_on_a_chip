#include <memory>
#include "CryptoHelper.h"
#ifdef __unix__
#include"CryptoHelperLinux.h"
#else
#include"CryptoHelperWindows.h"
#endif

using namespace std;
namespace openLM {

unique_ptr<CryptoHelper> CryptoHelper::getInstance() {
#ifdef __unix__
	unique_ptr<CryptoHelper> ptr((CryptoHelper*) new CryptoHelperLinux());
#else
	unique_ptr<CryptoHelper> ptr((CryptoHelper*) new CryptoHelperWindows());
#endif
	return ptr;
}
}

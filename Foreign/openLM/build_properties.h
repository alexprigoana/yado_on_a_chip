#ifndef BUILD_PROPERTIES_H_
#define BUILD_PROPERTIES_H_

#define PROJECT_INT_VERSION 100
#define PROJECT_VERSION "1.0.0"
#define PROJECT_BINARY_DIR "D:/OpenSource/open-license-manager-master/open-license-manager-master/build"
#define PROJECT_SRC_DIR "D:/OpenSource/open-license-manager-master/open-license-manager-master"
#define PROJECT_BASE_DIR "D:/OpenSource/open-license-manager-master/open-license-manager-master"
#define PROJECT_TEST_SRC_DIR "D:/OpenSource/open-license-manager-master/open-license-manager-master/test"
#define PROJECT_TEST_TEMP_DIR "D:/OpenSource/open-license-manager-master/open-license-manager-master/build/Testing/Temporary"
#define BUILD_TYPE ""
#endif
